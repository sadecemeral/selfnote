package de.mh.selfnote.toolbox;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import androidx.core.content.FileProvider;
import androidx.exifinterface.media.ExifInterface;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.mh.selfnote.BuildConfig;

public class ImagePicker {

    static private ImagePicker imagePicker;
    static public String[] permissionStringList = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_MEDIA_IMAGES};

    static public ImagePicker getInstance() {
        if (imagePicker == null)
            imagePicker = new ImagePicker();

        return imagePicker;
    }

    private ImagePicker() {
    }

    static private final int DEFAULT_MIN_WIDTH_QUALITY = 400; // min pixels
    static private final int minWidthQuality = DEFAULT_MIN_WIDTH_QUALITY;

    static private final String TAG = "ImagePicker";

    static private boolean allowCamera = false;
    static private boolean allowReadExternalStorage = false;
    static private boolean allowWriteExternalStorage = false;
    static private File imageFile;
    static private String currentPhotoPath;
    static public Uri selectedImageUri;

    static public void allowCameraUse() {
        allowCamera = true;
    }

    static public void allowReadExternalStorageUse() {
        allowReadExternalStorage = true;
    }

    static public void allowWriteExternalStorageUse() {
        allowWriteExternalStorage = true;
    }

    static private Intent getCameraIntent(Context ctx) {

        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePhotoIntent.resolveActivity(ctx.getPackageManager()) != null) {

            try {
                imageFile = createImageFile(ctx);
            } catch (IOException ex) {

            }

            if (imageFile != null) {

                Uri photoURI = FileProvider.getUriForFile(ctx,
                        BuildConfig.APPLICATION_ID + ".fileprovider",
                        imageFile);

                takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            }
        }

        return takePhotoIntent;
    }

    static private Intent getPickIntent(boolean allowMultipleSelection) {
        Intent pickIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        if (allowMultipleSelection)
            pickIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);

        return pickIntent;
    }

    static public Intent getPickImageIntent(Context ctx, String title, boolean allowMultipleSelection) {
        Intent chooserIntent = null;

        if (allowReadExternalStorage || allowWriteExternalStorage)
            chooserIntent = Intent.createChooser(getPickIntent(allowMultipleSelection), title);

        if (allowCamera) {
            if (chooserIntent == null)
                chooserIntent = Intent.createChooser(getCameraIntent(ctx), title);
            else
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{getCameraIntent(ctx)});
        }

        return chooserIntent;
    }


    static public Uri getImageFromResult(Context context, int resultCode, Intent imageReturnedIntent) {
        Log.d(TAG, "getImageFromResult, resultCode: " + resultCode);

        if (resultCode == Activity.RESULT_OK) {
            //Uri selectedImage;
            boolean isCamera = (imageReturnedIntent == null || imageReturnedIntent.getData() == null);
            if (isCamera) {     /** CAMERA **/
                selectedImageUri = FileProvider.getUriForFile(context,
                        BuildConfig.APPLICATION_ID + ".fileprovider", imageFile);
            } else {            /** ALBUM **/
                selectedImageUri = imageReturnedIntent.getData();
            }
        }
        return selectedImageUri;
    }


    static public String getRealPathFromURI(Context parent, Uri contentUri) {
        String result;
        Cursor cursor = parent.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    static private File createImageFile(Context context) throws IOException {
        // Create an image file name
        File storageDir = new File(context.getFilesDir(), "images");
        if (!storageDir.exists())
            storageDir.mkdirs();

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        imageFile = File.createTempFile(imageFileName, ".jpg", storageDir);
        currentPhotoPath = imageFile.getAbsolutePath();
        return imageFile;
    }


    static private Bitmap decodeBitmap(Context context, Uri theUri, int sampleSize) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = sampleSize;

        AssetFileDescriptor fileDescriptor = null;
        try {
            fileDescriptor = context.getContentResolver().openAssetFileDescriptor(theUri, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (fileDescriptor != null)
            return BitmapFactory.decodeFileDescriptor(
                    fileDescriptor.getFileDescriptor(), null, options);

        return null;
    }

    /**
     * Resize to avoid using too much memory loading big images (e.g.: 2560*1920)
     **/
    static private Bitmap getImageResized(Context context, Uri selectedImage) {
        Bitmap bm = null;
        int[] sampleSizes = new int[]{5, 3, 2, 1};
        int i = 0;
        do {
            bm = decodeBitmap(context, selectedImage, sampleSizes[i]);

            if (bm == null)
                return null;

            i++;
        } while (bm.getWidth() < minWidthQuality && i < sampleSizes.length);
        return bm;
    }


    static private int getRotation(Context context, Uri imageUri, boolean isCamera) {
        int rotation;
        if (isCamera) {
            rotation = getRotationFromCamera(context, imageUri);
        } else {
            rotation = getRotationFromGallery(context, imageUri);
        }
        Log.d(TAG, "Image rotation: " + rotation);
        return rotation;
    }


    static private int getRotationFromCamera(Context context, Uri imageFile) {
        int rotate = 0;
        try {

            context.getContentResolver().notifyChange(imageFile, null);
            ExifInterface exif = new ExifInterface(currentPhotoPath);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }


    static private int getRotationFromGallery(Context context, Uri imageUri) {
        int result = 0;
        String[] columns = {MediaStore.Images.Media.ORIENTATION};
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(imageUri, columns, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int orientationColumnIndex = cursor.getColumnIndex(columns[0]);
                result = cursor.getInt(orientationColumnIndex);
            }
        } catch (Exception e) {
            //Do nothing
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }//End of try-catch block
        return result;
    }


    static private Bitmap rotate(Bitmap bm, int rotation) {
        if (rotation != 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);
            return Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, true);
        }
        return bm;
    }

    private void setPic(ImageView imageView) {
        // Get the dimensions of the View
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(currentPhotoPath, bmOptions);

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.max(1, Math.min(photoW / targetW, photoH / targetH));

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        //bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath, bmOptions);
        imageView.setImageBitmap(bitmap);
    }
}