package de.mh.selfnote.toolbox;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class DatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private final Calendar pickedDate;
    private final View parentView;

    public DatePicker(View view) {
        super();
        parentView = view;
        pickedDate = Calendar.getInstance();
        Long longDate = null;
        if (parentView != null && !TextUtils.isEmpty(((EditText) parentView).getText().toString())) {
            longDate = Utilities.getDateLong(((EditText) parentView).getText().toString(), "dd.MM.yyyy");
        }

        if (longDate == null)
            pickedDate.add(Calendar.YEAR, -12); // mind. 12 Jahre
        else
            pickedDate.setTimeInMillis(longDate);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        int year = pickedDate.get(Calendar.YEAR);
        int month = pickedDate.get(Calendar.MONTH);
        int day = pickedDate.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(android.widget.DatePicker datePicker, int iYear, int iMonth, int iDay) {
        // Do something with the date chosen by the user
        pickedDate.set(Calendar.YEAR, iYear);
        pickedDate.set(Calendar.MONTH, iMonth);
        pickedDate.set(Calendar.DAY_OF_MONTH, iDay);

        EditText textView = (EditText) parentView;
        textView.setText(Utilities.formatDate("dd.MM.yyyy", pickedDate.getTimeInMillis()));
    }
}