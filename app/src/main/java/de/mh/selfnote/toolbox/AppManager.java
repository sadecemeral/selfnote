package de.mh.selfnote.toolbox;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.Stack;

public class AppManager {
    private static Stack<Activity> activityStack;
    private static AppManager instance;

    private AppManager() {
    }

    public static AppManager getAppManager() {
        if (instance == null) {
            instance = new AppManager();
        }
        return instance;
    }

    /**
     * Add Activity to stack
     */
    public void addActivity(Activity activity) {
        if (activityStack == null) {
            activityStack = new Stack<>();
        }
        activityStack.add(activity);
    }

    /**
     * Get current Activity（stack Top Activity）
     */
    public Activity currentActivity() {
        if (activityStack == null || activityStack.isEmpty()) {
            return null;
        }
        Activity activity = activityStack.lastElement();
        return activity;
    }

    public Activity previousActivity() {
        if (activityStack == null || activityStack.isEmpty()) {
            return null;
        }

        int iIndex = activityStack.indexOf(activityStack.lastElement());
        iIndex--;
        if (iIndex < 0)
            return null;

        return activityStack.elementAt(iIndex);
    }

    public Activity findActivity(Class<?> cls) {
        Activity activity = null;
        for (Activity aty : activityStack) {
            if (aty.getClass().equals(cls)) {
                activity = aty;
                break;
            }
        }
        return activity;
    }

    /**
     * Finish Activity（Stack Top Activity）
     */
    public void finishTopActivity() {
        finishActivity(currentActivity());
    }

    /**
     * Finish someone Activity
     */
    public void finishActivity(Activity activity, int resultCode) {
        if (activity != null) {
            activity.setResult(resultCode);
            finishActivity(activity);
        }
    }

    /**
     * Finish someone Activity
     */
    public void finishActivity(Activity activity, int resultCode, Intent intent) {
        if (activity != null) {
            activity.setResult(resultCode, intent);
            finishActivity(activity);
        }
    }

    /**
     * Finish someone Activity
     */
    private void finishActivity(Activity activity) {
        if (activity != null) {
            if (activityStack.contains(activity)) {
                activityStack.remove(activity);
            }
            activity.finish();
            activity = null;
        }
    }

    /**
     * Finish someone Activity
     */
    public void finishActivity(Class<?> cls) {
        if (activityStack == null || activityStack.isEmpty()) {
            return;
        }

        ArrayList<Activity> finishList = new ArrayList<>();
        for (Activity activity : activityStack) {
            if (activity.getClass().equals(cls)) {
                finishList.add(activity);
            }
        }

        for(Activity activity : finishList) {
            finishActivity(activity);
        }
    }

    /**
     * Finish All Activity except cls, if cls not in stack, then clear all
     *
     * @param cls
     */
    public void finishOthersActivity(Class<?> cls) {
        if (activityStack == null || activityStack.isEmpty()) {
            return;
        }

        ArrayList<Activity> finishList = new ArrayList<>();
        for (Activity activity : activityStack) {
            if (!(activity.getClass().equals(cls))) {
                finishList.add(activity);
            }
        }

        for(Activity activity : finishList) {
            finishActivity(activity);
        }
    }

    /**
     * Finish All Activity
     */
    public void finishAllActivity() {
        for (int i = 0, size = activityStack.size(); i < size; i++) {
            if (null != activityStack.get(i)) {
                if (activityStack.contains(activityStack.get(i))) {
                    activityStack.get(i).finish();
                }
            }
        }
        activityStack.clear();
    }

    /**
     * exit
     */
    public void AppExit(Context context) {
        try {
            finishAllActivity();
            ActivityManager activityMgr = (ActivityManager) context
                    .getSystemService(Context.ACTIVITY_SERVICE);
            activityMgr.killBackgroundProcesses(context.getPackageName());
            System.exit(0);
            android.os.Process.killProcess(android.os.Process.myPid());
        } catch (Exception e) {
            System.exit(0);
        }
    }
}