package de.mh.selfnote.toolbox;

import android.content.Context;

import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import java.util.Hashtable;

import de.mh.selfnote.R;

public class CircularImageLoadingDrawable {

    private static Hashtable<Context, CircularProgressDrawable> oList;

    public static CircularProgressDrawable getWait(Context parentCtx) {
        if (oList == null)
            oList = new Hashtable<>();

        if (!oList.containsKey(parentCtx)) {
            CircularProgressDrawable progressDrawable = new CircularProgressDrawable(parentCtx);
            final int iColor = ContextCompat.getColor(parentCtx, R.color.colorPrimaryDark);

            progressDrawable.setColorSchemeColors(iColor);
            progressDrawable.setStrokeWidth(20f);
            progressDrawable.setCenterRadius(100f);
            progressDrawable.start();
            oList.put(parentCtx, progressDrawable);
        }

        return oList.get(parentCtx);
    }
}
