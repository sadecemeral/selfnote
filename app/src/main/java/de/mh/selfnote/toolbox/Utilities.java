package de.mh.selfnote.toolbox;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.FileProvider;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.recyclerview.widget.DiffUtil;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.SignInButton;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import de.mh.selfnote.BuildConfig;
import de.mh.selfnote.NotificationActivity;
import de.mh.selfnote.R;
import de.mh.selfnote.objects.Note;
import de.mh.selfnote.objects.Notification;
import de.mh.selfnote.objects.UserProfile;
import de.mh.selfnote.viewModel.CategoryViewModel;
import de.mh.selfnote.viewModel.SortViewModel;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class Utilities {
    private static Utilities utilities;
    private static String prefLogin = "de.mh.selfnote.PREFERENCE.LOGIN";
    final public long ONE_MEGABYTE = 1024 * 1024;
    final public String CONTENT_TYPE_IMAGE = "image/jpg";

    public static Utilities getInstance() {
        if (utilities == null)
            utilities = new Utilities();

        return utilities;
    }

    public static void loadUserprofilePhotoIntoView(Context parentCtx, UserProfile userProfile, ImageView imgView) {

        if (userProfile == null || imgView == null)
            return;

        if (!TextUtils.isEmpty(userProfile.getUserPhotoURL())) {

            Glide.with(parentCtx)
                    .load(userProfile.getUserPhotoURL())
                    .circleCrop()
                    .placeholder(R.drawable.ic_user_profile)
                    .into(imgView);
        } else if (userProfile.getFirebaseUID() != null) {

            UserProfileViewModel.getInstance().getUserImageReference(userProfile.getFirebaseUID())
                    .child(userProfile.getFirebaseUID()).getDownloadUrl()
                    .addOnSuccessListener(uri -> {
                        userProfile.setUserPhotoURL(uri.toString());
                        Glide.with(parentCtx)
                                .load(uri.toString())
                                .circleCrop()
                                .placeholder(R.drawable.ic_user_profile)
                                .into(imgView);
                    })
                    .addOnFailureListener(e -> {
                        imgView.setImageResource(R.drawable.ic_user_profile);
                    });
        }
    }

    public static Drawable bytes2Drawable(byte[] byteArray) {
        if (byteArray == null || byteArray.length < 1)
            return null;

        Bitmap bitmap = bytes2Bitmap(byteArray);
        return bitmap2Drawable(bitmap);
    }

    public static Bitmap bytes2Bitmap(byte[] b) {
        if (b.length != 0) {
            return BitmapFactory.decodeByteArray(b, 0, b.length);
        }

        return null;
    }

    public static Drawable bitmap2Drawable(Bitmap bitmap) {
        @SuppressWarnings("deprecation")
        BitmapDrawable bd = new BitmapDrawable(bitmap);
        return (Drawable) bd;
    }

    public static Drawable getDrawableByName(Context ctx, String strItem) {
        if (ctx == null || ctx.getResources() == null)
            return null;

        if (strItem == null || strItem.isEmpty())
            return null;

        int iIdent = ctx.getResources().getIdentifier(strItem, "drawable", ctx.getPackageName());
        if (iIdent == 0)//not found
            return null;

        return ctx.getDrawable(iIdent);
    }

    public static int getDrawableIdentByName(Context ctx, String strItem) {
        if (ctx == null || ctx.getResources() == null)
            return 0;

        if (strItem == null || strItem.isEmpty())
            return 0;

        return ctx.getResources().getIdentifier(strItem, "drawable", ctx.getPackageName());
    }

    public static int getColorByName(Context ctx, String strItem) {
        if (ctx == null || ctx.getResources() == null)
            return 0;

        int iColorIdent = ctx.getResources().getIdentifier(strItem, "color", ctx.getPackageName());
        if (iColorIdent == 0)//not found
            return 0;

        return Color.parseColor(ctx.getResources().getString(iColorIdent));
    }

    public static byte[] getBytesFromImageUri(Context ctx, Uri uri) {
        try {
            InputStream inputStream = ctx.getContentResolver().openInputStream(uri);
            ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];

            int len = 0;
            while ((len = inputStream.read(buffer)) != -1) {
                byteBuffer.write(buffer, 0, len);
            }
            return byteBuffer.toByteArray();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }


    public static byte[] getBytes(ImageView imageView) {

        try {
            if (imageView == null || imageView.getDrawable() == null)
                return null;

            if (!(imageView.getDrawable() instanceof BitmapDrawable))
                return null;

            Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] bytesData = stream.toByteArray();
            stream.close();

            return bytesData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap getBitmap(ImageView imageView) {
        try {
            if (imageView == null || imageView.getDrawable() == null)
                return null;

            if (!(imageView.getDrawable() instanceof BitmapDrawable))
                return null;

            return ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Drawable bytes2RoundedDrawable(Context parent, byte[] byteArray) {
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        RoundedBitmapDrawable roundedBitmapDrawable =
                RoundedBitmapDrawableFactory.create(parent.getResources(), bitmap);

        //oval
        //roundedBitmapDrawable.setCornerRadius(Math.min(roundedBitmapDrawable.getMinimumWidth(),
        //        roundedBitmapDrawable.getMinimumHeight()));

        roundedBitmapDrawable.setCircular(true);

        return roundedBitmapDrawable;
    }

    static private File createImageFile2Share(Context context) {
        File storageDir = new File(context.getFilesDir(), "share");
        if (!storageDir.exists())
            storageDir.mkdirs();

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmssSSS").format(new Date());

        return new File(storageDir, "share_image_" + timeStamp + ".png");
    }

    public static Uri getUriForBitmap(Bitmap image, Context context) {

        // Store image to default external storage directory
        Uri bmpUri = null;

        try {
            // wrap File object into a content provider. NOTE: authority here should match authority in manifest declaration
            File file = createImageFile2Share(context);
            FileOutputStream out = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.PNG, 100, out);
            out.flush();
            out.close();

            // wrap File object into a content provider. NOTE: authority here should match authority in manifest declaration
            bmpUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileprovider", file);

        } catch (Exception e) {
            Log.e("Sharing", e.getMessage());
            e.printStackTrace();
        }

        return bmpUri;
    }

    public static ArrayList<Note> filterDataList(ArrayList<Note> dataList, List<String> filterCriteria) {

        CategoryViewModel categoryViewModel = UserProfileViewModel.getCategoryViewModel();
        ArrayList<Note> resultList = new ArrayList<>();

        if (dataList == null || dataList.size() < 1 || categoryViewModel == null)
            return resultList;

        if (filterCriteria == null)
            filterCriteria = categoryViewModel.getEnabledCategoryNameList();

        if (filterCriteria == null || filterCriteria.size() < 1)
            return resultList;

        for (Note oObject : dataList) {
            if (filterCriteria.contains(oObject.getNoteCategory().getCategoryName()))
                resultList.add(oObject);
        }

        return resultList;
    }

    public static void sortNotificationList(List<Notification> notificationList) {
        if (notificationList == null || notificationList.size() < 1)
            return;

        Collections.sort(notificationList, Notification.NotificationTimestampDESCComparator);
    }

    public static void sortDataList(List<Note> noteList, String[] sortKeyOrder) {
        if (noteList == null || noteList.size() < 1)
            return;

        SortViewModel sortViewModel = UserProfileViewModel.getSortViewModel();

        if (sortKeyOrder == null)
            sortKeyOrder = sortViewModel.getEnabledSortKeyList();

        if (sortKeyOrder == null || sortKeyOrder.length < 1) {
            Collections.sort(noteList, Note.NoteTimestampDESCComparator);
        } else if (sortKeyOrder.length == 1) {
            if (sortKeyOrder[0].equals("NOTE_TIMESTAMP"))
                Collections.sort(noteList, Note.NoteTimestampDESCComparator);
            else if (sortKeyOrder[0].equals("NOTE_TITLE"))
                Collections.sort(noteList, Note.NoteTitleComparator);
            else if (sortKeyOrder[0].equals("NOTE_CATEGORY_ID"))
                Collections.sort(noteList, Note.NoteCategoryComparator);
        }
    }

    public static class NoteDiffUtil extends DiffUtil.ItemCallback<Note> {
        @Override
        public boolean areItemsTheSame(@NonNull Note oldItem, @NonNull Note newItem) {
            return oldItem.getNoteKey() == newItem.getNoteKey();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Note oldItem, @NonNull Note newItem) {
            if (TextUtils.equals(oldItem.getNoteTitle(), newItem.getNoteTitle()))
                if (TextUtils.equals(oldItem.getNoteText(), newItem.getNoteText()))
                    if (TextUtils.equals(oldItem.getNoteCategory().categoryKey, newItem.getNoteCategory().categoryKey))
                        if (TextUtils.equals(oldItem.getNoteKeyword(), newItem.getNoteKeyword()))
                            if (oldItem.isNotePersonal() == newItem.isNotePersonal())
                                if (oldItem.getNoteImageListURL().containsAll(newItem.getNoteImageListURL()))
                                    if (oldItem.getNoteRating() == newItem.getNoteRating())
                                        if (oldItem.isNotePinned() == newItem.isNotePinned())
                                            return true;

            return false;
        }
    }

    public static String formatDate(String strFormat, Long timestamp) {
        if (timestamp == null || strFormat == null)
            return null;

        SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
        return sdf.format(new Timestamp(timestamp));
    }

    public static Long getDateLong(String strFormattedDate, String strFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(strFormat);
        try {
            return ((Date) Objects.requireNonNull(sdf.parse(strFormattedDate))).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Long subMonthFromTimestamp(Long timestamp, int iMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        calendar.add(Calendar.MONTH, -iMonth);

        return calendar.getTimeInMillis();
    }

    public static boolean isLoginRemember(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(prefLogin, ctx.MODE_PRIVATE);
        String checkRemember = preferences.getString("remember", "");
        if ("true".equals(checkRemember))
            return true;

        return false;
    }

    public static void setLoginRemember(Context ctx, String strUserEmail, String strUserPassword) {
        SharedPreferences preferences = ctx.getSharedPreferences(prefLogin, ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("remember", "true");
        editor.putString("UserEmail", strUserEmail);
        editor.putString("UserPassword", strUserPassword);
        editor.apply();
    }

    public static String getLoginUserEmail(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(prefLogin, ctx.MODE_PRIVATE);
        return preferences.getString("UserEmail", "");
    }

    public static String getLoginUserPassword(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(prefLogin, ctx.MODE_PRIVATE);
        return preferences.getString("UserPassword", "");
    }

    public static void resetLoginRemember(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(prefLogin, ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("remember", "false");
        editor.apply();
    }

    public static void setSignInToken(Context ctx, String idToken) {
        SharedPreferences preferences = ctx.getSharedPreferences(prefLogin, ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("googleIdToken", idToken);
        editor.apply();
    }

    public static String getSignInToken(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(prefLogin, ctx.MODE_PRIVATE);
        return preferences.getString("googleIdToken", "");
    }

    public static void resetSignInToken(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences(prefLogin, ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("googleIdToken", null);
        editor.apply();
    }

    public static void setShowOneTapUI(Context ctx, boolean showOneTapUI){
        SharedPreferences preferences = ctx.getSharedPreferences(prefLogin, ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("showOneTapUI", showOneTapUI);
        editor.apply();
    }

    public static boolean getShowOneTapUI(Context ctx){
        SharedPreferences preferences = ctx.getSharedPreferences(prefLogin, ctx.MODE_PRIVATE);
        return preferences.getBoolean("showOneTapUI", true);
    }

    public static void resetShowOneTapUI(Context ctx){
        SharedPreferences preferences = ctx.getSharedPreferences(prefLogin, ctx.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove("showOneTapUI");
        editor.apply();
    }

    public static void showNotification(Context ctx, String messageTitle, String messageBody) {
        Intent intent = new Intent(ctx, NotificationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            pendingIntent = PendingIntent.getActivity(ctx, 0 /* Request code */, intent,
                    PendingIntent.FLAG_MUTABLE);
        } else {
            pendingIntent = PendingIntent.getActivity(ctx, 0 /* Request code */, intent,
                    PendingIntent.FLAG_MUTABLE);
        }

        String channelId = ctx.getString(R.string.default_notification_channel_id);
        String channelName = ctx.getString(R.string.default_notification_channel_name);
        String channelDescription = ctx.getString(R.string.default_notification_channel_descr);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(ctx, channelId)
                        .setSmallIcon(R.drawable.ic_notification_selfnote)
                        .setContentTitle(messageTitle)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    channelId,
                    channelName,
                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription(channelDescription);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationManager.createNotificationChannel(notificationChannel);
        }

        notificationManager.notify(new Random().nextInt() /* ID of notification */, notificationBuilder.build());
    }

    public static void setGooglePlusButtonText(SignInButton signInButton, String buttonText) {
        for (int i = 0; i < signInButton.getChildCount(); i++) {
            View v = signInButton.getChildAt(i);

            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setTextSize(15);
                tv.setTypeface(null, Typeface.NORMAL);
                tv.setText(buttonText);
                tv.setAllCaps(false);
                return;
            }
        }
    }
}