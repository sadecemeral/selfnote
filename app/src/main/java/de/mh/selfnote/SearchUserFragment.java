package de.mh.selfnote;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import javax.annotation.Nullable;

import de.mh.selfnote.adapter.UserListAdapter;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.extended.WrapContentLinearLayoutManager;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class SearchUserFragment extends Fragment {

    public static final String TAG = "SearchUserFragment";
    private static final String ARGS = "SearchString";

    private UserListAdapter _adapter;
    private RecyclerView _viewResult;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        _viewResult = view.findViewById(R.id.result_user_list);
        _viewResult.setLayoutManager(new WrapContentLinearLayoutManager(getActivity()));

        if (savedInstanceState != null && savedInstanceState.containsKey(ARGS)) {
            startSearch(savedInstanceState.getString(ARGS));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (ViewGroup) inflater.inflate(R.layout.fragment_search_user, container, false);
    }

    public void startSearch(String strSearch) {

        Bundle args = new Bundle();
        args.putString(ARGS, strSearch);
        setArguments(args);

        if (_adapter == null) {
            _adapter = new UserListAdapter((BasicActivity) getActivity(), UserProfileViewModel.getInstance().getResultUserOptions(this, strSearch));
        } else {
            _adapter.updateOptions(UserProfileViewModel.getInstance().getResultUserOptions(this, strSearch));
        }

        if (_viewResult != null)
            _viewResult.setAdapter(_adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (_adapter != null)
            _adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (_adapter != null) {
            _adapter.stopListening();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        Bundle args = getArguments();
        if (args != null && args.containsKey(ARGS))
            outState.putString(ARGS, args.getString(ARGS));
    }
}