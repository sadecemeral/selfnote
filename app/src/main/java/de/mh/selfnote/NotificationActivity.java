package de.mh.selfnote;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Objects;

import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.extended.WrapContentLinearLayoutManager;
import de.mh.selfnote.objects.Notification;
import de.mh.selfnote.objects.NotificationType;
import de.mh.selfnote.toolbox.AppManager;
import de.mh.selfnote.toolbox.Utilities;
import de.mh.selfnote.viewModel.NotificationViewModel;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class NotificationActivity extends BasicActivity {

    private NotificationActivity_Adapter _adapter;
    private NotificationViewModel _viewNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.activity_title_notification);

        mProgressBar = findViewById(R.id.progressBar_notification);

        _viewNotification = UserProfileViewModel.getNotificationViewModel();

        RecyclerView viewList = findViewById(R.id.notification_list);
        _adapter = new NotificationActivity_Adapter(this, _viewNotification.getNotificationOptions(this));
        viewList.setLayoutManager(new WrapContentLinearLayoutManager(this));
        viewList.setAdapter(_adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //ActionBar
        this.getMenuInflater().inflate(R.menu.menu_notification_activity, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.MenuItemCancelNotification:
                AppManager.getAppManager().finishActivity(this, RESULT_CANCELED);

                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (_adapter != null)
            _adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (_adapter != null) {
            int i = -1;
            while (++i < _adapter.getItemCount())
                _viewNotification.setNotificationToRead(_adapter.getItem(i));

            _viewNotification.setNoNewNotificationExist();
            _adapter.stopListening();

        }
    }
}

class NotificationActivity_Adapter extends FirebaseRecyclerAdapter<Notification, NotificationActivity_Adapter.NotificationViewHolder> {

    final private BasicActivity parent;
    final private Hashtable<String, Notification> hashTable = new Hashtable<>();
    private ArrayList<Notification> dataList;

    public ArrayList<Notification> getDataList() {
        return dataList;
    }

    public NotificationActivity_Adapter(BasicActivity ctx, @NonNull FirebaseRecyclerOptions<Notification> options) {
        super(options);
        parent = ctx;
    }

    @NonNull
    @Override
    public Notification getItem(int position) {
        if (dataList != null && dataList.size() > 0) {
            return dataList.get(position);
        }

        return null;
    }

    @Override
    public int getItemCount() {
        if (dataList != null)
            return dataList.size();

        return 0;
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();

        dataList = null;

        if (hashTable != null && hashTable.size() > 0)
            dataList = new ArrayList<Notification>(hashTable.values());

        Utilities.sortNotificationList(dataList);

        notifyDataSetChanged();

        if (parent != null)
            parent.finishProgressBar();
    }

    @Override
    public void onChildChanged(@NonNull ChangeEventType type,
                               @NonNull DataSnapshot snapshot,
                               int newIndex,
                               int oldIndex) {

        if (parent != null)
            parent.startProgressBar();

        if (snapshot.exists()) {

            Notification oObject = snapshot.getValue(Notification.class);

            switch (type) {
                case ADDED:
                case CHANGED:
                    hashTable.put(oObject.getNotificationKey(), oObject);
                    break;
                case REMOVED:
                    hashTable.remove(oObject.getNotificationKey());
                    break;
            }
        }

        super.onChildChanged(type, snapshot, newIndex, oldIndex);
    }

    @Override
    protected void onBindViewHolder(@NonNull NotificationViewHolder holder, int position, @NonNull Notification model) {
        holder.bind(model);
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_list_item, parent, false);

        return new NotificationViewHolder(view, parent.getContext());
    }

    class NotificationViewHolder extends RecyclerView.ViewHolder {

        TextInputLayout NotificationInputLayout;
        TextInputEditText NotificationInputText;
        Button NotificationBtnConfirm;
        Button NotificationBtnDelete;
        Notification oNotification;
        Context parentCtx;

        public NotificationViewHolder(View itemView, Context parent) {
            super(itemView);

            parentCtx = parent;
            NotificationInputLayout = itemView.findViewById(R.id.inputLayout_notification_text);
            NotificationInputText = itemView.findViewById(R.id.inputText_notification_text);
            NotificationBtnConfirm = itemView.findViewById(R.id.btn_notification_confirm);
            NotificationBtnConfirm.setOnClickListener(view -> {

                UserProfileViewModel.getInstance()
                        .confirmRelationshipRequest(parentCtx, oNotification);
            });

            NotificationBtnDelete = itemView.findViewById(R.id.btn_notification_delete);
            NotificationBtnDelete.setOnClickListener(view -> {

                UserProfileViewModel.getInstance()
                        .denyRelationshipRequest(parentCtx, oNotification);
            });
        }

        public void bind(Notification oObject) {
            if (oObject == null)
                return;

            oNotification = oObject;

            String strHintText = "";
            if (oNotification.getNotificationType() != null)
                strHintText += oNotification.getNotificationType().toString();

            if (oNotification.getNotificationTimestamp() != null)
                strHintText += " vom " + Utilities.formatDate("dd.MM.yyyy HH:mm",
                        oNotification.getNotificationTimestamp());

            NotificationInputLayout.setHint(strHintText);
            NotificationInputText.setText(oNotification.getNotificationText());

            if (oNotification.getNotificationType().equals(NotificationType.Anfrage)) {
                if (oNotification.isNotificationConfirmed()) {
                    NotificationBtnConfirm.setVisibility(View.GONE);
                    NotificationBtnDelete.setVisibility(View.GONE);
                } else {
                    NotificationBtnConfirm.setVisibility(View.VISIBLE);
                    NotificationBtnDelete.setVisibility(View.VISIBLE);
                }
            } else {
                NotificationBtnConfirm.setVisibility(View.GONE);
                NotificationBtnDelete.setVisibility(View.GONE);
            }

            if (!oNotification.isNotificationRead())
                NotificationInputText.setTypeface(null, Typeface.BOLD);
            else
                NotificationInputText.setTypeface(null, Typeface.NORMAL);
        }
    }
}