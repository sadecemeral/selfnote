package de.mh.selfnote.objects;

import android.graphics.Bitmap;
import android.net.Uri;

import com.google.firebase.database.Exclude;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class NoteImage {
    public static final String TABLE_NAME = "user_note_image_table";

    public String noteImageURL;
    public Enum<StatusKnz> noteImageStatusKnz;
    public Uri noteImageUri;
    public Bitmap noteImageBitmap;

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("noteImageURL", noteImageURL);
        result.put("noteImageStatusKnz", noteImageStatusKnz);
        result.put("noteImageUri", noteImageUri);

        return result;
    }

    public Bitmap getNoteImageBitmap() { return noteImageBitmap; }

    public void setNoteImageBitmap(Bitmap noteImageBitmap) { this.noteImageBitmap = noteImageBitmap; }

    public String getNoteImageURL() {
        return noteImageURL;
    }

    public void setNoteImageURL(String noteImageURL) {
        this.noteImageURL = noteImageURL;
    }

    public Enum<StatusKnz> getNoteImageStatusKnz() {
        return noteImageStatusKnz;
    }

    public void setNoteImageStatusKnz(Enum<StatusKnz> noteImageStatusKnz) {
        this.noteImageStatusKnz = noteImageStatusKnz;
    }

    public Uri getNoteImageUri() {
        return noteImageUri;
    }

    public void setNoteImageUri(Uri noteImageUri) {
        this.noteImageUri = noteImageUri;
    }

    public NoteImage(String noteImageURL) {
        this.noteImageURL = noteImageURL;
    }

    public NoteImage() { }

    public static Comparator<NoteImage> NoteImageStatusComparator = (n1, n2) -> {

        if (n1.getNoteImageStatusKnz() == null && n2.getNoteImageStatusKnz() != null)
            return -1;

        if(n1.getNoteImageStatusKnz() != null && n2.getNoteImageStatusKnz() == null)
            return 1;

        if(n1.getNoteImageStatusKnz() != null && n2.getNoteImageStatusKnz() != null)
            return n1.getNoteImageStatusKnz().toString().compareTo(n2.getNoteImageStatusKnz().toString());

        return 0;
    };
}
