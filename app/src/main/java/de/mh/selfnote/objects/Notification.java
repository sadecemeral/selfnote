package de.mh.selfnote.objects;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Notification {
    public static final String TABLE_NAME = "user_notification_table";

    public String notificationKey;
    public String notificationReceiverUID;
    public String notificationSenderUID;
    public String notificationText;
    public boolean notificationRead;
    public boolean notificationConfirmed;
    public NotificationType notificationType;
    public Long notificationTimestamp;

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("notificationKey", notificationKey);
        result.put("notificationReceiverUID", notificationReceiverUID);
        result.put("notificationSenderUID", notificationSenderUID);
        result.put("notificationText", notificationText);
        result.put("notificationRead", notificationRead);
        result.put("notificationConfirmed", notificationConfirmed);
        result.put("notificationType", notificationType);
        result.put("notificationTimestamp", notificationTimestamp);

        return result;
    }

    public Notification() {
    }

    public void setNotificationKey(String notificationKey) {
        this.notificationKey = notificationKey;
    }

    public void setNotificationReceiverUID(String notificationReceiverUID) {
        this.notificationReceiverUID = notificationReceiverUID;
    }

    public void setNotificationSenderUID(String notificationSenderUID) {
        this.notificationSenderUID = notificationSenderUID;
    }

    public void setNotificationText(String notificationText) {
        this.notificationText = notificationText;
    }

    public void setNotificationRead(boolean notificationRead) {
        this.notificationRead = notificationRead;
    }

    public void setNotificationConfirmed(boolean notificationConfirmed) {
        this.notificationConfirmed = notificationConfirmed;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public void setNotificationTimestamp(Long notificationTimestamp) {
        this.notificationTimestamp = notificationTimestamp;
    }

    public String getNotificationKey() {
        return notificationKey;
    }

    public String getNotificationReceiverUID() {
        return notificationReceiverUID;
    }

    public String getNotificationSenderUID() {
        return notificationSenderUID;
    }

    public String getNotificationText() {
        return notificationText;
    }

    public boolean isNotificationRead() {
        return notificationRead;
    }

    public boolean isNotificationConfirmed() {
        return notificationConfirmed;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public Long getNotificationTimestamp() {
        return notificationTimestamp;
    }

    public static Comparator<Notification> NotificationTimestampDESCComparator = new Comparator<Notification>() {

        @Override
        public int compare(Notification n1, Notification n2) {
            return n2.getNotificationTimestamp().compareTo(n1.getNotificationTimestamp());
        }
    };
}
