package de.mh.selfnote.objects;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;

import java.util.Comparator;

public class NoteAndCategory {

    @ColumnInfo(name = "NOTE_ID")
    private int noteID;
    @NonNull
    @ColumnInfo(name = "NOTE_TIMESTAMP")
    private Long noteTimestamp;
    @ColumnInfo(name = "NOTE_TEXT")
    private String noteText;
    @ColumnInfo(name = "NOTE_TITLE")
    private String noteTitle;
    @NonNull
    @ColumnInfo(name = "NOTE_CATEGORY_ID")
    private int noteCategoryID;
    @ColumnInfo(name = "NOTE_KEYWORD")
    private String noteKeyword;
    @ColumnInfo(name = "NOTE_IMAGE")
    private byte[] noteImage;
    @ColumnInfo(name = "NOTE_PERSONAL")
    private boolean notePersonal;
    @ColumnInfo(name = "NOTE_PINNED")
    private boolean notePinned;
    @ColumnInfo(name = "NOTE_RATING")
    private float noteRating;

    @ColumnInfo(name = "CATEGORY_ID")
    private int categoryID;
    @NonNull
    @ColumnInfo(name = "CATEGORY_NAME")
    private String categoryName;
    @ColumnInfo(name = "CATEGORY_COLOR_INT")
    private int categoryColor;
    @ColumnInfo(name = "CATEGORY_ENABLED")
    private boolean categoryEnabled;

    public int getNoteID(){ return noteID; }
    public Long getNoteTimestamp(){ return noteTimestamp; }
    public String getNoteTitle(){ return noteTitle; }
    public String getNoteText(){ return noteText; }
    public int getNoteCategoryID(){ return noteCategoryID; }
    public String getNoteKeyword(){ return noteKeyword; }
    public byte[] getNoteImage(){ return noteImage; }
    public boolean isNotePersonal(){ return notePersonal; }
    public boolean isNotePinned(){ return notePinned; }
    public float getNoteRating(){ return noteRating; }
    public int getCategoryID(){ return categoryID; }
    public String getCategoryName(){ return categoryName; }
    public int getCategoryColor(){ return categoryColor; }
    public boolean isCategoryEnabled() { return categoryEnabled; }

    public void setNoteID(int id){ noteID = id; }
    public void setNoteTimestamp(Long stampTime){ noteTimestamp = stampTime; }
    public void setNoteTitle(String strTitle){ noteTitle = strTitle; }
    public void setNoteText(String strText){ noteText = strText; }
    public void setNoteCategoryID(int iCategoryID){ noteCategoryID = iCategoryID; }
    public void setNoteKeyword(String strKeyword){ noteKeyword = strKeyword; }
    public void setNoteImage(byte[] bImage){ noteImage = bImage; }
    public void setNotePersonal(boolean bPersonal){ notePersonal = bPersonal; }
    public void setNotePinned(boolean bPinned){ notePinned = bPinned; }
    public void setNoteRating(float rating){ noteRating = rating; }
    public void setCategoryID(int iValue){ categoryID = iValue; }
    public void setCategoryName(String strValue){ categoryName = strValue; }
    public void setCategoryColor(int iValue){ categoryColor = iValue; }
    public void setCategoryEnabled(boolean isEnabled){ categoryEnabled = isEnabled; }

    public static Comparator<NoteAndCategory> NoteTimestampDESCComparator = new Comparator<NoteAndCategory>() {

        @Override
        public int compare(NoteAndCategory n1, NoteAndCategory n2) {

            int iEqual;
            iEqual = Boolean.compare(n2.isNotePinned(), n1.isNotePinned());

            if(iEqual == 0)
                iEqual = n2.getNoteTimestamp().compareTo(n1.getNoteTimestamp());

            return iEqual;
        }
    };

    public static Comparator<NoteAndCategory> NoteTitleComparator = new Comparator<NoteAndCategory>() {

        @Override
        public int compare(NoteAndCategory n1, NoteAndCategory n2) {

            int iEqual;
            iEqual = Boolean.compare(n2.isNotePinned(), n1.isNotePinned());

            if(iEqual == 0)
                iEqual = n1.getNoteTitle().compareTo(n2.getNoteTitle());

            return iEqual;
        }
    };

    public static Comparator<NoteAndCategory> NoteCategoryComparator = new Comparator<NoteAndCategory>() {

        @Override
        public int compare(NoteAndCategory n1, NoteAndCategory n2) {

            int iEqual;
            iEqual = Boolean.compare(n2.isNotePinned(), n1.isNotePinned());

            if(iEqual == 0)
                iEqual = n1.getNoteCategoryID() - n2.getNoteCategoryID();

            return iEqual;
        }
    };

    public static Comparator<NoteAndCategory> NoteTitleCategoryComparator = new Comparator<NoteAndCategory>() {

        @Override
        public int compare(NoteAndCategory n1, NoteAndCategory n2) {

            int iEqual;
            iEqual = Boolean.compare(n2.isNotePinned(), n1.isNotePinned());

            if(iEqual == 0)
                iEqual = n1.getNoteTitle().compareTo(n2.getNoteTitle());

            if(iEqual == 0)
                iEqual = n1.getNoteCategoryID() - n2.getNoteCategoryID();

            return iEqual;
        }
    };

    public static Comparator<NoteAndCategory> NoteCategoryTitleComparator = new Comparator<NoteAndCategory>() {

        @Override
        public int compare(NoteAndCategory n1, NoteAndCategory n2) {

            int iEqual;
            iEqual = Boolean.compare(n2.isNotePinned(), n1.isNotePinned());

            if(iEqual == 0)
                iEqual = n1.getNoteCategoryID() - n2.getNoteCategoryID();

            if(iEqual == 0)
                iEqual = n1.getNoteTitle().compareTo(n2.getNoteTitle());

            return iEqual;
        }
    };
}
