package de.mh.selfnote.objects;

public class UserRelationship {
    final public static String TABLE_NAME = "user_relationship_table";

    public String userRelation_Key;
    public String contactUID;
    public boolean isFriend;
    public boolean isClose;
    public boolean isFollowing;
    public boolean isRequested;

    public void setUserRelation_Key(String userRelation_Key) {
        this.userRelation_Key = userRelation_Key;
    }

    public void setContactUID(String strContactUID) {
        contactUID = strContactUID;
    }

    public void setFriend(boolean friend) {
        isFriend = friend;
    }

    public void setClose(boolean close) {
        isClose = close;
    }

    public void setFollowing(boolean following) { isFollowing = following; }

    public void setRequested(boolean requested) { isRequested = requested; }

    public String getUserRelation_Key() {
        return userRelation_Key;
    }

    public String getContactUID() {
        return contactUID;
    }

    public boolean isFriend() {
        return isFriend;
    }

    public boolean isClose() {
        return isClose;
    }

    public boolean isFollowing() {
        return isFollowing;
    }

    public boolean isRequested() { return isRequested; }

    public UserRelationship(String contactUID,
                            boolean isFriend, boolean isClose, boolean isFollowing,
                            boolean isRequested) {
        this.contactUID = contactUID;
        this.isFriend = isFriend;
        this.isClose = isClose;
        this.isFollowing = isFollowing;
        this.isRequested = isRequested;
    }

    public UserRelationship() {
    }
}
