package de.mh.selfnote.objects;

public enum NotificationType {
    Nachricht,
    Anfrage,
    Bestätigung,
    Ablehnung,
    Freundschaftskündigung
}
