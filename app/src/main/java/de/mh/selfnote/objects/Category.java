package de.mh.selfnote.objects;

public class Category {

    public static final String TABLE_NAME = "user_category_table";

    public String categoryKey;
    public String categoryName;
    public int categoryColor;
    public boolean categoryEnabled;

    public Category() {
    }

    public Category(String CategoryName, int CategoryColor, boolean isEnabled) {
        categoryName = CategoryName;
        categoryColor = CategoryColor;
        categoryEnabled = isEnabled;
    }

    public String getCategoryKey() {
        return categoryKey;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public int getCategoryColor() {
        return categoryColor;
    }

    public boolean isCategoryEnabled() {
        return categoryEnabled;
    }

    public void setCategoryKey(String strKey) {
        categoryKey = strKey;
    }

    public void setCategoryName(String strValue) {
        categoryName = strValue;
    }

    public void setCategoryColor(int iValue) {
        categoryColor = iValue;
    }

    public void setCategoryEnabled(boolean isEnabled) {
        categoryEnabled = isEnabled;
    }
}