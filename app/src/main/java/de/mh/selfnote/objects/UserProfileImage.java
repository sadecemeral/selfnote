package de.mh.selfnote.objects;

import android.graphics.Bitmap;
import android.net.Uri;

public class UserProfileImage {
    public static final String TABLE_NAME = "user_profile_image_table";

    public String profileImageURL;
    public Uri profileImageUri;
    public Bitmap profileImageBitmap;

    public void setProfileImageURL(String profileImageURL) {
        this.profileImageURL = profileImageURL;
    }

    public void setProfileImageUri(Uri profileImageUri) {
        this.profileImageUri = profileImageUri;
    }

    public void setProfileImageBitmap(Bitmap profileImageBitmap) {
        this.profileImageBitmap = profileImageBitmap;
    }

    public String getProfileImageURL() {
        return profileImageURL;
    }

    public Uri getProfileImageUri() {
        return profileImageUri;
    }

    public Bitmap getProfileImageBitmap() {
        return profileImageBitmap;
    }

    public UserProfileImage() {
    }
}