package de.mh.selfnote.objects;

public class Permission {
    public static final int PERMISSION_REQUEST_CAMERA = 0;
    public static final int PERMISSION_REQUEST_READ_EXTERNAL_STORAGE = 1;
    public static final int PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 2;

    private final String permissionName;
    private final int requestCode;

    public Permission(String permission, int requestCode){
        this.permissionName = permission;
        this.requestCode = requestCode;
    }

    public String getPermissionName(){ return permissionName; }
    public int getRequestCode(){ return requestCode; }
}