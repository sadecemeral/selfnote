package de.mh.selfnote.objects;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class UserProfile {
    final public static String TABLE_NAME = "user_profile_table";

    public String firebaseUID;
    public String userID;
    public String userName;
    public String userEmail;
    public Long userBirthday;
    public String userPhotoURL;
    public String userRegistrationToken;
    public Long userRegistrationTime;

    public UserProfile() { }
    public void setUserRegistrationToken(String strToken) {
        this.userRegistrationToken = strToken;
    }
    public void setUserRegistrationTime(Long lTimestamp) { this.userRegistrationTime = lTimestamp; }
    public void setFirebaseUID(String firebaseUID) { this.firebaseUID = firebaseUID; }
    public void setUserID(String userID) { this.userID = userID; }
    public void setUserName(String userName) { this.userName = userName; }
    public void setUserEmail(String userEmail) { this.userEmail = userEmail; }
    public void setUserBirthday(Long userBirthday) { this.userBirthday = userBirthday; }
    public void setUserPhotoURL(String userPhotoURL) { this.userPhotoURL = userPhotoURL; }

    public String getFirebaseUID() { return firebaseUID; }
    public String getUserID() { return userID; }
    public String getUserName() { return userName; }
    public String getUserEmail() { return userEmail; }
    public Long getUserBirthday() { return userBirthday; }
    public String getUserPhotoURL() { return userPhotoURL; }
    public String getUserRegistrationToken() { return userRegistrationToken; }
    public Long getUserRegistrationTime() { return userRegistrationTime; }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("firebaseUID", firebaseUID);
        result.put("userID", userID);
        result.put("userName", userName);
        result.put("userEmail", userEmail);
        result.put("userBirthday", userBirthday);
        result.put("userPhotoURL", userPhotoURL);
        result.put("userRegistrationToken", userRegistrationToken);
        result.put("userRegistrationTime", userRegistrationTime);

        return result;
    }

    public void deserialize(UserProfile oObject) {
        if(oObject == null)
            return;

        setUserID(oObject.getUserID());
        setUserBirthday(oObject.getUserBirthday());
        setUserEmail(oObject.getUserEmail());
        setUserName(oObject.getUserName());
        setUserPhotoURL(oObject.getUserPhotoURL());
        setFirebaseUID(oObject.getFirebaseUID());
        setUserRegistrationToken(oObject.getUserRegistrationToken());
        setUserRegistrationTime(oObject.getUserRegistrationTime());
    }
}
