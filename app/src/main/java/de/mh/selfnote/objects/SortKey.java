package de.mh.selfnote.objects;

public class SortKey {

    public static final String TABLE_NAME = "user_sortkey_table";

    public String sortKeyKey;
    public String sortKeyName;
    public String sortKeyNoteCol;
    public int sortKeyOrder;
    public boolean sortKeyEnabled;

    public final static String SORTKEY_TIMESTAMP = "NOTE_TIMESTAMP";

    public SortKey() {
    }

    public SortKey(String SortKeyName, String SortKeyNoteCol, int SortKeyOrder, boolean isEnabled) {
        sortKeyName = SortKeyName;
        sortKeyNoteCol = SortKeyNoteCol;
        sortKeyOrder = SortKeyOrder;
        sortKeyEnabled = isEnabled;
    }

    public String getSortKeyKey() {
        return sortKeyKey;
    }

    public String getSortKeyName() {
        return sortKeyName;
    }

    public String getSortKeyNoteCol() {
        return sortKeyNoteCol;
    }

    public int getSortKeyOrder() {
        return sortKeyOrder;
    }

    public boolean isSortKeyEnabled() {
        return sortKeyEnabled;
    }

    public void setSortKeyKey(String strKey) {
        sortKeyKey = strKey;
    }

    public void setSortKeyName(String strValue) {
        sortKeyName = strValue;
    }

    public void setSortKeyNoteCol(String SortKeyNoteCol) {
        sortKeyNoteCol = SortKeyNoteCol;
    }

    public void setSortKeyOrder(int iValue) {
        sortKeyOrder = iValue;
    }

    public void setSortKeyEnabled(boolean isEnabled) {
        sortKeyEnabled = isEnabled;
    }
}
