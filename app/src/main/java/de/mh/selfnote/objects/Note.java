package de.mh.selfnote.objects;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Note {

    public static final String TABLE_NAME = "user_note_table";

    public String noteKey;
    public Long noteTimestamp;
    public String noteText;
    public String noteTitle;
    public Category noteCategory;
    public String noteKeyword;
    public boolean notePersonal;
    public boolean notePinned;
    public float noteRating;
    public String creatorUID;
    public HashMap<String, Boolean> readerListUID;
    public HashMap<String, Boolean> notePinnedByUID;
    public ArrayList<String> noteImageListURL;

    public Note() {
    }

    public String getNoteKey() {
        return noteKey;
    }

    public Long getNoteTimestamp() {
        return noteTimestamp;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public String getNoteText() {
        return noteText;
    }

    public Category getNoteCategory() {
        return noteCategory;
    }

    public String getNoteCategoryName() {
        if (noteCategory != null)
            return noteCategory.getCategoryName();

        return null;
    }

    public String getNoteCategoryKey() {
        if (noteCategory != null)
            return noteCategory.getCategoryKey();

        return null;
    }

    public String getNoteKeyword() {
        return noteKeyword;
    }

    public boolean isNotePersonal() {
        return notePersonal;
    }

    public boolean isNotePinned() {
        return notePinned;
    }

    public float getNoteRating() {
        return noteRating;
    }

    public String getCreatorUID() {
        return creatorUID;
    }

    public HashMap<String, Boolean> getReaderListUID() {
        return readerListUID;
    }

    public HashMap<String, Boolean> getNotePinnedByUID() {
        return notePinnedByUID;
    }

    public ArrayList<String> getNoteImageListURL() {
        return noteImageListURL;
    }

    public void setNoteKey(String strKey) {
        noteKey = strKey;
    }

    public void setNoteTimestamp(Long stampTime) {
        noteTimestamp = stampTime;
    }

    public void setNoteTitle(String strTitle) {
        noteTitle = strTitle;
    }

    public void setNoteText(String strText) {
        noteText = strText;
    }

    public void setNoteCategory(Category oCategory) {
        noteCategory = oCategory;
    }

    public void setNoteKeyword(String strKeyword) {
        noteKeyword = strKeyword;
    }

    public void setNotePersonal(boolean bPersonal) {
        notePersonal = bPersonal;
    }

    public void setNotePinned(boolean bPinned) {
        notePinned = bPinned;
    }

    public void setNoteRating(float rating) {
        noteRating = rating;
    }

    public void setCreatorUID(String strUID) {
        creatorUID = strUID;
    }

    public void setReaderListUID(HashMap<String, Boolean> readerListUID) {
        this.readerListUID = readerListUID;
    }

    public void setNotePinnedByUID(HashMap<String, Boolean> notePinnedByUID) {
        this.notePinnedByUID = notePinnedByUID;
    }

    public void setNoteImageListURL(ArrayList<String> noteImageListURL) {
        this.noteImageListURL = noteImageListURL;
    }

    public void addReaderToList(String strReaderUID, Boolean bValue) {
        if (readerListUID == null)
            readerListUID = new HashMap<>();

        if (!readerListUID.containsKey(strReaderUID))
            readerListUID.put(strReaderUID, bValue);
    }

    public void removeReaderUIDFromList(String strReaderUID) {
        if (readerListUID == null || strReaderUID == null)
            return;

        if (readerListUID.containsKey(strReaderUID))
            readerListUID.remove(strReaderUID);
    }

    public void setNotePersonal() {
        if (readerListUID == null)
            readerListUID = new HashMap<>();

        readerListUID.clear();
        readerListUID.put(this.getCreatorUID(), true);

        this.setNotePersonal(true);
    }

    public void addUIDToPinnedNoteList(String strUserID, Boolean bValue) {
        if (notePinnedByUID == null)
            notePinnedByUID = new HashMap<>();

        if (!notePinnedByUID.containsKey(strUserID))
            notePinnedByUID.put(strUserID, bValue);
    }

    public void removeUIDFromPinnedNoteList(String strUserID) {
        if (notePinnedByUID == null || strUserID == null)
            return;

        if (notePinnedByUID.containsKey(strUserID))
            notePinnedByUID.remove(strUserID);
    }

    public boolean isNotePinnedByUser(String strUserID) {
        if (notePinnedByUID == null || strUserID == null)
            return false;

        return notePinnedByUID.containsKey(strUserID);
    }

    public void changeNoteImageListURL(String noteImageURL, StatusKnz statusKnz) {
        if (this.noteImageListURL == null)
            this.noteImageListURL = new ArrayList<>();

        switch (statusKnz) {
            case DELETE:
                if (noteImageListURL.contains(noteImageURL))
                    noteImageListURL.remove(noteImageURL);
                break;
            case INSERT:
                if (!noteImageListURL.contains(noteImageURL))
                    noteImageListURL.add(noteImageURL);
                break;
        }
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("noteKey", noteKey);
        result.put("noteTimestamp", noteTimestamp);
        result.put("noteCategory", noteCategory);
        result.put("noteKeyword", noteKeyword);
        result.put("notePersonal", notePersonal);
        result.put("notePinned", notePinned);
        result.put("noteRating", noteRating);
        result.put("noteText", noteText);
        result.put("noteTitle", noteTitle);
        result.put("creatorUID", creatorUID);
        result.put("readerListUID", readerListUID);
        result.put("noteImageListURL", noteImageListURL);
        result.put("notePinnedByUID", notePinnedByUID);

        return result;
    }

    public static Comparator<Note> NoteTimestampDESCComparator = (n1, n2) -> {

        int iEqual;
        iEqual = Boolean.compare(n2.isNotePinned(), n1.isNotePinned());

        if (iEqual == 0)
            iEqual = n2.getNoteTimestamp().compareTo(n1.getNoteTimestamp());

        return iEqual;
    };

    public static Comparator<Note> NoteTitleComparator = (n1, n2) -> {

        int iEqual;
        iEqual = Boolean.compare(n2.isNotePinned(), n1.isNotePinned());

        if (iEqual == 0)
            iEqual = n1.getNoteTitle().compareTo(n2.getNoteTitle());

        return iEqual;
    };

    public static Comparator<Note> NoteCategoryComparator = (n1, n2) -> {

        int iEqual;
        iEqual = Boolean.compare(n2.isNotePinned(), n1.isNotePinned());

        if (iEqual == 0)
            iEqual = n1.getNoteCategory().getCategoryName().compareTo(n2.getNoteCategory().getCategoryName());

        return iEqual;
    };
}