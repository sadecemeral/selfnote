package de.mh.selfnote.extended;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.util.ArrayList;
import java.util.Map;

import de.mh.selfnote.objects.UserRelationship;
import de.mh.selfnote.toolbox.AppManager;
import de.mh.selfnote.toolbox.BetterActivityResult;
import de.mh.selfnote.toolbox.ImagePicker;


public class BasicActivity extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback,
        NoticeDialogFragment.NoticeDialogListener {

    protected ProgressBar mProgressBar;
    protected ScrollView mScrollView;
    protected final BetterActivityResult<Intent, ActivityResult> activityLauncher = BetterActivityResult.registerActivityForResult(this);

    public BetterActivityResult<Intent, ActivityResult> getActivityLauncher() {
        return activityLauncher;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppManager.getAppManager().addActivity(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        finishProgressBar();
    }

    public void startProgressBar() {
        if (mProgressBar != null && mProgressBar.getVisibility() != View.VISIBLE) {
            mProgressBar.setVisibility(View.VISIBLE);

            if (mScrollView != null)
                mScrollView.scrollTo(mProgressBar.getTop(), mProgressBar.getTop());
        }
    }

    public void finishProgressBar() {
        if (mProgressBar != null)
            mProgressBar.setVisibility(View.GONE);
    }

    //check permission
    private final ActivityResultLauncher<String[]> requestPermissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestMultiplePermissions(), isGranted -> {

        ArrayList<String> permissionNotGranted = new ArrayList<>();
        for (Map.Entry map : isGranted.entrySet()) {
            if (map.getValue().equals(false))
                permissionNotGranted.add((String) map.getKey());
            else {
                allowPermission(map.getKey().toString());
            }
        }
    });

    protected void checkPermissions(String[] permissions) {
        if (permissions == null || permissions.length < 1)
            return;

        ArrayList<String> permissionNotGranted = new ArrayList<>();
        for (String permission : permissions) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                permissionNotGranted.add(permission);
            } else {
                allowPermission(permission);
            }
        }
        if (!permissionNotGranted.isEmpty()) {
            requestPermissionLauncher.launch(permissionNotGranted.toArray(new String[]{}));
        }
    }

    private void allowPermission(String permission) {
        if (TextUtils.equals(permission, Manifest.permission.CAMERA))
            ImagePicker.allowCameraUse();
        else if (TextUtils.equals(permission, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            ImagePicker.allowWriteExternalStorageUse();
        else if (TextUtils.equals(permission, Manifest.permission.READ_EXTERNAL_STORAGE))
            ImagePicker.allowReadExternalStorageUse();
        else if (TextUtils.equals(permission, Manifest.permission.READ_MEDIA_IMAGES))
            ImagePicker.allowReadExternalStorageUse();
    }

    protected void showNoticeDialog(String strTitle, String strMessage) {
        // Create an instance of the dialog fragment and show it
        NoticeDialogFragment dialog = new NoticeDialogFragment(strTitle, strMessage);
        dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
    }

    public void showNoticeDialog(String strTitle, String strMessage, int[] iBtn) {
        // Create an instance of the dialog fragment and show it
        NoticeDialogFragment dialog = new NoticeDialogFragment(strTitle, strMessage, iBtn);
        dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
    }

    public void showNoticeDialog(String strTitle, String strMessage, int[] iBtn, UserRelationship relationship) {
        // Create an instance of the dialog fragment and show it
        NoticeDialogFragment dialog = new NoticeDialogFragment(strTitle, strMessage, iBtn, relationship);
        dialog.show(getSupportFragmentManager(), "NoticeDialogFragment");
    }

    @Override
    public void onDialogPositiveClick(NoticeDialogFragment dialog, int iTextId) {
        if (dialog.isShowRequestPermissionRationale())
            ActivityCompat.requestPermissions(this,
                    new String[]{dialog.permission.getPermissionName()},
                    dialog.permission.getRequestCode());
    }

    @Override
    public void onDialogNegativeClick(NoticeDialogFragment dialog, int iTextId) {

    }
}