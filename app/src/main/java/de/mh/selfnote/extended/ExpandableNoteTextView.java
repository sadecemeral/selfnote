package de.mh.selfnote.extended;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ms.square.android.expandabletextview.ExpandableTextView;

import javax.annotation.Nullable;

import de.mh.selfnote.R;

public class ExpandableNoteTextView extends ExpandableTextView {

    TextView NoteTextView;
    private boolean mCollapsed = true;
    private Drawable mExpandDrawable;
    private Drawable mCollapseDrawable;

    public ExpandableNoteTextView(Context context) {
        super(context);
        mExpandDrawable = context.getDrawable(R.drawable.ic_expand_note_text);
        mCollapseDrawable = context.getDrawable(R.drawable.ic_collapse_note_text);
    }

    public ExpandableNoteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mExpandDrawable = context.getDrawable(R.drawable.ic_expand_note_text);
        mCollapseDrawable = context.getDrawable(R.drawable.ic_collapse_note_text);
    }

    public ExpandableNoteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mExpandDrawable = context.getDrawable(R.drawable.ic_expand_note_text);
        mCollapseDrawable = context.getDrawable(R.drawable.ic_collapse_note_text);
    }

    @Override
    public void onClick(View view) {
        if (mButton.getVisibility() != View.VISIBLE) {
            return;
        }

        mCollapsed = !mCollapsed;
        mButton.setImageDrawable(mCollapsed ? mExpandDrawable : mCollapseDrawable);
        NoteTextView.setVisibility(mCollapsed ? View.GONE : View.VISIBLE);

        if (NoteTextView.getVisibility() == View.VISIBLE)
            setFullNoteText(super.getText());
    }

    public void setText(@Nullable CharSequence text, @NonNull SparseBooleanArray collapsedStatus,
                        int position,
                        TextView textView) {
        super.setText(text, collapsedStatus, position);
        NoteTextView = textView;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        mButton.setImageDrawable(mCollapsed ? mExpandDrawable : mCollapseDrawable);
        NoteTextView.setVisibility(mCollapsed ? View.GONE : View.VISIBLE);

        if (NoteTextView.getVisibility() == View.VISIBLE)
            setFullNoteText(super.getText());
    }

    private void setFullNoteText(CharSequence text) {
        if (NoteTextView == null || mTv == null || mTv.getLayout() == null)
            return;

        SpannableStringBuilder value = new SpannableStringBuilder(text);
        int iLen = value.length();
        int iMaxLines = mTv.getMaxLines();
        int iLineCount = mTv.getLineCount();
        int end = 0;
        StringBuilder result = new StringBuilder("");

        if (iLineCount < iMaxLines) {
            end = mTv.getLayout().getLineEnd(iLineCount - 1);
        } else {
            end = mTv.getLayout().getLineEnd(iMaxLines - 1);
        }

        if (iLen > end) {
            result.append(value.subSequence(end, iLen));
        }

        NoteTextView.setText(result);
        NoteTextView.setVisibility(TextUtils.isEmpty(result) ? View.GONE : View.VISIBLE);
    }
}