package de.mh.selfnote.extended;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import de.mh.selfnote.R;
import de.mh.selfnote.objects.Permission;
import de.mh.selfnote.objects.UserRelationship;

public class NoticeDialogFragment extends DialogFragment {

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(NoticeDialogFragment dialog, int iTextId);
        public void onDialogNegativeClick(NoticeDialogFragment dialog, int iTextId);
    }

    // Use this instance of the interface to deliver action events
    NoticeDialogListener listener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (NoticeDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(getActivity().toString()
                    + " must implement NoticeDialogListener");
        }
    }

    AlertDialog.Builder builder;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        builder = new AlertDialog.Builder(getActivity());
        builder
                .setTitle(strTitle)
                .setMessage(strMessage)
                .setPositiveButton(iBtnPosTextId, (dialog, id) -> {
                    // Send the negative button event back to the host activity
                    listener.onDialogPositiveClick(NoticeDialogFragment.this, iBtnPosTextId);
                })
                .setNegativeButton(iBtnNegTextId, (dialog, id) -> {
                    // Send the positive button event back to the host activity
                    listener.onDialogNegativeClick(NoticeDialogFragment.this, iBtnNegTextId);
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    String strTitle;
    String strMessage;
    boolean bShowRequestPermissionRationale = false;
    Permission permission;
    int iBtnNegTextId = R.string.btn_Cancel;
    int iBtnPosTextId = R.string.btn_OK;
    public UserRelationship userRelationship;

    public NoticeDialogFragment(String title, String message){
        super();
        strTitle = title;
        strMessage = message;
    }

    public NoticeDialogFragment(String title, String message,
                                int[] iButtonTextId){
        super();
        strTitle = title;
        strMessage = message;
        if(iButtonTextId == null || iButtonTextId.length != 2)
            return;
        iBtnPosTextId = iButtonTextId[0];
        iBtnNegTextId = iButtonTextId[1];
    }

    public NoticeDialogFragment(String title, String message,
                                int[] iButtonTextId, UserRelationship relationship){
        super();
        strTitle = title;
        strMessage = message;
        if(iButtonTextId == null || iButtonTextId.length != 2)
            return;
        iBtnPosTextId = iButtonTextId[0];
        iBtnNegTextId = iButtonTextId[1];
        userRelationship = relationship;
    }

    public NoticeDialogFragment(String title, String message, Permission objPermission){
        super();
        strTitle = title;
        strMessage = message;
        permission = objPermission;
        bShowRequestPermissionRationale = true;
    }

    public boolean isShowRequestPermissionRationale(){
        return bShowRequestPermissionRationale;
    }
}