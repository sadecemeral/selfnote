package de.mh.selfnote.viewModel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.auth.api.identity.Identity;
import com.google.android.gms.auth.api.identity.SignInClient;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import de.mh.selfnote.EditUserProfileActivity;
import de.mh.selfnote.LoginActivity;
import de.mh.selfnote.R;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.objects.Note;
import de.mh.selfnote.objects.Notification;
import de.mh.selfnote.objects.UserProfile;
import de.mh.selfnote.objects.UserProfileImage;
import de.mh.selfnote.objects.UserRelationship;
import de.mh.selfnote.service.UserNotificationService;
import de.mh.selfnote.toolbox.AppManager;
import de.mh.selfnote.toolbox.Utilities;

public class UserProfileViewModel extends MainViewModel {

    private static UserProfileViewModel INSTANCE;

    private static UserProfile userProfile;

    private static CategoryViewModel mCategoryView;
    private static SortViewModel mSortView;
    private static NoteViewModel mNoteView;
    private static NotificationViewModel mNotificationView;
    private static Hashtable<String, UserProfile> userProfileList = new Hashtable<>();
    private static Hashtable<String, UserRelationship> userRelationshipList = new Hashtable<>();

    final private static FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
    private static FirebaseUser mFirebaseUser;
    private static AuthCredential mAuthCredential;

    final private DatabaseReference userProfileDBRef = mDatabase.child(UserProfile.TABLE_NAME);
    final private DatabaseReference allUserDBRef = mDatabase.child("user_table");
    private DatabaseReference userDBRef;
    private DatabaseReference userRelationshipDBRef;
    private StorageReference userImgStRef;
    private Menu mainMenu;
    private ValueEventListener userRelationshipValueEventListener;

    private SignInClient oneTapClient;

    public SignInClient createOneTapClient(Context parentCtx) {
        oneTapClient = Identity.getSignInClient(parentCtx);
        return oneTapClient;
    }


    private UserProfileViewModel() {
        super();
    }

    public static UserProfileViewModel getInstance() {
        if (INSTANCE == null)
            INSTANCE = new UserProfileViewModel();

        return INSTANCE;
    }

    public static CategoryViewModel getCategoryViewModel() {
        return mCategoryView;
    }

    public static SortViewModel getSortViewModel() {
        return mSortView;
    }

    public static NoteViewModel getNoteViewModel() {
        return mNoteView;
    }

    public static NotificationViewModel getNotificationViewModel() {
        return mNotificationView;
    }

    /**
     * @param strKey = FirebaseUID
     * @return
     */
    public StorageReference getUserImageReference(String strKey) {
        if (userImgStRef == null)
            userImgStRef = super.getUserImageReference(strKey);

        return super.getUserImageReference(strKey);
    }

    public void addReaderUIDToNote(Note oObject) {
        if (oObject == null)
            return;

        oObject.addReaderToList(getLoggedUserID(), true);

        if (oObject.isNotePersonal())
            return;

        if (userRelationshipList == null || userRelationshipList.size() < 1)
            return;

        Enumeration<String> keyEnum = userRelationshipList.keys();
        while (keyEnum.hasMoreElements()) {
            String strUID = keyEnum.nextElement();
            UserRelationship relationship = userRelationshipList.get(strUID);

            if (relationship.isFriend())
                oObject.addReaderToList(strUID, true);
        }
    }

    private UserRelationship getUserRelationship(String strContactUID) {
        if (strContactUID == null)
            return null;

        if (userRelationshipList != null && userRelationshipList.containsKey(strContactUID))
            return userRelationshipList.get(strContactUID);

        return null;
    }

    public static UserProfile getUserProfile(String strUID) {
        if (strUID == null ||
                (userProfile != null && TextUtils.equals(strUID, userProfile.getFirebaseUID())))
            return userProfile;

        if (userProfileList != null && userProfileList.containsKey(strUID))
            return userProfileList.get(strUID);

        return null;
    }

    public static boolean isLoggedUser(String strUserID) {
        String uID = (strUserID != null && !strUserID.isEmpty()) ? strUserID : "";

        return (TextUtils.equals(getLoggedUserID(), uID)) ? true : false;
    }

    public static String getLoggedUserID() {
        return mFirebaseUser != null ? mFirebaseUser.getUid() : null;
    }

    private void logoutUser() {
        if (mFirebaseAuth != null) {
            mFirebaseAuth.signOut();
            mFirebaseUser = null;

            String authCredentialSignInMethod = mAuthCredential != null ? mAuthCredential.getSignInMethod() : "";
            if (TextUtils.equals(authCredentialSignInMethod, GoogleAuthProvider.GOOGLE_SIGN_IN_METHOD)) {
                oneTapClient.signOut();
            }

            mAuthCredential = null;
        }
    }

    public void clearUserProfile(Context parent, boolean isLogout) {
        Utilities.resetLoginRemember(parent);
        Utilities.resetSignInToken(parent);
        Utilities.resetShowOneTapUI(parent);

        userProfile = null;

        if (mCategoryView != null)
            mCategoryView.initInstance();
        if (mSortView != null)
            mSortView.initInstance();
        if (mNoteView != null)
            mNoteView.initInstance();
        if (mNotificationView != null)
            mNotificationView.initInstance();

        userProfileList.clear();
        userRelationshipList.clear();

        userImgStRef = null;
        mainMenu = null;
        userDBRef = null;
        if (userRelationshipDBRef != null && userRelationshipValueEventListener != null)
            userRelationshipDBRef.removeEventListener(userRelationshipValueEventListener);

        if (isLogout)
            logoutUser();
    }

    public void setMainMenu(Context parent, Menu menu) {
        if (menu != null)
            mainMenu = menu;

        setUserProfileIcon(parent);
        setNotificationIcon(mNotificationView != null ? mNotificationView.isExistNewNotification() : false);
    }

    private void setUserProfileIcon(Context parent) {
        MenuItem menuItem = mainMenu != null ? mainMenu.findItem(R.id.MenuItemUserProfile) : null;

        Glide.with(parent)
                .asDrawable()
                .circleCrop()
                .load(
                        (userProfile != null && !userProfile.getUserPhotoURL().isEmpty()) ? userProfile.getUserPhotoURL() : R.drawable.ic_user_profile
                )
                .placeholder(R.drawable.ic_user_profile)
                .into(new CustomTarget<Drawable>() {

                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        if (menuItem != null)
                            menuItem.setIcon(resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                    }
                });
    }

    public void setNotificationIcon(boolean bActive) {
        if (mainMenu == null)
            return;

        int iId = bActive ? R.drawable.ic_user_notifications_active : R.drawable.ic_user_notifications_none;

        mainMenu.findItem(R.id.MenuItemUserNotifications).setIcon(iId);
    }

    public String getRelationshipState(Context parent, String strContactUID) {
        UserRelationship oRelationship = getUserRelationship(strContactUID);
        if (oRelationship == null)
            return null;

        if (oRelationship.isFriend())
            return parent.getString(R.string.textview_user_relationship_state_friend);
        else if (oRelationship.isRequested())
            return parent.getString(R.string.textview_user_relationship_state_friendship_requested);
        else if (oRelationship.isClose())
            return parent.getString(R.string.textview_user_relationship_state_close_friend);
        else if (oRelationship.isFollowing)
            return parent.getString(R.string.textview_user_relationship_state_followed);
        else
            return null;
    }

    public FirebaseRecyclerOptions<UserRelationship> getRelationshipOptions(BasicActivity parent) {
        if (userRelationshipDBRef == null)
            return null;

        return new FirebaseRecyclerOptions.Builder<UserRelationship>()
                .setQuery(userRelationshipDBRef, UserRelationship.class)
                .setLifecycleOwner(parent)
                .build();
    }

    public FirebaseRecyclerOptions<UserProfile> getResultUserOptions(Fragment parent, String strSearchKey) {

        if (strSearchKey == null || strSearchKey.length() < 1)
            return null;

        Query resultUserQuery;

        if (strSearchKey.startsWith("*"))
            resultUserQuery = userProfileDBRef.orderByChild("userID");
        else
            resultUserQuery = userProfileDBRef.orderByChild("userID")
                    .startAt(strSearchKey)
                    .endAt(strSearchKey + "uf8ff");

        return new FirebaseRecyclerOptions.Builder<UserProfile>()
                .setQuery(resultUserQuery, UserProfile.class)
                .setLifecycleOwner(parent)
                .build();
    }

    private void handleUserRelationshipData(DataSnapshot snapshot) {
        userRelationshipList.clear();
        userProfileList.clear();

        if (snapshot.exists() && snapshot.hasChildren()) {

            for (DataSnapshot dataset : snapshot.getChildren()) {
                if (!dataset.exists())
                    continue;

                UserRelationship contactRelation = dataset.getValue(UserRelationship.class);

                if (contactRelation == null || contactRelation.getContactUID() == null)
                    continue;

                userRelationshipList.put(contactRelation.getContactUID(), contactRelation);

                userProfileDBRef.child(contactRelation.getContactUID()).get().addOnSuccessListener(successTask -> {
                    userProfileList.put(contactRelation.getContactUID(), successTask.getValue(UserProfile.class));
                });
            }
        }
    }

    private void initializeData(LoginActivity parent) {
        mNoteView = NoteViewModel.getInstance(getLoggedUserID());
        mNotificationView = NotificationViewModel.getInstance(parent, getLoggedUserID());

        //Storage for images
        userImgStRef = this.getUserImageReference(getLoggedUserID());

        //
        userRelationshipDBRef.addValueEventListener(userRelationshipValueEventListener = new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                handleUserRelationshipData(snapshot);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(parent, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateUI(LoginActivity parent, FirebaseUser user) {
        parent.setProgressBarProgress(20);

        if (user.isEmailVerified()) {

            userDBRef = allUserDBRef.child(user.getUid());

            //Profildaten
            Task<DataSnapshot> userProfileDataTask = userProfileDBRef.child(user.getUid()).get();

            //Relationship
            TaskCompletionSource<DataSnapshot> relationDataSource = new TaskCompletionSource<>();
            Task relationDataTask = relationDataSource.getTask();
            userRelationshipDBRef = userDBRef.child(UserRelationship.TABLE_NAME);
            userRelationshipDBRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    relationDataSource.setResult(snapshot);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    relationDataSource.setException(error.toException());
                }
            });

            //load Data
            mCategoryView = CategoryViewModel.getInstance(userDBRef);
            mSortView = SortViewModel.getInstance(userDBRef);

            Tasks.whenAll(userDBRef.get(), userProfileDataTask, relationDataTask,
                    mCategoryView.initDataTask(parent),
                    mSortView.initDataTask(parent)).addOnCompleteListener(taskAll ->
            {
                if (taskAll.isSuccessful()) {
                    userProfile = userProfileDataTask.getResult().getValue(UserProfile.class);
                    handleUserRelationshipData((DataSnapshot) relationDataTask.getResult());
                    initializeData(parent);
                    parent.setProgressBarProgress(60);
                } else {
                    parent.finishProgressBar();
                    Toast.makeText(parent, taskAll.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            parent.finishProgressBar();
            parent.showNoticeDialog(
                    parent.getString(R.string.dialog_title_verify_email),
                    parent.getString(R.string.dialog_text_verfiy_email),
                    new int[]{R.string.btn_resend_email_verification, R.string.btn_Cancel});
        }
    }

    public void loginUser(LoginActivity parent, String strUserEmail, String strPassword, String idToken) {

        parent.startProgressBar();

        if (mFirebaseUser != null) {
            updateUI(parent, mFirebaseUser);
            return;
        }

        if (mAuthCredential == null) {
            if (idToken != null && !idToken.isEmpty()) {
                mAuthCredential = GoogleAuthProvider.getCredential(idToken, null);
            } else {
                mAuthCredential = EmailAuthProvider.getCredential(strUserEmail, strPassword);
            }
        }

        mFirebaseAuth.signInWithCredential(mAuthCredential).addOnCompleteListener(parent, task -> {
            if (task.isSuccessful()) {
                // Sign in success, update UI with the signed-in user's information
                mFirebaseUser = mFirebaseAuth.getCurrentUser();
                updateUI(parent, mFirebaseUser);
            } else {
                String authCredentialSignInMethod = mAuthCredential != null ? mAuthCredential.getSignInMethod() : "";
                parent.finishProgressBar();
                parent.clearInputFileds();
                clearUserProfile(parent, true);
                parent.signInFailed(authCredentialSignInMethod, task.getException());
            }
        });
    }

    public void createUser(EditUserProfileActivity parent, Intent data, UserProfileImage profilePhoto, String idToken) {

        if (data == null)
            return;

        parent.startProgressBar();

        if (idToken != null && !idToken.isEmpty()) {
            //signUpGoogle
            mAuthCredential = GoogleAuthProvider.getCredential(idToken, null);
            mFirebaseAuth.signInWithCredential(mAuthCredential).addOnCompleteListener(parent, task -> {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    mFirebaseUser = mFirebaseAuth.getCurrentUser();

                    userProfile = new UserProfile();
                    userProfile.setUserRegistrationToken(idToken);
                    userProfile.setUserRegistrationTime(UserNotificationService.registrationTime);
                    userProfile.setFirebaseUID(mFirebaseUser.getUid());
                    userProfile.setUserBirthday(data.getLongExtra("UserProfile_UserBirthday", 0));
                    userProfile.setUserEmail(data.getStringExtra("UserProfile_UserEmail"));
                    userProfile.setUserName(data.getStringExtra("UserProfile_UserName"));
                    userProfile.setUserID(data.getStringExtra("UserProfile_UserID"));
                    userProfile.setUserPhotoURL(profilePhoto.getProfileImageURL());
                    save(parent, null, userProfile);

                } else {
                    // If sign in fails, display a message to the user.
                    parent.finishProgressBar();
                    Toast.makeText(parent, task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                    task.getException().printStackTrace();
                }
            });
        } else {
            mFirebaseAuth.createUserWithEmailAndPassword(data.getStringExtra("UserProfile_UserEmail"), data.getStringExtra("UserProfile_UserPassword"))
                    .addOnCompleteListener(parent, task -> {
                        if (task.isSuccessful()) {
                            AuthResult taskResult = task.getResult();
                            mFirebaseUser = mFirebaseAuth.getCurrentUser();
                            mAuthCredential = taskResult.getCredential();

                            userProfile = new UserProfile();
                            userProfile.setUserRegistrationToken(UserNotificationService.registrationToken);
                            userProfile.setUserRegistrationTime(UserNotificationService.registrationTime);
                            userProfile.setFirebaseUID(mFirebaseUser.getUid());
                            userProfile.setUserBirthday(data.getLongExtra("UserProfile_UserBirthday", 0));
                            userProfile.setUserEmail(data.getStringExtra("UserProfile_UserEmail"));
                            userProfile.setUserName(data.getStringExtra("UserProfile_UserName"));
                            userProfile.setUserID(data.getStringExtra("UserProfile_UserID"));
                            save(parent, profilePhoto.getProfileImageUri(), userProfile);

                            Utilities.setLoginRemember(parent,
                                    data.getStringExtra("UserProfile_UserEmail"),
                                    data.getStringExtra("UserProfile_UserPassword"));

                        } else {
                            parent.finishProgressBar();
                            Toast.makeText(parent, task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                            task.getException().printStackTrace();
                        }
                    });
        }
    }

    public void save(EditUserProfileActivity parent, Uri profilePhotoUri, UserProfile oUserProfile) {

        if (oUserProfile == null)
            return;

        if (userImgStRef == null)
            userImgStRef = this.getUserImageReference(getLoggedUserID());

        parent.startProgressBar();

        if (profilePhotoUri != null) {

            StorageMetadata metadata = new StorageMetadata.Builder()
                    .setContentType("image/jpg")
                    .setCustomMetadata("userID", oUserProfile.getUserID())
                    .build();

            UploadTask uploadTask = userImgStRef.child(getLoggedUserID()).putFile(profilePhotoUri, metadata);
            uploadTask.addOnSuccessListener(taskSnapshot -> {

                        taskSnapshot.getStorage().getDownloadUrl().addOnSuccessListener(uri -> {
                                    oUserProfile.setUserPhotoURL(uri.toString());
                                    saveProfile(parent, oUserProfile);
                                }
                        );
                    })
                    .addOnFailureListener(exception -> {
                        // Handle unsuccessful uploads
                        parent.finishProgressBar();
                        Toast.makeText(parent, exception.getMessage(), Toast.LENGTH_LONG).show();
                    });
        } else {
            saveProfile(parent, oUserProfile);
        }
    }

    private void saveProfile(EditUserProfileActivity parent, UserProfile data) {
        if (data == null || mFirebaseUser == null)
            return;

        userProfileDBRef.child(getLoggedUserID()).setValue(data.toMap())
                .addOnSuccessListener(taskSnapshot -> {

                    userProfile.deserialize(data);

                    Toast.makeText(parent, R.string.toast_save_successful, Toast.LENGTH_SHORT).show();

                    //set MainMenuIcon userprofile image
                    setUserProfileIcon(parent);

                    //Profil im Firebase aktualisieren
                    UserProfileChangeRequest.Builder profileUpdates = new UserProfileChangeRequest.Builder();

                    if (data.getUserName() != null)
                        profileUpdates.setDisplayName(data.getUserName());

                    if (data.getUserPhotoURL() != null)
                        profileUpdates.setPhotoUri(Uri.parse(data.getUserPhotoURL()));

                    mFirebaseUser.updateProfile(profileUpdates.build());
                    this.sendEmailVerification(parent); // Bestätigungs-Email senden, falls noch nicht geschehen
                    parent.onCompleteSave();
                })
                .addOnFailureListener(e -> {
                    parent.finishProgressBar();
                    Toast.makeText(parent, e.getMessage(), Toast.LENGTH_LONG).show();
                });
    }

    public void sendEmailVerification(Context parent) {
        if (mFirebaseUser == null || mFirebaseUser.isEmailVerified())
            return;

        mFirebaseUser.sendEmailVerification().addOnCompleteListener(task1 -> {

                    if (task1.isSuccessful()) {
                        Toast.makeText(parent,
                                R.string.toast_sent_email_verification,
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(parent,
                                task1.getException().getMessage(),
                                Toast.LENGTH_LONG).show();
                    }

                    clearUserProfile(parent, true);
                }
        );
    }

    public void sendPasswordResetEmail(Context parent, String strEmailAdress) {
        if (mFirebaseAuth == null)
            return;

        mFirebaseAuth.sendPasswordResetEmail(strEmailAdress)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Toast.makeText(parent,
                                R.string.toast_sent_email_password_reset,
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(parent,
                                task.getException().getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void deleteUserAccount(BasicActivity parent) {
        if (mFirebaseUser == null || getLoggedUserID() == null)
            return;

        mFirebaseUser.reauthenticate(mAuthCredential).addOnCompleteListener(task -> {

            if (task.isSuccessful()) {

                Task deleteUserData = allUserDBRef.child(getLoggedUserID()).removeValue(); //Benutzerdaten
                Task deleteProfileData = userProfileDBRef.child(getLoggedUserID()).removeValue(); //Profildaten
                Task<ListResult> listAllImages = getUserImageReference(getLoggedUserID()).listAll(); //alle Bilder im Storage

                //Notizen
                TaskCompletionSource tcsNotes = new TaskCompletionSource();
                Task<DataSnapshot> getNotesTask = tcsNotes.getTask();
                mNoteView.getQueryNotesByCreator().addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        tcsNotes.setResult(snapshot);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        tcsNotes.setException(error.toException());
                    }
                });

                //empfangene Benachrichtigungen
                TaskCompletionSource tcsNotificationReceiver = new TaskCompletionSource();
                Task<DataSnapshot> getNotificationsReceiverTask = tcsNotificationReceiver.getTask();
                mNotificationView.getQueryNotificationsByReceiver().addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        tcsNotificationReceiver.setResult(snapshot);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        tcsNotificationReceiver.setException(error.toException());
                    }
                });

                //gesendete Benachrichtigungen
                TaskCompletionSource tcsNotificationSender = new TaskCompletionSource();
                Task<DataSnapshot> getNotificationsSenderTask = tcsNotificationSender.getTask();
                mNotificationView.getQueryNotificationsBySender().addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        tcsNotificationSender.setResult(snapshot);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        tcsNotificationSender.setException(error.toException());
                    }
                });

                Tasks.whenAll(deleteUserData, deleteProfileData, listAllImages, getNotesTask, getNotificationsReceiverTask, getNotificationsSenderTask).addOnCompleteListener(taskAll -> {
                    if (deleteUserData.isSuccessful() && deleteProfileData.isSuccessful())
                        Toast.makeText(parent, "Benutzerdaten erfolgreich gelöscht!", Toast.LENGTH_SHORT).show();

                    final List<Task<Void>> listTask = new ArrayList<>();
                    if (listAllImages.isSuccessful()) {
                        for (StorageReference item : listAllImages.getResult().getItems()) {
                            listTask.add(item.delete());
                        }
                    }

                    if (getNotesTask.isSuccessful() && getNotesTask.getResult().exists()) {
                        for (DataSnapshot dataSnapshot : getNotesTask.getResult().getChildren()) {
                            listTask.add(dataSnapshot.getRef().removeValue());
                        }
                    }

                    if (getNotificationsReceiverTask.isSuccessful() && getNotificationsReceiverTask.getResult().exists()) {
                        for (DataSnapshot object : getNotificationsReceiverTask.getResult().getChildren()) {
                            listTask.add(object.getRef().removeValue());
                        }
                    }

                    if (getNotificationsSenderTask.isSuccessful() && getNotificationsSenderTask.getResult().exists()) {
                        for (DataSnapshot object : getNotificationsSenderTask.getResult().getChildren()) {
                            listTask.add(object.getRef().removeValue());
                        }
                    }

                    Tasks.whenAll(listTask).addOnSuccessListener(unused -> {
                        Toast.makeText(parent, "Alle Beiträge erfolgreich gelöscht!", Toast.LENGTH_SHORT).show();

                        clearUserProfile(parent, false);
                        mFirebaseUser.delete().addOnCompleteListener(taskDeleteAccount -> {
                            if (taskDeleteAccount.isSuccessful()) {
                                Toast.makeText(parent, "Benutzerkonto erfolgreich gelöscht!", Toast.LENGTH_LONG).show();

                                String authCredentialSignInMethod = mAuthCredential != null ? mAuthCredential.getSignInMethod() : "";
                                if (TextUtils.equals(authCredentialSignInMethod, GoogleAuthProvider.GOOGLE_SIGN_IN_METHOD)) {
                                    oneTapClient.signOut();
                                }

                                Intent intent = new Intent(parent, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |
                                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                parent.startActivity(intent);
                                AppManager.getAppManager().finishOthersActivity(LoginActivity.class);

                            } else
                                Toast.makeText(parent, taskDeleteAccount.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        });
                    });
                });
            } else {
                Toast.makeText(parent, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void updateUserPassword(BasicActivity parent, String strUserEmail,
                                   String strOldPassword, String strNewPassword) {
        if (mFirebaseUser == null)
            return;

        parent.startProgressBar();
        mAuthCredential = EmailAuthProvider.getCredential(strUserEmail, strOldPassword);
        mFirebaseUser.reauthenticate(mAuthCredential).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                mFirebaseUser.updatePassword(strNewPassword).addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {
                        parent.finishProgressBar();
                        Toast.makeText(parent, "Passwort erfolgreich geändert!", Toast.LENGTH_SHORT).show();

                        AppManager.getAppManager().finishActivity(parent, Activity.RESULT_OK);
                    } else {
                        parent.finishProgressBar();
                        Toast.makeText(parent, task1.getException().getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                parent.finishProgressBar();
                Toast.makeText(parent, task.getException().getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void requestFriendship(Context parent, String strContactUID, FirebaseRecyclerAdapter adapter) {

        if (userRelationshipDBRef == null)
            return;

        UserRelationship friendship = getUserRelationship(strContactUID);

        if (friendship == null)
            friendship = new UserRelationship(
                    strContactUID, false, false, false, true);
        else
            return;

        String strKey = userRelationshipDBRef.push().getKey();
        friendship.setUserRelation_Key(strKey);

        userRelationshipDBRef.child(strKey).setValue(friendship)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        mNotificationView.sendNotificationRequest(userProfile.getUserID() +
                                        parent.getString(R.string.notification_request_friendship_text),
                                getLoggedUserID(), strContactUID);

                        adapter.notifyDataSetChanged();

                    } else {
                        Toast.makeText(parent,
                                task.getException().getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void confirmRelationshipRequest(Context parent, Notification oNotification) {

        if (oNotification == null || oNotification.getNotificationSenderUID() == null ||
                userRelationshipDBRef == null)
            return;

        String strContactUID = oNotification.getNotificationSenderUID();

        UserRelationship friendship = getUserRelationship(strContactUID);

        if (friendship == null)
            friendship = new UserRelationship(
                    strContactUID,
                    true, false, true, false);
        else
            return;

        String strKey = userRelationshipDBRef.push().getKey();
        friendship.setUserRelation_Key(strKey);

        userRelationshipDBRef
                .child(friendship.getUserRelation_Key()).setValue(friendship)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        mNoteView.addReaderUIDToNotes(strContactUID);
                        mNotificationView.confirmNotification(oNotification,
                                userProfile.getUserID() + parent.getString(R.string.notification_request_friendship_accepted_text));
                    } else {
                        Toast.makeText(parent,
                                task.getException().getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void denyRelationshipRequest(Context parent, Notification oNotification) {
        mNotificationView.denyNotification(oNotification,
                userProfile.getUserID() + parent.getString(R.string.notification_request_friendship_denied_text));
    }

    public void updateRelationshipRequestDenied(Notification oNotification) {
        if (oNotification == null || oNotification.getNotificationSenderUID() == null ||
                userRelationshipDBRef == null)
            return;

        String strContactUID = oNotification.getNotificationSenderUID();
        UserRelationship friendship = getUserRelationship(strContactUID);
        if (friendship == null || friendship.getUserRelation_Key() == null)
            return;

        userRelationshipDBRef
                .child(friendship.getUserRelation_Key()).removeValue()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        ;
                    }
                });
    }

    public void updateRelationshipRequestAccepted(Notification oNotification) {
        if (oNotification == null || oNotification.getNotificationSenderUID() == null ||
                userRelationshipDBRef == null)
            return;

        String strContactUID = oNotification.getNotificationSenderUID();
        UserRelationship friendship = getUserRelationship(strContactUID);
        if (friendship == null || friendship.getUserRelation_Key() == null)
            return;

        friendship.setFriend(true);
        friendship.setRequested(false);
        friendship.setFollowing(true);

        userRelationshipDBRef
                .child(friendship.getUserRelation_Key()).setValue(friendship)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        mNoteView.addReaderUIDToNotes(strContactUID);
                    }
                });
    }


    public void cancelRelationship(Context parent, UserRelationship oFriend) {
        if (oFriend == null || oFriend.getUserRelation_Key() == null ||
                userRelationshipDBRef == null)
            return;

        userRelationshipDBRef
                .child(oFriend.getUserRelation_Key()).removeValue()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        mNoteView.removeReaderUIDFromNotes(oFriend.getContactUID());

                        mNotificationView.sendNotificationRelationshipCanceled(
                                userProfile.getUserID() + parent.getString(R.string.notification_request_friendship_canceled_text),
                                getLoggedUserID(), oFriend.getContactUID());
                    } else {
                        Toast.makeText(parent,
                                task.getException().getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void handleEndedFriendship(Context parent, String strContactUID) {

        UserRelationship oFriend = getUserRelationship(strContactUID);
        if (oFriend == null || oFriend.getUserRelation_Key() == null ||
                userRelationshipDBRef == null)
            return;

        userRelationshipDBRef
                .child(oFriend.getUserRelation_Key()).removeValue()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        mNoteView.removeReaderUIDFromNotes(strContactUID);
                    } else {
                        Toast.makeText(parent,
                                task.getException().getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void updateFCMRegistrationToken() {
        if (userProfile == null)
            return;

        if (userProfile.getUserRegistrationTime() != null &&
                userProfile.getUserRegistrationToken() != null) {

            Long timestamp = Utilities.subMonthFromTimestamp(
                    userProfile.getUserRegistrationTime(), 2);

            if (userProfile.getUserRegistrationTime() >= timestamp) {
                UserNotificationService.registrationTime = userProfile.getUserRegistrationTime();
                UserNotificationService.registrationToken = userProfile.getUserRegistrationToken();

                return;
            }
        }

        if (UserNotificationService.registrationTime == null ||
                UserNotificationService.registrationToken == null) {

            FirebaseMessaging.getInstance().getToken().addOnSuccessListener(task -> {
                // Get new FCM registration token
                UserNotificationService.registrationToken = task;
                UserNotificationService.registrationTime = System.currentTimeMillis();

                userProfile.setUserRegistrationToken(UserNotificationService.registrationToken);
                userProfile.setUserRegistrationTime(UserNotificationService.registrationTime);
                //this.saveProfile(parent, userProfile);
            });
        } else {
            userProfile.setUserRegistrationToken(UserNotificationService.registrationToken);
            userProfile.setUserRegistrationTime(UserNotificationService.registrationTime);
            //this.saveProfile(parent, userProfile);
        }
    }

    public boolean isNotePinnedByLoggedUser(Note oObject) {
        if (mNoteView == null)
            return false;

        return mNoteView.isNotePinnedByUID(oObject, getLoggedUserID());
    }

    public void setNotePinned(Note oObject) {
        if (mNoteView == null)
            return;

        if (isNotePinnedByLoggedUser(oObject))
            mNoteView.saveNotePinned(oObject, false, getLoggedUserID());
        else
            mNoteView.saveNotePinned(oObject, true, getLoggedUserID());
    }
}