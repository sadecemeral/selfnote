package de.mh.selfnote.viewModel;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class MainViewModel {

    final protected DatabaseReference mDatabase;
    final protected StorageReference mStorage;
    final protected StorageReference mImgStorageRef;
    protected static final String TAG = "MainViewModel";
    final private String dbURL = "https://selfnote-c2766-default-rtdb.europe-west1.firebasedatabase.app/";
    final private String stURL = "gs://selfnote-c2766.appspot.com";

    public MainViewModel() {
        mDatabase = FirebaseDatabase.getInstance(dbURL).getReference();
        mStorage = FirebaseStorage.getInstance(stURL).getReference();
        mImgStorageRef = mStorage.child("images");
    }

    protected StorageReference getUserImageReference(String strKey){
        if(strKey == null || strKey.length() < 1)
            return null;

        return mImgStorageRef.child(strKey);
    }
}