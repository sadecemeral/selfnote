package de.mh.selfnote.viewModel;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;

import com.firebase.ui.database.FirebaseListOptions;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.mh.selfnote.CategoryFilterActivity;
import de.mh.selfnote.R;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.objects.Category;
import de.mh.selfnote.toolbox.Utilities;

public class CategoryViewModel extends MainViewModel {

    private static CategoryViewModel INSTANCE;
    final private HashMap<String, Category> htCategory = new HashMap<>();
    final public static String categoryName_Diary = "Tagebuch";

    final private DatabaseReference dbRefCategory;

    final private static String TAG = "CategoryViewModel";

    public static CategoryViewModel getInstance(DatabaseReference dbRef) {
        if (INSTANCE == null)
            INSTANCE = new CategoryViewModel(dbRef);

        return INSTANCE;
    }

    public void initInstance() {
        INSTANCE = null;
    }

    private CategoryViewModel(DatabaseReference dbRef) {
        super();
        dbRefCategory = dbRef.child(Category.TABLE_NAME);
    }

    public Task initDataTask(Context parent) {
        final TaskCompletionSource tcs = new TaskCompletionSource();
        dbRefCategory.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                tcs.setResult(snapshot);

                if (!snapshot.exists())
                    initData(parent);
                else {
                    htCategory.clear();

                    for (DataSnapshot dataset : snapshot.getChildren()) {
                        Category oCategory = dataset.getValue(Category.class);
                        htCategory.put(oCategory.getCategoryName(), oCategory);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                tcs.setException(error.toException());
            }
        });

        return tcs.getTask();
    }

    public FirebaseListOptions<Category> getCategoryOptions(BasicActivity parent) {
        return new FirebaseListOptions.Builder<Category>()
                .setQuery(dbRefCategory, Category.class)
                .setLifecycleOwner(parent)
                .setLayout(R.layout.category_selection_spinner_item)
                .build();
    }

    public FirebaseRecyclerOptions<Category> getCategoryAdapterOptions(CategoryFilterActivity parent) {
        return new FirebaseRecyclerOptions.Builder<Category>()
                .setQuery(dbRefCategory, Category.class)
                .setLifecycleOwner(parent)
                .build();
    }

    private void initData(Context parent) {
        if (parent == null)
            return;

        String[] strInitCatArray = parent.getResources().getStringArray(R.array.noteCategoryInit_array);
        if (strInitCatArray != null && strInitCatArray.length > 0) {

            for (String s : strInitCatArray) {

                int iColor = Utilities.getColorByName(parent, s);
                this.save(new Category(s, iColor, true));
            }
        }
    }

    public List<String> getEnabledCategoryNameList() {

        List<String> enabledCategoryList = new ArrayList<>();

        for (Category item : htCategory.values()) {
            if (item.isCategoryEnabled())
                enabledCategoryList.add(item.getCategoryName());
        }

        return enabledCategoryList;
    }

    public Category getCategoryByName(String strCatName) {
        if (TextUtils.isEmpty(strCatName))
            return null;

        if (htCategory.isEmpty() || !htCategory.containsKey(strCatName))
            return null;

        return htCategory.get(strCatName);
    }

    public void save(Category oCategory) {
        if (dbRefCategory == null || oCategory == null)
            return;

        if (oCategory.getCategoryKey() == null || oCategory.getCategoryKey().length() < 1)
            oCategory.setCategoryKey(dbRefCategory.push().getKey());

        dbRefCategory.child(oCategory.getCategoryKey()).setValue(oCategory).addOnCompleteListener(task ->
        {
            if (!task.isSuccessful()) {
                Log.e(TAG, task.getException().getMessage());
            } else {
                htCategory.put(oCategory.getCategoryName(), oCategory);
            }
        });
    }
}