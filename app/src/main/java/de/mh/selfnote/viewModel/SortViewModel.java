package de.mh.selfnote.viewModel;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import de.mh.selfnote.R;
import de.mh.selfnote.SortKeySetActivity;
import de.mh.selfnote.objects.SortKey;


public class SortViewModel extends MainViewModel {

    private static SortViewModel INSTANCE;
    private HashMap<String, SortKey> htSortKey = new HashMap<>();
    final private DatabaseReference dbRefSortKey;
    final private static String TAG = "SortViewModel";

    public static SortViewModel getInstance(DatabaseReference dbRef) {
        if (INSTANCE == null)
            INSTANCE = new SortViewModel(dbRef);

        return INSTANCE;
    }

    public void initInstance() {
        INSTANCE = null;
    }

    private SortViewModel(DatabaseReference dbRef) {
        super();
        //benutzerbezogene Sortierung
        dbRefSortKey = dbRef.child(SortKey.TABLE_NAME);
    }

    public Task initDataTask(Context parent) {
        final TaskCompletionSource tcs = new TaskCompletionSource();
        dbRefSortKey.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                tcs.setResult(snapshot);

                if (!snapshot.exists()) {
                    initData(parent);
                } else {
                    htSortKey.clear();
                    for (DataSnapshot dataset : snapshot.getChildren()) {
                        SortKey sortKey = dataset.getValue(SortKey.class);
                        htSortKey.put(sortKey.getSortKeyKey(), sortKey);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                tcs.setException(error.toException());
            }
        });

        return tcs.getTask();
    }

    public FirebaseRecyclerOptions<SortKey> getSortKeyOptions(SortKeySetActivity parent) {
        return new FirebaseRecyclerOptions.Builder<SortKey>()
                .setQuery(dbRefSortKey, SortKey.class)
                .setLifecycleOwner(parent)
                .build();
    }

    private void initData(Context parent) {
        if (parent == null)
            return;

        String[] strSortKeyName = parent.getResources().getStringArray(R.array.noteSortKeyNameInit_array);
        String[] strSortKeyCol = parent.getResources().getStringArray(R.array.noteSortKeyColInit_array);

        if (strSortKeyName == null || strSortKeyCol == null || strSortKeyName.length != strSortKeyCol.length)
            return;

        boolean bEnabled = true;
        for (int i = 0; i < strSortKeyName.length; i++) {

            if (i > 0)
                bEnabled = false;

            this.save(new SortKey(strSortKeyName[i], strSortKeyCol[i], i + 1, bEnabled));
        }
    }

    public String[] getEnabledSortKeyList() {

        String[] enabledSortKeys = null;
        ArrayList<String> enabledList = new ArrayList();
        for (SortKey item : htSortKey.values()) {
            if (item.isSortKeyEnabled())
                enabledList.add(item.getSortKeyNoteCol());
        }

        if (enabledList.size() > 0)
            enabledSortKeys = enabledList.toArray(new String[0]);

        return enabledSortKeys;
    }

    public void save(SortKey oSortKey) {
        if (dbRefSortKey == null || oSortKey == null)
            return;

        if (oSortKey.getSortKeyKey() == null || oSortKey.getSortKeyKey().isEmpty())
            oSortKey.setSortKeyKey(dbRefSortKey.push().getKey());

        dbRefSortKey.child(oSortKey.getSortKeyKey()).setValue(oSortKey).addOnCompleteListener(task ->
        {
            if (!task.isSuccessful()) {
                Log.e(TAG, task.getException().getMessage());
            } else {
                htSortKey.put(oSortKey.getSortKeyKey(), oSortKey);
            }
        });
    }
}