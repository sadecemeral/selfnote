package de.mh.selfnote.viewModel;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;

import com.firebase.ui.database.FirebaseListOptions;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import de.mh.selfnote.AddNewNoteActivity;
import de.mh.selfnote.DiaryActivity;
import de.mh.selfnote.R;
import de.mh.selfnote.adapter.NoteListAdapter;
import de.mh.selfnote.adapter.PinnedNoteListAdapter;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.objects.Note;
import de.mh.selfnote.objects.NoteImage;
import de.mh.selfnote.objects.StatusKnz;
import de.mh.selfnote.toolbox.AppManager;

public class NoteViewModel extends MainViewModel {

    private static NoteViewModel INSTANCE;
    private StorageReference userImgStRef;
    final private DatabaseReference userNoteDBRef = mDatabase.child(Note.TABLE_NAME);
    final private String loggedUserID;
    private NoteListAdapter noteListAdapter;
    private PinnedNoteListAdapter pinnedNoteListAdapter;

    public static NoteViewModel getInstance(String strLoggedUserID) {
        if (INSTANCE == null)
            INSTANCE = new NoteViewModel(strLoggedUserID);

        return INSTANCE;
    }

    public void initInstance() {
        INSTANCE = null;
        if (noteListAdapter != null)
            noteListAdapter.stopListening();
        if (pinnedNoteListAdapter != null)
            pinnedNoteListAdapter.stopListening();
    }

    private NoteViewModel(String strLoggedUserID) {
        super();

        loggedUserID = strLoggedUserID;

        if (loggedUserID != null) {
            userImgStRef = super.getUserImageReference(loggedUserID);
        }
    }

    public StorageReference getUserImageReference() {
        return userImgStRef;
    }

    /**
     * Options Pinnwand
     *
     * @param parent
     * @return
     */
    private FirebaseListOptions<Note> getPinnedNoteOptions(LifecycleOwner parent) {
        Query queryPinnedNotes = userNoteDBRef.orderByChild("notePinnedByUID/" + loggedUserID).equalTo(true);

        return new FirebaseListOptions.Builder<Note>()
                .setQuery(queryPinnedNotes, Note.class)
                .setLifecycleOwner(parent)
                .setLayout(R.layout.main_pinned_note_list_item)
                .build();
    }

    /**
     * Adapter Pinnwand
     *
     * @return
     */
    public PinnedNoteListAdapter createPinnedNoteListAdapter(BasicActivity parentActivity, LifecycleOwner lifecycleOwner) {
        return pinnedNoteListAdapter = new PinnedNoteListAdapter(parentActivity, this.getPinnedNoteOptions(lifecycleOwner));
    }

    private FirebaseRecyclerOptions<Note> getNoteOptionsByOwner(LifecycleOwner lifecycleOwner) {
        return new FirebaseRecyclerOptions.Builder<Note>()
                .setQuery(getQueryNotesByCreator(), Note.class)
                .setLifecycleOwner(lifecycleOwner)
                .build();
    }

    public FirebaseRecyclerOptions<Note> getNoteOptions(LifecycleOwner lifecycleOwner) {
        Query queryNotes = userNoteDBRef.orderByChild("readerListUID/" + loggedUserID).equalTo(true);

        return new FirebaseRecyclerOptions.Builder<Note>()
                .setQuery(queryNotes, Note.class)
                .setLifecycleOwner(lifecycleOwner)
                .build();
    }

    public NoteListAdapter createNoteListAdapter(BasicActivity parentActivity, LifecycleOwner lifecycleOwner) {
        return noteListAdapter = new NoteListAdapter(parentActivity, this.getNoteOptions(lifecycleOwner));
    }

    public void updateNoteListAdapter(LifecycleOwner lifecycleOwner, boolean byOwner) {
        if (noteListAdapter == null)
            return;

        if (byOwner)
            noteListAdapter.updateOptions(this.getNoteOptionsByOwner(lifecycleOwner));
        else
            noteListAdapter.updateOptions(this.getNoteOptions(lifecycleOwner));
    }

    /**
     * Suche nach Notizen
     *
     * @param strSearchKey
     * @return
     */
    public FirebaseRecyclerOptions<Note> getResultNoteOptions(LifecycleOwner lifecycleOwner, String strSearchKey) {

        if (strSearchKey == null || strSearchKey.isEmpty())
            return null;

        Query resultNoteQuery = userNoteDBRef.orderByChild("noteText")
                .startAt(strSearchKey)
                .endAt(strSearchKey + "uf8ff");

        return new FirebaseRecyclerOptions.Builder<Note>()
                .setQuery(resultNoteQuery, Note.class)
                .setLifecycleOwner(lifecycleOwner)
                .build();
    }

    public String getNewNoteKey() {
        return userNoteDBRef.push().getKey();
    }

    public void save(BasicActivity parent, ArrayList<NoteImage> imageList, Note oNote) {

        parent.startProgressBar();

        if (imageList == null || imageList.isEmpty())
            this.saveData(oNote);
        else {

            ArrayList<String> noteImageListURL = new ArrayList<>();

            //nach Status Knz sortieren, alphabetisch
            Collections.sort(imageList, NoteImage.NoteImageStatusComparator);

            for (int iCounter = 0; iCounter < imageList.size(); iCounter++) {

                NoteImage oImageNote = imageList.get(iCounter);
                if (oImageNote == null)
                    continue;

                if (oImageNote.getNoteImageStatusKnz() == null) {
                    imageList.remove(iCounter);

                    if (imageList.size() == 0) {
                        saveData(oNote);
                        return;
                    }

                    iCounter--;
                } else if (StatusKnz.DELETE.equals(oImageNote.getNoteImageStatusKnz()) &&
                        !TextUtils.isEmpty(oImageNote.getNoteImageURL())) {

                    oNote.changeNoteImageListURL(oImageNote.getNoteImageURL(), StatusKnz.DELETE);

                    userImgStRef.getStorage().getReferenceFromUrl(oImageNote.getNoteImageURL())
                            .delete()
                            .addOnFailureListener(e -> {
                                Toast.makeText(parent, e.getMessage(), Toast.LENGTH_LONG);
                                return;
                            });

                    imageList.remove(oImageNote);

                    if (imageList.size() == 0) {
                        saveData(oNote);
                        return;
                    }

                    iCounter--;

                } else if (StatusKnz.INSERT.equals(oImageNote.getNoteImageStatusKnz()) &&
                        oImageNote.getNoteImageUri() != null) {

                    StorageMetadata metadata = new StorageMetadata.Builder()
                            .setContentType("image/jpg")
                            .setCustomMetadata("noteKey", oNote.getNoteKey())
                            .setCustomMetadata("creatorUID", oNote.getCreatorUID())
                            .build();


                    userImgStRef.child(oNote.getNoteKey()).child(UUID.randomUUID().toString())
                            .putFile(oImageNote.getNoteImageUri(), metadata)
                            .addOnSuccessListener(taskSnapshot -> {
                                taskSnapshot.getStorage().getDownloadUrl().addOnSuccessListener(uri -> {

                                    noteImageListURL.add(uri.toString());
                                    oNote.changeNoteImageListURL(uri.toString(), StatusKnz.INSERT);

                                    if (noteImageListURL.size() == imageList.size()) {
                                        saveData(oNote);
                                    }
                                });
                            })
                            .addOnFailureListener(exception -> {
                                // Handle unsuccessful uploads
                                Toast.makeText(parent, exception.getMessage(), Toast.LENGTH_LONG).show();
                            });

                }
            }
        }
    }

    private Task saveData(Note oNote) {
        if (oNote == null || oNote.getNoteKey() == null)
            return null;

        Activity currentActivity = AppManager.getAppManager().currentActivity();

        if (currentActivity == null) {
            return userNoteDBRef.child(oNote.getNoteKey()).setValue(oNote.toMap());
        } else {
            return userNoteDBRef.child(oNote.getNoteKey()).setValue(oNote.toMap()).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {

                    String currentClsName = currentActivity.getClass().getName();
                    String clsAddNewNote = AddNewNoteActivity.class.getName();
                    String clsAddNewDiary = DiaryActivity.class.getName();

                    if (TextUtils.equals(currentClsName, clsAddNewDiary) ||
                            TextUtils.equals(currentClsName, clsAddNewNote)) {
                        Toast.makeText(currentActivity, R.string.toast_save_successful, Toast.LENGTH_LONG).show();
                        AppManager.getAppManager().finishActivity(currentActivity, Activity.RESULT_OK);
                    }
                } else {
                    Toast.makeText(currentActivity, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public boolean isNotePinnedByUID(Note oNote, String strUID) {
        return (oNote != null && !TextUtils.isEmpty(strUID)) ? oNote.isNotePinnedByUser(strUID) : false;
    }

    public void saveNotePinned(Note oNote, boolean isPinned, String strUID) {
        if (oNote == null || oNote.getNoteKey() == null || strUID == null)
            return;

        if (isPinned)
            oNote.addUIDToPinnedNoteList(strUID, isPinned);
        else
            oNote.removeUIDFromPinnedNoteList(strUID);

        userNoteDBRef.child(oNote.getNoteKey()).setValue(oNote.toMap());
    }

    public void delete(Context parent, Note oNote) {
        if (oNote == null || oNote.getNoteKey() == null)
            return;

        ArrayList<String> imageListURL = oNote.getNoteImageListURL();

        userNoteDBRef.child(oNote.getNoteKey()).removeValue().addOnCompleteListener(task -> {

            if (task.isSuccessful()) {
                Toast.makeText(parent, R.string.toast_delete_successful, Toast.LENGTH_LONG).show();

                if (imageListURL != null && !imageListURL.isEmpty()) {
                    for (String imageURL : imageListURL) {
                        userImgStRef.getStorage().getReferenceFromUrl(imageURL).delete()
                                .addOnFailureListener(e -> {
                                    Toast.makeText(parent, e.getMessage(), Toast.LENGTH_LONG);
                                    return;
                                });
                    }
                }

                if (parent instanceof AddNewNoteActivity || parent instanceof DiaryActivity) {
                    AppManager.getAppManager().finishActivity((BasicActivity) parent, Activity.RESULT_OK);
                }
            } else {
                Toast.makeText(parent, task.getException().getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public Query getQueryNotesByCreator() {
        return (userNoteDBRef != null && !TextUtils.isEmpty(loggedUserID)) ? userNoteDBRef.orderByChild("creatorUID").equalTo(loggedUserID) : null;
    }

    public void addReaderUIDToNotes(String strReaderUID) {
        if (strReaderUID == null || strReaderUID.isEmpty())
            return;

        //Query: Select Notes by Owner/LoggedUser
        final List<Task<Void>> listTask = new ArrayList<>();
        getQueryNotesByCreator().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                if (snapshot.exists() && snapshot.hasChildren()) {

                    for (DataSnapshot dataset : snapshot.getChildren()) {
                        if (!dataset.exists())
                            continue;

                        Note oItem = dataset.getValue(Note.class);
                        if (oItem.isNotePersonal())
                            continue;

                        oItem.addReaderToList(strReaderUID, true);
                        listTask.add(saveData(oItem));
                    }
                }

                Tasks.whenAll(listTask).addOnSuccessListener(unused -> {
                    if (noteListAdapter != null) noteListAdapter.notifyDataSetChanged();
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });


    }

    public void removeReaderUIDFromNotes(String strReaderUID) {
        if (strReaderUID == null || strReaderUID.isEmpty())
            return;

        //Query: Select Notes by Owner/LoggedUser
        final List<Task<Void>> listTask = new ArrayList<>();
        getQueryNotesByCreator().addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                if (snapshot.exists() && snapshot.hasChildren()) {

                    for (DataSnapshot dataset : snapshot.getChildren()) {
                        if (!dataset.exists())
                            continue;

                        Note oItem = dataset.getValue(Note.class);
                        oItem.removeReaderUIDFromList(strReaderUID);
                        oItem.removeUIDFromPinnedNoteList(strReaderUID);
                        listTask.add(saveData(oItem));
                    }
                }

                Tasks.whenAll(listTask).addOnSuccessListener(unused -> {
                    if (noteListAdapter != null) noteListAdapter.notifyDataSetChanged();
                    if (pinnedNoteListAdapter != null) pinnedNoteListAdapter.notifyDataSetChanged();
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    public Note findNoteByKey(String strNoteKey) {
        return (noteListAdapter != null) ? noteListAdapter.findNoteByKey(strNoteKey) : null;
    }

    public Note findPinnedNoteByKey(String strNoteKey) {
        return (pinnedNoteListAdapter != null) ? pinnedNoteListAdapter.findNoteByKey(strNoteKey) : null;
    }
}