package de.mh.selfnote.viewModel;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.objects.Notification;
import de.mh.selfnote.objects.NotificationType;
import de.mh.selfnote.toolbox.Utilities;

public class NotificationViewModel extends MainViewModel {

    private static NotificationViewModel INSTANCE;

    final private DatabaseReference userNotificationDBRef = mDatabase.child(Notification.TABLE_NAME);
    final private String loggedUserID;
    private Query queryUserNotification;
    private ChildEventListener notificationChildEventListener;
    private boolean existNewNotification = false;

    public static NotificationViewModel getInstance(Context parent, String strLoggedUserID) {
        if (INSTANCE == null)
            INSTANCE = new NotificationViewModel(parent, strLoggedUserID);

        return INSTANCE;
    }

    public void initInstance() {
        INSTANCE = null;
        if (queryUserNotification != null)
            queryUserNotification.removeEventListener(notificationChildEventListener);
    }

    public boolean isExistNewNotification() {
        return existNewNotification;
    }

    public void setNoNewNotificationExist() {
        existNewNotification = false;
        UserProfileViewModel.getInstance().setNotificationIcon(existNewNotification);
    }

    private NotificationViewModel(Context parent, String strLoggedUserID) {
        super();

        loggedUserID = strLoggedUserID;

        notificationChildEventListener = createListener(parent);
        queryUserNotification = userNotificationDBRef.orderByChild("notificationReceiverUID").equalTo(loggedUserID);
        queryUserNotification.addChildEventListener(notificationChildEventListener);
    }

    private ChildEventListener createListener(Context parent) {
        return new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                if (!snapshot.exists())
                    return;

                Notification oObject = snapshot.getValue(Notification.class);

                if (oObject.isNotificationRead())
                    return;

                existNewNotification = true;

                UserProfileViewModel.getInstance().setNotificationIcon(existNewNotification);

                if (oObject.getNotificationType() == null)
                    oObject.setNotificationType(NotificationType.Nachricht);

                if (oObject.getNotificationType() == NotificationType.Bestätigung) {
                    UserProfileViewModel.getInstance().updateRelationshipRequestAccepted(oObject);
                } else if (oObject.getNotificationType() == NotificationType.Ablehnung) {
                    UserProfileViewModel.getInstance().updateRelationshipRequestDenied(oObject);
                } else if (oObject.getNotificationType() == NotificationType.Freundschaftskündigung) {
                    UserProfileViewModel.getInstance().handleEndedFriendship(parent, oObject.getNotificationSenderUID());
                }

                Utilities.showNotification(parent,
                        oObject.getNotificationType().toString(),
                        oObject.getNotificationText());
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };
    }

    public FirebaseRecyclerOptions<Notification> getNotificationOptions(BasicActivity parent) {
        return new FirebaseRecyclerOptions.Builder<Notification>()
                .setQuery(queryUserNotification, Notification.class)
                .setLifecycleOwner(parent)
                .build();
    }

    public void sendNotificationRequest(String strMessage, String strSender, String strReceiver) {
        Notification oObject = new Notification();
        oObject.setNotificationText(strMessage);
        oObject.setNotificationType(NotificationType.Anfrage);
        oObject.setNotificationSenderUID(strSender);
        oObject.setNotificationReceiverUID(strReceiver);
        oObject.setNotificationTimestamp(System.currentTimeMillis());
        oObject.setNotificationKey(userNotificationDBRef.push().getKey());

        userNotificationDBRef.child(oObject.getNotificationKey()).setValue(oObject);
    }

    private void sendNotificationRequestAccpted(String strMessage, String strSender, String strReceiver) {
        Notification oObject = new Notification();
        oObject.setNotificationText(strMessage);
        oObject.setNotificationType(NotificationType.Bestätigung);
        oObject.setNotificationSenderUID(strSender);
        oObject.setNotificationReceiverUID(strReceiver);
        oObject.setNotificationTimestamp(System.currentTimeMillis());
        oObject.setNotificationKey(userNotificationDBRef.push().getKey());

        userNotificationDBRef.child(oObject.getNotificationKey()).setValue(oObject);
    }

    private void sendNotificationRequestDenied(String strMessage, String strSender, String strReceiver) {
        Notification oObject = new Notification();
        oObject.setNotificationText(strMessage);
        oObject.setNotificationType(NotificationType.Ablehnung);
        oObject.setNotificationSenderUID(strSender);
        oObject.setNotificationReceiverUID(strReceiver);
        oObject.setNotificationTimestamp(System.currentTimeMillis());
        oObject.setNotificationKey(userNotificationDBRef.push().getKey());

        userNotificationDBRef.child(oObject.getNotificationKey()).setValue(oObject);
    }

    public void confirmNotification(Notification oObject, String strMessage) {
        if (oObject == null || oObject.getNotificationKey() == null)
            return;

        oObject.setNotificationConfirmed(true);
        oObject.setNotificationRead(true);
        userNotificationDBRef.child(oObject.getNotificationKey()).setValue(oObject)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {

                        String strContactUID = oObject.getNotificationSenderUID();
                        sendNotificationRequestAccpted(
                                strMessage,
                                loggedUserID, strContactUID
                        );
                    }
                });
    }

    public void denyNotification(Notification oNotification, String strMessage) {
        if (oNotification == null || oNotification.getNotificationKey() == null)
            return;

        userNotificationDBRef.child(oNotification.getNotificationKey()).removeValue()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {

                        String strContactUID = oNotification.getNotificationSenderUID();
                        sendNotificationRequestDenied(
                                strMessage,
                                loggedUserID, strContactUID);
                    }
                });
    }

    public void setNotificationToRead(Notification oNotification) {
        if (oNotification == null || oNotification.getNotificationKey() == null)
            return;

        oNotification.setNotificationRead(true);

        userNotificationDBRef.child(oNotification.getNotificationKey()).setValue(oNotification)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        ;
                    }
                });
    }

    public void sendNotificationRelationshipCanceled(String strMessage, String strSender, String strReceiver) {
        Notification oObject = new Notification();
        oObject.setNotificationText(strMessage);
        oObject.setNotificationType(NotificationType.Freundschaftskündigung);
        oObject.setNotificationSenderUID(strSender);
        oObject.setNotificationReceiverUID(strReceiver);
        oObject.setNotificationTimestamp(System.currentTimeMillis());
        oObject.setNotificationKey(userNotificationDBRef.push().getKey());

        userNotificationDBRef.child(oObject.getNotificationKey()).setValue(oObject);
    }

    public Query getQueryNotificationsByReceiver() {
        if (userNotificationDBRef == null || loggedUserID == null)
            return null;

        return userNotificationDBRef.orderByChild("notificationReceiverUID").equalTo(loggedUserID);
    }

    public Query getQueryNotificationsBySender() {
        if (userNotificationDBRef == null || loggedUserID == null)
            return null;

        return userNotificationDBRef.orderByChild("notificationSenderUID").equalTo(loggedUserID);
    }
}