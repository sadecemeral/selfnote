package de.mh.selfnote;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import javax.annotation.Nullable;

import de.mh.selfnote.adapter.NoteListAdapter;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.extended.WrapContentLinearLayoutManager;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class SearchNotesFragment extends Fragment {

    public static final String TAG = "SearchNotesFragment";

    private NoteListAdapter _adapter;
    private RecyclerView _viewResult;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        _viewResult = view.findViewById(R.id.result_note_list);
        _viewResult.setLayoutManager(new WrapContentLinearLayoutManager(getActivity()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (ViewGroup) inflater.inflate(
                R.layout.fragment_search_notes, container, false);
    }

    public void startSearch(String strSearch) {
        if (_adapter == null) {
            _adapter = new NoteListAdapter((BasicActivity) getActivity(), UserProfileViewModel.getNoteViewModel().getResultNoteOptions(this, strSearch));
        } else {
            _adapter.updateOptions(UserProfileViewModel.getNoteViewModel().getResultNoteOptions(this, strSearch));
        }

        _viewResult.setAdapter(_adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (_adapter != null)
            _adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (_adapter != null) {
            _adapter.stopListening();
        }
    }
}