package de.mh.selfnote;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.widget.ImageButton;

import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import de.mh.selfnote.adapter.ViewPagerAdapter;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.provider.SearchSuggestionProvider;
import de.mh.selfnote.toolbox.AppManager;

public class SearchActivity extends BasicActivity implements SearchView.OnQueryTextListener {

    private static final String TAG = "SearchActivity";
    private SearchRecentSuggestions _suggestions;
    private Toolbar _toolbar;
    private ImageButton cancelBtn;
    private SearchView _searchView;
    private ViewPager2 _viewPager;
    private TabLayout _tabLayout;
    private ViewPagerAdapter _pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //ProgressBar
        mProgressBar = findViewById(R.id.progressBar_search);

        //Suche
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        _searchView = findViewById(R.id.search_view);
        _searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        _searchView.setIconifiedByDefault(false);
        _searchView.requestFocus();
        _searchView.setOnQueryTextListener(this);
        _searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return true;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                Cursor cursor = _searchView.getSuggestionsAdapter().getCursor();
                cursor.moveToPosition(position);

                String suggestion = cursor.getString(2);//2 is the index of col containing suggestion name.
                _searchView.setQuery(suggestion, true);//setting suggestion
                return true;
            }
        });

        //Abbrechen
        cancelBtn = findViewById(R.id.cancel_search_button);
        cancelBtn.setOnClickListener(view -> {
            AppManager.getAppManager().finishActivity(this, RESULT_CANCELED);
        });

        //Toolbar
        _toolbar = findViewById(R.id.search_toolbar);
        setSupportActionBar(_toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        _suggestions = new SearchRecentSuggestions(this,
                SearchSuggestionProvider.AUTHORITY, SearchSuggestionProvider.MODE);

        //Pager
        _tabLayout = findViewById(R.id.search_tab_layout);
        _viewPager = findViewById(R.id.search_viewpager);
        _pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), getLifecycle());
        _pagerAdapter.addFragment(new SearchUserFragment(), 0);
        //_pagerAdapter.addFragment(new SearchNotesFragment(), 1);
        _viewPager.setAdapter(_pagerAdapter);

        new TabLayoutMediator(_tabLayout, _viewPager,
                (tab, position) -> {
                    switch (position) {
                        case 0:
                            tab.setText("Trefferliste");
                            break;
                        case 1:
                            tab.setText("Notizen");
                            break;
                    }
                }
        ).attach();
    }

    @Override
    public void onBackPressed() {
        if (_viewPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            _viewPager.setCurrentItem(_viewPager.getCurrentItem() - 1);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        setIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String strQuery = intent.getStringExtra(SearchManager.QUERY);

            if (strQuery == null || strQuery.length() < 1) {
                return;
            }

            strQuery = strQuery.trim();
            _suggestions.saveRecentQuery(strQuery, null);
            switch (_tabLayout.getSelectedTabPosition()) {
                case 0:
                    ((SearchUserFragment) _pagerAdapter.getItem(0)).startSearch(strQuery);
                    break;
                case 1:
                    ((SearchNotesFragment) _pagerAdapter.getItem(1)).startSearch(strQuery);
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }
}