/*
package de.mh.selfnote.provider;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import de.mh.selfnote.database.SelfNoteRepository;
import de.mh.selfnote.database.SelfNoteRoomDatabase;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.objects.Note;

public class SelfNoteContentProvider extends ContentProvider {
    // defining authority so that other application can access it
    public static final String AUTHORITY = "de.mh.selfnote.provider";
    // defining content URI and parsing the content URI
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + Note.TABLE_NAME);

    private static final UriMatcher MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
    private static final int ALLROWS = 1;
    private static final int SINGLE_ROW = 2;
    private static final int SEARCH = 3;

    static {
        MATCHER.addURI(AUTHORITY, Note.TABLE_NAME, ALLROWS);
        MATCHER.addURI(AUTHORITY, Note.TABLE_NAME + "/*", SINGLE_ROW);
        MATCHER.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY, SEARCH);
        MATCHER.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY + "/*", SEARCH);
        MATCHER.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_SHORTCUT, SEARCH);
        MATCHER.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_SHORTCUT + "/*", SEARCH);
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s,
                        @Nullable String[] strings1, @Nullable String s1) {

        //Anfrage ausführen und Cursor liefern

        final int code = MATCHER.match(uri);
        if (code == ALLROWS || code == SINGLE_ROW) {
            final Context ctx = getContext();
            if (ctx == null)
                return null;

            final Cursor cursor;

            if (code == ALLROWS)
                cursor = SelfNoteRepository.getInstance(((BasicActivity) ctx).getApplication()).selectAllNote();
            else
                cursor = SelfNoteRepository.getInstance(((BasicActivity) ctx).getApplication()).selectNote((int) ContentUris.parseId(uri));

            cursor.setNotificationUri(ctx.getContentResolver(), uri);

            return cursor;
        } else
            throw new IllegalArgumentException("Unknown URI: " + uri);
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        //Liefere den MIME-Typ der Anfrage
        switch (MATCHER.match(uri)) {
            case ALLROWS:
                return "vnd.android.cursor.dir/" + AUTHORITY + "." + Note.TABLE_NAME;
            case SINGLE_ROW:
                return "vnd.android.cursor.item/" + AUTHORITY + "." + Note.TABLE_NAME;
            case SEARCH:
                return SearchManager.SUGGEST_MIME_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {

        final Context ctx = getContext();
        if (ctx == null) {
            return null;
        }
        final long id = SelfNoteRepository.getInstance(((BasicActivity) ctx).getApplication())
                .insert(Note.fromContentValues(contentValues));

        if(id > -1) {
            Uri insertedId = ContentUris.withAppendedId(CONTENT_URI, id);

            //Informiert die Observer über die Änderung im Data Set
            ctx.getContentResolver().notifyChange(insertedId, null);
            return insertedId;
        }

        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        switch (MATCHER.match(uri)) {
            case ALLROWS:
                throw new IllegalArgumentException("Invalid URI, cannot update without ID" + uri);
            case SINGLE_ROW:
                final Context ctx = getContext();
                if (ctx == null) {
                    return 0;
                }
                final int deleteCount = SelfNoteRepository.getInstance(((BasicActivity) ctx).getApplication())
                        .deleteNoteById((int) ContentUris.parseId(uri));
                ctx.getContentResolver().notifyChange(uri, null);
                return deleteCount;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        switch (MATCHER.match(uri)) {
            case ALLROWS:
                throw new IllegalArgumentException("Invalid URI, cannot update without ID" + uri);
            case SINGLE_ROW:
                final Context ctx = getContext();
                if (ctx == null) {
                    return 0;
                }
                final Note note = Note.fromContentValues(contentValues);
                final int updatedCount = SelfNoteRepository.getInstance(((BasicActivity) ctx).getApplication())
                        .update(note);
                ctx.getContentResolver().notifyChange(uri, null);
                return updatedCount;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }
}
*/
