package de.mh.selfnote;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;

import de.mh.selfnote.adapter.NoteListAdapter;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.extended.WrapContentLinearLayoutManager;
import de.mh.selfnote.objects.UserProfile;
import de.mh.selfnote.toolbox.Utilities;
import de.mh.selfnote.viewModel.NoteViewModel;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class UserNotesActivity extends BasicActivity {

    private static final String TAG = "UserNotesActivity";
    private NoteViewModel _viewNote;
    private RecyclerView _viewResult;
    private NoteListAdapter _adapter;
    private UserProfile userProfile;

    TextView viewUserID;
    TextView viewUserName;
    ShapeableImageView viewImage;
    TextView viewUserEMail;
    TextView viewUserBirthday;

    public static final String EXTRA_PARAM_ID = "FIREBASEUID";

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_notes);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //ProgressBar
        mProgressBar = findViewById(R.id.progressBar_user_notes);
        mProgressBar.setVisibility(View.VISIBLE);

        viewUserID = findViewById(R.id.view_userprofile_userid);
        viewUserName = findViewById(R.id.view_userprofile_username);
        viewImage = findViewById(R.id.view_userprofile_userimage);
        viewUserEMail = findViewById(R.id.view_userprofile_useremail);
        viewUserBirthday = findViewById(R.id.view_userprofile_userbirthday);

        //NoteViewModel
        _viewNote = UserProfileViewModel.getNoteViewModel();

        //Views suchen
        _viewResult = findViewById(R.id.user_note_list);
        _viewResult.setLayoutManager(new WrapContentLinearLayoutManager(this));

        fillInputFields();
    }

    private void fillInputFields() {
        userProfile = UserProfileViewModel.getUserProfile(getIntent().getStringExtra(EXTRA_PARAM_ID));

        if (userProfile == null)
            return;

        viewUserID.setText(userProfile.getUserID());
        viewUserName.setText("Name: " + userProfile.getUserName());
        //viewUserEMail.setText("Email: " + userProfile.getUserEmail());
        //viewUserBirthday.setText("Geburtstag: " + Utilities.formatDate("dd.MM.yyyy", userProfile.getUserBirthday()));

        Utilities.loadUserprofilePhotoIntoView(this, userProfile, viewImage);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (_adapter != null) {
            _adapter.startListening();
        } else if(userProfile != null) {
            //Adapter initialisierung
            _adapter = new NoteListAdapter(this, _viewNote.getNoteOptions(this), userProfile.getFirebaseUID());
            _viewResult.setAdapter(_adapter);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (_adapter != null) {
            _adapter.stopListening();
        }
    }
}
