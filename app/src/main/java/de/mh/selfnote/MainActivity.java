package de.mh.selfnote;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.viewpager2.widget.ViewPager2;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;

import de.mh.selfnote.adapter.ViewPagerAdapter;
import de.mh.selfnote.adapter.ZoomOutPageTransformer;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.toolbox.AppManager;
import de.mh.selfnote.viewModel.NoteViewModel;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class MainActivity extends BasicActivity {

    private static final String TAG = "MainActivity";
    private NoteViewModel _viewNote;
    private FloatingActionsMenu _fabMenu;
    private ViewPager2 _viewPager;
    private TabLayout _tabLayout;
    private ViewPagerAdapter _pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ic_actionbar_selfnote);

        //ProgressBar
        mProgressBar = findViewById(R.id.progressBar_main);
        mProgressBar.setVisibility(View.VISIBLE);

        //NoteViewModel
        _viewNote = UserProfileViewModel.getNoteViewModel();

        //Pager
        _tabLayout = findViewById(R.id.main_tab_layout);
        _viewPager = findViewById(R.id.main_viewpager);
        _pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), getLifecycle());
        _pagerAdapter.addFragment(new MainNotesFragment(), 0);
        _pagerAdapter.addFragment(new MainPinnedNotesFragment(), 1);
        _viewPager.setAdapter(_pagerAdapter);
        _viewPager.setPageTransformer(new ZoomOutPageTransformer());

        new TabLayoutMediator(_tabLayout, _viewPager,
                (tab, position) -> {
                    switch (position) {
                        case 0:
                            tab.setIcon(R.drawable.ic_main_note_list);
                            break;
                        case 1:
                            tab.setIcon(R.drawable.ic_pin_note_item);
                            break;
                    }
                }
        ).attach();

        //Buttons
        _fabMenu = findViewById(R.id.fab_add_new_menu);

        FloatingActionButton fab = findViewById(R.id.fab_add_new_note);
        fab.setOnClickListener(view -> {
            activityLauncher.launch(new Intent(MainActivity.this, AddNewNoteActivity.class), result -> {
                insertItem(result.getResultCode());
            });
        });

        fab = findViewById(R.id.fab_add_new_diary);
        fab.setOnClickListener(view -> {
            activityLauncher.launch(new Intent(MainActivity.this, DiaryActivity.class), result -> {
                insertItem(result.getResultCode());
            });
        });
    }

    private void insertItem(int resultCode) {

        _fabMenu.collapseImmediately();

        if (resultCode != Activity.RESULT_OK) {
            Toast.makeText(getApplicationContext(), R.string.toast_save_new_canceled,
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (_viewPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            _viewPager.setCurrentItem(_viewPager.getCurrentItem() - 1);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //ActionBar
        this.getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        UserProfileViewModel.getInstance().setMainMenu(this, menu);

        return true;
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        int iItemId = R.id.SubMenuItemShowByOwner;
        MenuItem oItem = menu.findItem(iItemId);
        if (oItem == null)
            return super.onMenuOpened(featureId, menu);

        boolean bVisible = false;
        switch (_tabLayout.getSelectedTabPosition()) {
            case 0:
                bVisible = true;
                break;
        }
        oItem.setVisible(bVisible);

        return super.onMenuOpened(featureId, menu);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.SubMenuItemShowByOwner:

                if (item.isChecked())
                    item.setChecked(false);
                else
                    item.setChecked(true);

                _viewNote.updateNoteListAdapter(_pagerAdapter.getItem(0), item.isChecked());

                return true;

            case R.id.SubMenuItemCategoryFilter:
                activityLauncher.launch(new Intent(MainActivity.this, CategoryFilterActivity.class), result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {

                        Intent resultData = result.getData();
                        if (resultData == null || !resultData.hasExtra("CATEGORY_LIST_ENABLED"))
                            return;

                        ArrayList<String> enabledList = resultData.getStringArrayListExtra("CATEGORY_LIST_ENABLED");
                        //_adapter.filterData(enabledList);

                        ((MainNotesFragment) _pagerAdapter.getItem(0)).filterListByCategory(enabledList);
                        ((MainPinnedNotesFragment) _pagerAdapter.getItem(1)).filterListByCategory(enabledList);
                    }
                });
                return true;

            case R.id.SubMenuItemSortNoteList:
                activityLauncher.launch(new Intent(MainActivity.this, SortKeySetActivity.class), result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {

                        Intent resultData = result.getData();
                        if (resultData == null || !resultData.hasExtra("SORTKEY_LIST_ENABLED"))
                            return;

                        ArrayList<String> enabledList = resultData.getStringArrayListExtra("SORTKEY_LIST_ENABLED");
                        //_adapter.sortData(enabledList.toArray(new String[0]));
                        ((MainNotesFragment) _pagerAdapter.getItem(0)).sortListBySortKey(enabledList);
                        ((MainPinnedNotesFragment) _pagerAdapter.getItem(1)).sortListBySortKey(enabledList);
                    }
                });
                return true;
            case R.id.MenuItemSearchNote:
                startActivity(new Intent(MainActivity.this, SearchActivity.class));
                return true;
            case R.id.SubMenuItemUserProfile:
                startActivity(new Intent(MainActivity.this, UserProfileActivity.class));
                return true;
            case R.id.SubMenuItemUserLogout:
                UserProfileViewModel.getInstance().clearUserProfile(MainActivity.this, true);

                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                AppManager.getAppManager().finishOthersActivity(LoginActivity.class);

                return true;
            case R.id.MenuItemUserNotifications:
                startActivity(new Intent(MainActivity.this, NotificationActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // [START storage_upload_lifecycle]
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // If there's an upload in progress, save the reference so you can query it later
        if (_viewNote.getUserImageReference() != null) {
            outState.putString("reference", _viewNote.getUserImageReference().toString());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // If there was an upload in progress, get its reference and create a new StorageReference
        final String stringRef = savedInstanceState.getString("reference");
        if (stringRef == null) {
            return;
        }
        StorageReference mStorageRef = FirebaseStorage.getInstance().getReferenceFromUrl(stringRef);

        // Find all UploadTasks under this StorageReference (in this example, there should be one)
        List<UploadTask> tasks = mStorageRef.getActiveUploadTasks();
        if (tasks.size() > 0) {
            // Get the task monitoring the upload
            UploadTask task = tasks.get(0);

            // Add new listeners to the task using an Activity scope
            task.addOnSuccessListener(this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot state) {
                    // Success!
                    // ...
                }
            });
        }
    }
    // [END storage_upload_lifecycle]
}