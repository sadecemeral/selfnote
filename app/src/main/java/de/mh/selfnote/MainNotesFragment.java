package de.mh.selfnote;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import javax.annotation.Nullable;

import de.mh.selfnote.adapter.NoteListAdapter;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.extended.WrapContentLinearLayoutManager;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class MainNotesFragment extends Fragment {

    public static final String TAG = "MainNotesFragment";

    private RecyclerView _viewResult;
    private NoteListAdapter _adapter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //Views suchen
        _viewResult = view.findViewById(R.id.main_note_list);
        _viewResult.setLayoutManager(new WrapContentLinearLayoutManager(getActivity()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_notes, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (_adapter != null) {
            _adapter.startListening();
        } else if(UserProfileViewModel.getNoteViewModel() != null) {
            //Adapter initialisierung
            _adapter = UserProfileViewModel.getNoteViewModel().createNoteListAdapter((BasicActivity) getActivity(), this);
            _viewResult.setAdapter(_adapter);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (_adapter != null) {
            _adapter.stopListening();
        }
    }

    public void filterListByCategory(ArrayList<String> enabledList) {
        if (_adapter != null)
            _adapter.filterData(enabledList);
    }

    public void sortListBySortKey(ArrayList<String> enabledList) {
        if (_adapter != null)
            _adapter.sortData(enabledList.toArray(new String[0]));
    }
}