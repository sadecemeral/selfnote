package de.mh.selfnote;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import javax.annotation.Nullable;

import de.mh.selfnote.adapter.PinnedNoteListAdapter;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.objects.Note;
import de.mh.selfnote.toolbox.Utilities;
import de.mh.selfnote.viewModel.CategoryViewModel;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class MainPinnedNotesFragment extends Fragment {

    public static final String TAG = "MainPinnedNotesFragment";

    private GridView gridView;
    private PinnedNoteListAdapter _adapter;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        gridView = view.findViewById(R.id.pinned_note_list);
        gridView.setOnItemClickListener(mOnItemClickListener);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (_adapter != null) {
            _adapter.startListening();
        } else if(UserProfileViewModel.getNoteViewModel() != null) {
            _adapter = UserProfileViewModel.getNoteViewModel().createPinnedNoteListAdapter((BasicActivity) getActivity(), this);
            gridView.setAdapter(_adapter);
        }
    }

    @Override
    public void onStop() {
        super.onStop();

        if (_adapter != null) {
            _adapter.stopListening();
        }
    }

    public void filterListByCategory(ArrayList<String> enabledList) {
        if (_adapter != null)
            _adapter.filterData(enabledList);
    }

    public void sortListBySortKey(ArrayList<String> enabledList) {
        if (_adapter != null)
            _adapter.sortData(enabledList.toArray(new String[0]));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(
                R.layout.fragment_pinned_notes, container, false);
    }

    private final AdapterView.OnItemClickListener mOnItemClickListener
            = new AdapterView.OnItemClickListener() {

        /**
         * Called when an item in the {@link android.widget.GridView} is clicked. Here will launch
         * the , using the Scene Transition animation functionality.
         */
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Note item = (Note) adapterView.getItemAtPosition(position);

            // Construct an Intent as normal
            Intent intent = new Intent(MainPinnedNotesFragment.this.getContext(), SingleNoteActivity.class);
            intent.putExtra(SingleNoteActivity.EXTRA_PARAM_ID, item.getNoteKey());
            intent.putExtra(SingleNoteActivity.NOTE_CATEGORY_COLOR, item.getNoteCategory().getCategoryColor());
            intent.putExtra(SingleNoteActivity.NOTE_PERSONAL, item.isNotePersonal());
            intent.putExtra(SingleNoteActivity.NOTE_TIMESTAMP, item.getNoteTimestamp());
            intent.putExtra(SingleNoteActivity.NOTE_TITLE, item.getNoteTitle());
            intent.putExtra(SingleNoteActivity.NOTE_TEXT, item.getNoteText());

            if (item.getNoteKeyword() != null && item.getNoteKeyword().length() > 0)
                intent.putExtra(SingleNoteActivity.NOTE_KEYWORD, item.getNoteKeyword());

            if (TextUtils.equals(item.getNoteCategory().getCategoryName(), CategoryViewModel.categoryName_Diary))
                intent.putExtra(SingleNoteActivity.NOTE_RATING, item.getNoteRating());

            ImageView imgView = view.findViewById(R.id.pinned_note_list_imageview_item);
            intent.putStringArrayListExtra(SingleNoteActivity.NOTE_IMAGE_LIST, item.getNoteImageListURL());
            intent.putExtra(SingleNoteActivity.NOTE_IMAGE, Utilities.getBytes(imgView));

            // BEGIN_INCLUDE(start_activity)
            /*
             * Now create an {@link android.app.ActivityOptions} instance using the
             * {@link ActivityOptionsCompat#makeSceneTransitionAnimation(Activity, Pair[])} factory
             * method.
             */
            @SuppressWarnings("unchecked")
            ActivityOptionsCompat activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
                    getActivity(),

                    // Now we provide a list of Pair items which contain the view we can transitioning
                    // from, and the name of the view it is transitioning to, in the launched activity
                    new Pair<>(view.findViewById(R.id.pinned_note_list_imageview_item),
                            SingleNoteActivity.VIEW_NAME_HEADER_IMAGE),
                    new Pair<>(view.findViewById(R.id.pinned_note_list_textview_name),
                            SingleNoteActivity.VIEW_NAME_HEADER_TITLE));

            // Now we can start the Activity, providing the activity options as a bundle
            ActivityCompat.startActivity(getActivity(), intent, activityOptions.toBundle());
            // END_INCLUDE(start_activity)
        }
    };
}