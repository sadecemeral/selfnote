package de.mh.selfnote;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import de.mh.selfnote.extended.WrapContentLinearLayoutManager;
import de.mh.selfnote.objects.Category;
import de.mh.selfnote.toolbox.AppManager;
import de.mh.selfnote.viewModel.CategoryViewModel;
import de.mh.selfnote.viewModel.UserProfileViewModel;


public class CategoryFilterActivity extends Activity implements LifecycleOwner {

    private CategoryFilterActivity_Adapter _adapter;
    private ProgressBar mProgressBar;
    private LifecycleRegistry lifecycleRegistry;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lifecycleRegistry = new LifecycleRegistry(this);
        lifecycleRegistry.setCurrentState(Lifecycle.State.CREATED);

        setContentView(R.layout.activity_category_filter);
        setTitle(getString(R.string.activity_title_categoryFilter));

        mProgressBar = findViewById(R.id.progressBar_categoryfilter);

        //ViewModel
        CategoryViewModel _viewModel = UserProfileViewModel.getCategoryViewModel();
        //get data and initialize list view adapter
        _adapter = new CategoryFilterActivity_Adapter(
                _viewModel.getCategoryAdapterOptions(this),
                this);

        //Views suchen
        RecyclerView filterListView = findViewById(R.id.category_filter_list);
        filterListView.setLayoutManager(new WrapContentLinearLayoutManager(this));
        filterListView.setAdapter(_adapter);

        Button btnOK = findViewById(R.id.btn_category_selection_ok);
        btnOK.setOnClickListener(view -> {
            Intent replyIntent = new Intent();
            List<Category> dataList = _adapter.getModifiedCategoryList();
            if (dataList != null && dataList.size() > 0) {

                ArrayList<String> enabledList = new ArrayList();

                for (Category oObject : dataList) {
                    if (oObject.isCategoryEnabled())
                        enabledList.add(oObject.getCategoryName());

                    _viewModel.save(oObject);
                }

                replyIntent.putStringArrayListExtra("CATEGORY_LIST_ENABLED", enabledList);
            }

            AppManager.getAppManager().finishActivity(this, RESULT_OK, replyIntent);
        });

        Button btnCancel = findViewById(R.id.btn_category_selection_cancel);
        btnCancel.setOnClickListener(view -> {
            AppManager.getAppManager().finishActivity(this, RESULT_CANCELED);
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        lifecycleRegistry.setCurrentState(Lifecycle.State.STARTED);

        if (_adapter != null)
            _adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (_adapter != null) {
            _adapter.stopListening();
        }
    }

    public void startProgressBar() {
        if (mProgressBar != null)
            mProgressBar.setVisibility(View.VISIBLE);
    }

    public void finishProgessBar() {
        if (mProgressBar != null)
            mProgressBar.setVisibility(View.GONE);
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return lifecycleRegistry;
    }
}


class CategoryFilterActivity_Adapter extends FirebaseRecyclerAdapter<Category, CategoryFilterActivity_Adapter.CategoryFilterViewHolder> {

    final private Hashtable<String, Category> hashTable = new Hashtable<>();
    List<Category> dataList;
    final Context parent;
    ArrayList<Category> dataChangedList = new ArrayList<>();

    public List<Category> getModifiedCategoryList() {
        return dataList;
    }

    public CategoryFilterActivity_Adapter(FirebaseRecyclerOptions<Category> options,
                                          Context ctx) {
        super(options);
        this.parent = ctx;
    }

    @NonNull
    @Override
    public CategoryFilterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.category_filter_list_item, viewGroup, false);
        return new CategoryFilterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryFilterViewHolder holder, int position) {
        Category current = dataList.get(position);
        holder.bind(current);
    }

    @Override
    protected void onBindViewHolder(@NonNull CategoryFilterViewHolder holder, int position,
                                    @NonNull Category model) {
        Category current = dataList.get(position);
        holder.bind(current);
    }

    @Override
    public int getItemCount() {
        if (dataList != null)
            return dataList.size();

        return 0;
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();

        dataList = null;

        if (hashTable != null && hashTable.size() > 0)
            dataList = new ArrayList<>(hashTable.values());

        if (parent != null && parent instanceof CategoryFilterActivity)
            ((CategoryFilterActivity) parent).finishProgessBar();
    }

    @Override
    public void onChildChanged(@NonNull ChangeEventType type,
                               @NonNull DataSnapshot snapshot,
                               int newIndex,
                               int oldIndex) {

        if (parent != null && parent instanceof CategoryFilterActivity)
            ((CategoryFilterActivity) parent).startProgressBar();

        if (snapshot.exists()) {

            Category category = snapshot.getValue(Category.class);

            switch (type) {
                case ADDED:
                case CHANGED:
                    hashTable.put(category.getCategoryKey(), category);
                    break;
                case REMOVED:
                    hashTable.remove(category.getCategoryKey());
                    break;
            }
        }

        super.onChildChanged(type, snapshot, newIndex, oldIndex);
    }

    class CategoryFilterViewHolder extends RecyclerView.ViewHolder {
        final TextView CategoryTitle;
        final CheckBox CategoryCheckBox;
        Category Category;

        public CategoryFilterViewHolder(View itemView) {
            super(itemView);
            CategoryTitle = itemView.findViewById(R.id.category_filter_text_list_item);
            CategoryCheckBox = itemView.findViewById(R.id.category_filter_checkbox_list_item);

            CategoryCheckBox.setOnCheckedChangeListener((compoundButton, b) -> {
                Category.setCategoryEnabled(b);
                dataChangedList.add(Category);
            });
        }

        public void bind(Category item) {
            if (item == null)
                return;

            Category = item;
            CategoryTitle.setText(item.getCategoryName());
            CategoryCheckBox.setChecked(item.isCategoryEnabled());
            CategoryCheckBox.setButtonTintList(ColorStateList.valueOf(item.getCategoryColor()));
        }
    }
}