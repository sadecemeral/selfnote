package de.mh.selfnote;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

import de.mh.selfnote.extended.WrapContentLinearLayoutManager;
import de.mh.selfnote.objects.SortKey;
import de.mh.selfnote.toolbox.AppManager;
import de.mh.selfnote.toolbox.ItemMoveCallback;
import de.mh.selfnote.toolbox.StartDragListener;
import de.mh.selfnote.viewModel.SortViewModel;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class SortKeySetActivity extends Activity implements StartDragListener, LifecycleOwner {

    private ItemTouchHelper touchHelper;
    private SortKeySetActivity_Adapter _adapter;
    private ProgressBar mProgressBar;
    private LifecycleRegistry lifecycleRegistry;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lifecycleRegistry = new LifecycleRegistry(this);
        lifecycleRegistry.setCurrentState(Lifecycle.State.CREATED);

        setContentView(R.layout.activity_sortkey_list);
        setTitle(getString(R.string.activity_title_sortBy));

        mProgressBar = findViewById(R.id.progressBar_sortkeySet);

        SortViewModel _viewModel = UserProfileViewModel.getSortViewModel();
        //get data and initialize list view adapter
        _adapter = new SortKeySetActivity_Adapter(
                _viewModel.getSortKeyOptions(this),
                this,
                this);

        //Views suchen
        RecyclerView filterListView = findViewById(R.id.sortkey_list);
        filterListView.setLayoutManager(new WrapContentLinearLayoutManager(this));
        filterListView.setAdapter(_adapter);

        //Drag&Drop
        ItemTouchHelper.Callback callback = new ItemMoveCallback(_adapter);
        touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(filterListView);

        Button btnOK = findViewById(R.id.btn_sortkey_selection_ok);
        btnOK.setOnClickListener(view -> {
            Intent replyIntent = new Intent();
            List<SortKey> dataList = _adapter.getModifiedSortKeyList();
            if (dataList != null && dataList.size() > 0) {

                ArrayList<String> enabledList = new ArrayList();

                for (SortKey oObject : dataList) {
                    _viewModel.save(oObject);
                    if (oObject.isSortKeyEnabled())
                        enabledList.add(oObject.getSortKeyNoteCol());
                }

                replyIntent.putStringArrayListExtra("SORTKEY_LIST_ENABLED", enabledList);
            }

            AppManager.getAppManager().finishActivity(this, RESULT_OK, replyIntent);
        });

        Button btnCancel = findViewById(R.id.btn_sortkey_selection_cancel);
        btnCancel.setOnClickListener(view -> { AppManager.getAppManager().finishActivity(this, RESULT_CANCELED);
        });
    }

    @Override
    public void requestDrag(RecyclerView.ViewHolder viewHolder) {
        touchHelper.startDrag(viewHolder);
    }

    @Override
    protected void onStart() {
        super.onStart();

        lifecycleRegistry.setCurrentState(Lifecycle.State.STARTED);

        if (_adapter != null)
            _adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (_adapter != null) {
            _adapter.stopListening();
        }
    }

    public void startProgressBar() {
        if (mProgressBar != null)
            mProgressBar.setVisibility(View.VISIBLE);
    }

    public void finishProgessBar() {
        if (mProgressBar != null)
            mProgressBar.setVisibility(View.GONE);
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return lifecycleRegistry;
    }
}

class SortKeySetActivity_Adapter extends FirebaseRecyclerAdapter<SortKey, SortKeySetActivity_Adapter.SortKeyViewHolder>
        implements ItemMoveCallback.ItemTouchHelperContract {

    final private Hashtable<String, SortKey> hashTable = new Hashtable<>();
    List<SortKey> dataList;
    final StartDragListener startDragListener;
    private Drawable defaultBackground;
    final Context parent;

    public List<SortKey> getModifiedSortKeyList() {
        return dataList;
    }

    public SortKeySetActivity_Adapter(FirebaseRecyclerOptions<SortKey> options,
                                      StartDragListener startDragListener,
                                      Context parent) {
        super(options);
        this.startDragListener = startDragListener;
        this.parent = parent;
    }

    public void setSingleSelection(SortKey oObject) {
        if (oObject == null)
            return;

        for (SortKey sortKey : dataList) {
            if (sortKey.getSortKeyKey() == oObject.getSortKeyKey())
                oObject.setSortKeyEnabled(true);
            else
                sortKey.setSortKeyEnabled(false);
        }

        notifyDataSetChanged();
    }

    @Override
    public void onRowMoved(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(dataList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(dataList, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onRowSelected(RecyclerView.ViewHolder myViewHolder) {
        defaultBackground = myViewHolder.itemView.getBackground();
        myViewHolder.itemView.setBackgroundColor(Color.GRAY);
    }

    @Override
    public void onRowClear(RecyclerView.ViewHolder myViewHolder) {
        myViewHolder.itemView.setBackground(defaultBackground);

        if (dataList != null && dataList.size() > 0) {
            int iIndex = -1;
            while (++iIndex < dataList.size())
                dataList.get(iIndex).setSortKeyOrder(iIndex + 1);
        }
    }

    @NonNull
    @Override
    public SortKeyViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.sortkey_list_item, viewGroup, false);

        return new SortKeyViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(@NonNull SortKeyViewHolder holder, int position) {
        holder.bind(dataList.get(position), holder);
    }

    @Override
    protected void onBindViewHolder(@NonNull SortKeyViewHolder holder, int position,
                                    @NonNull SortKey model) {
        holder.bind(dataList.get(position), holder);
    }

    @Override
    public int getItemCount() {
        if (dataList != null)
            return dataList.size();

        return 0;
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();

        dataList = null;

        if (hashTable != null && hashTable.size() > 0)
            dataList = new ArrayList<>(hashTable.values());

        if (parent != null && parent instanceof SortKeySetActivity)
            ((SortKeySetActivity) parent).finishProgessBar();
    }

    @Override
    public void onChildChanged(@NonNull ChangeEventType type,
                               @NonNull DataSnapshot snapshot,
                               int newIndex,
                               int oldIndex) {

        if (parent != null && parent instanceof SortKeySetActivity)
            ((SortKeySetActivity) parent).startProgressBar();

        if (snapshot.exists()) {

            SortKey sortKey = snapshot.getValue(SortKey.class);

            switch (type) {
                case ADDED:
                case CHANGED:
                    hashTable.put(sortKey.getSortKeyKey(), sortKey);
                    break;
                case REMOVED:
                    hashTable.remove(sortKey.getSortKeyKey());
                    break;
            }
        }

        super.onChildChanged(type, snapshot, newIndex, oldIndex);
    }

    class SortKeyViewHolder extends RecyclerView.ViewHolder {
        final TextView SortTitle;
        final CheckBox SortKeyCheckBox;
        final ImageView SortKeyDragDrop;
        SortKey SortKey;

        public SortKeyViewHolder(View itemView, SortKeySetActivity_Adapter parent) {
            super(itemView);
            SortKeyDragDrop = itemView.findViewById(R.id.sortkey_order_dragdrop_list_item);
            SortTitle = itemView.findViewById(R.id.sortkey_text_list_item);
            SortKeyCheckBox = itemView.findViewById(R.id.sortkey_checkbox_list_item);

            SortKeyCheckBox.setOnClickListener(view -> {
                if (((CheckBox)view).isChecked())
                    parent.setSingleSelection(SortKey);
            });
        }

        @SuppressLint("ClickableViewAccessibility")
        public void bind(SortKey item, SortKeyViewHolder holder) {
            if (item == null)
                return;

            SortKey = item;
            SortTitle.setText(item.getSortKeyName());
            SortKeyCheckBox.setChecked(item.isSortKeyEnabled());

            SortKeyDragDrop.setOnTouchListener((view, motionEvent) -> {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    startDragListener.requestDrag(holder);
                }
                return false;
            });
        }
    }
}