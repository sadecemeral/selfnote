package de.mh.selfnote.database;

import android.database.Cursor;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import de.mh.selfnote.objects.Note;
import de.mh.selfnote.objects.NoteAndCategory;

//Data Access Object
@Dao
public interface NoteDAO {

    @Query("SELECT COUNT(*) FROM " + Note.TABLE_NAME)
    int count();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Note note);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Note note);

    @Delete()
    void delete(Note note);

    @Query("DELETE FROM " + Note.TABLE_NAME + " WHERE NOTE_ID = :id")
    int deleteById(int id);

    @Query("DELETE FROM " + Note.TABLE_NAME)
    void deleteAll();

    @Query("SELECT * FROM " + Note.TABLE_NAME)
    Cursor selectAll();

    @Query("SELECT * FROM " + Note.TABLE_NAME + " WHERE NOTE_ID = :id")
    Cursor selectById(int id);

    @Query("SELECT * FROM note_table ORDER BY NOTE_TIMESTAMP DESC")
    LiveData<List<Note>> getListNoteBySortTimestamp();

    @Query("SELECT * " +
            "FROM note_table AS note JOIN category_table AS cat ON cat.CATEGORY_ID = note.NOTE_CATEGORY_ID " +
            "WHERE cat.CATEGORY_ENABLED = 1 ORDER BY note.NOTE_PINNED DESC, note.NOTE_TIMESTAMP DESC")
    LiveData<List<NoteAndCategory>> getListNoteAndCategoryBySortTimestamp();

    @Query("SELECT * " +
            "FROM note_table AS note JOIN category_table AS cat ON cat.CATEGORY_ID = note.NOTE_CATEGORY_ID " +
            "WHERE cat.CATEGORY_ENABLED = 1 ORDER BY note.NOTE_PINNED DESC, note.NOTE_TITLE ASC")
    LiveData<List<NoteAndCategory>> getListNoteAndCategoryBySortTitle();

    @Query("SELECT * " +
            "FROM note_table AS note JOIN category_table AS cat ON cat.CATEGORY_ID = note.NOTE_CATEGORY_ID " +
            "WHERE cat.CATEGORY_ENABLED = 1 ORDER BY note.NOTE_PINNED DESC, cat.CATEGORY_NAME ASC")
    LiveData<List<NoteAndCategory>> getListNoteAndCategoryBySortCategory();

    @Query("SELECT * " +
            "FROM note_table AS note JOIN category_table AS cat ON cat.CATEGORY_ID = note.NOTE_CATEGORY_ID " +
            "WHERE cat.CATEGORY_ENABLED = 1 ORDER BY note.NOTE_PINNED DESC, cat.CATEGORY_NAME ASC, note.NOTE_TITLE ASC")
    LiveData<List<NoteAndCategory>> getListNoteAndCategoryBySortCatTitle();

    @Query("SELECT * " +
            "FROM note_table AS note JOIN category_table AS cat ON cat.CATEGORY_ID = note.NOTE_CATEGORY_ID " +
            "WHERE cat.CATEGORY_ENABLED = 1 ORDER BY note.NOTE_PINNED DESC, note.NOTE_TITLE ASC, cat.CATEGORY_NAME ASC")
    LiveData<List<NoteAndCategory>> getListNoteAndCategoryBySortTitleCat();

    @Query("SELECT * " +
            "FROM note_table AS note JOIN category_table AS cat ON cat.CATEGORY_ID = note.NOTE_CATEGORY_ID " +
            "WHERE cat.CATEGORY_ENABLED = 1 AND " +
            "( NOTE_TEXT LIKE '%' || :strSearch || '%' OR NOTE_TITLE LIKE '%' || :strSearch || '%' OR NOTE_KEYWORD LIKE '%' || :strSearch || '%' " +
            "OR CATEGORY_NAME LIKE '%' || :strSearch || '%' )" +
            "ORDER BY note.NOTE_PINNED DESC")
    LiveData<List<NoteAndCategory>> getListNoteAndCategoryBySearch(String strSearch);
}
