package de.mh.selfnote.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import de.mh.selfnote.objects.Category;

@Dao
public interface CategoryDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Category category);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void update(Category category);

    @Query("DELETE FROM category_table")
    void deleteAll();

    @Query("SELECT * FROM category_table ORDER BY CATEGORY_ID")
    List<Category> getListCategory();

    @Query("SELECT DISTINCT CATEGORY_ID FROM category_table WHERE CATEGORY_NAME = :strCategoryName")
    int getCategoryIdByName(String strCategoryName);
}
