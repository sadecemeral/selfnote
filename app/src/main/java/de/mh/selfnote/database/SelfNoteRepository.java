/*
package de.mh.selfnote.database;

import android.app.Application;
import android.database.Cursor;

import androidx.lifecycle.LiveData;

import java.util.List;

import de.mh.selfnote.objects.Category;
import de.mh.selfnote.objects.Note;
import de.mh.selfnote.objects.NoteAndCategory;
import de.mh.selfnote.objects.SortKey;
import de.mh.selfnote.objects.UserProfile;

public class SelfNoteRepository {

    static private SelfNoteRepository INSTANCE;

    final private NoteDAO mNoteDao;
    final private CategoryDAO mCategoryDao;
    final private SortKeyDAO mSortKeyDao;
    final private UserDAO mUserDao;

    static public SelfNoteRepository getInstance(Application application) {
        if (INSTANCE == null)
            INSTANCE = new SelfNoteRepository(application);

        return INSTANCE;
    }

    private SelfNoteRepository(Application application) {
        SelfNoteRoomDatabase database = SelfNoteRoomDatabase.getDatabase(application);
        mNoteDao = database.noteDao();
        mCategoryDao = database.categoryDao();
        mSortKeyDao = database.sortKeyDao();
        mUserDao = database.userDao();
    }

    public Cursor selectAllNote(){
        return mNoteDao.selectAll();
    }

    public Cursor selectNote(int id){
        return mNoteDao.selectById(id);
    }

    public int getCategoryIdByName(String strCategoryName) {
        return mCategoryDao.getCategoryIdByName(strCategoryName);
    }

    public UserProfile getUserProfile(String userNickname, String userPassword) {
        return mUserDao.getUserProfile(userNickname, userPassword);
    }

    public LiveData<List<NoteAndCategory>> getResultListNoteAndCategory(String strSearch) {
        return mNoteDao.getListNoteAndCategoryBySearch(strSearch);
    }

    public LiveData<List<NoteAndCategory>> getAllNoteAndCategory() {
        String[] sortKeyOrder = this.getAllSortKeysEnabled();

        if (sortKeyOrder == null || sortKeyOrder.length < 1) {
            return mNoteDao.getListNoteAndCategoryBySortTimestamp();
        } else if (sortKeyOrder.length == 1) {
            if (sortKeyOrder[0].equals("NOTE_TITLE"))
                return mNoteDao.getListNoteAndCategoryBySortTitle();
            else
                return mNoteDao.getListNoteAndCategoryBySortCategory();
        } else if (sortKeyOrder.length == 2) {
            if (sortKeyOrder[0].equals("NOTE_TITLE"))
                return mNoteDao.getListNoteAndCategoryBySortTitleCat();
            else
                return mNoteDao.getListNoteAndCategoryBySortCatTitle();
        }

        return null;
    }

    public LiveData<List<Note>> getAllNotes() { return mNoteDao.getListNoteBySortTimestamp(); }

    public List<Category> getAllCategories() {
        return mCategoryDao.getListCategory();
    }

    public List<SortKey> getAllSortKeys() {
        return mSortKeyDao.getListSortKey();
    }

    public String[] getAllSortKeysEnabled() {
        return mSortKeyDao.getListSortKeyEnabled();
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    public long insert(Note note) {

        final long[] id = {0};
        SelfNoteRoomDatabase.databaseWriteExecutor.execute(() -> {
            id[0] = mNoteDao.insert(note);
        });
        return id[0];
    }

    public int update(Note note) {

        final int[] id = {0};
        SelfNoteRoomDatabase.databaseWriteExecutor.execute(() -> {
            id[0] = mNoteDao.update(note);
        });
        return id[0];
    }

    public int deleteNoteById(int id){
        return mNoteDao.deleteById(id);
    }

    public void delete(Note note) {
        SelfNoteRoomDatabase.databaseWriteExecutor.execute(() -> mNoteDao.delete(note));
    }

    public void insert(Category category) {
        SelfNoteRoomDatabase.databaseWriteExecutor.execute(() -> mCategoryDao.insert(category));
    }

    public void update(Category category) {
        SelfNoteRoomDatabase.databaseWriteExecutor.execute(() -> mCategoryDao.update(category));
    }

    public void insert(SortKey sortKey) {
        SelfNoteRoomDatabase.databaseWriteExecutor.execute(() -> mSortKeyDao.insert(sortKey));
    }

    public void update(SortKey sortKey) {
        SelfNoteRoomDatabase.databaseWriteExecutor.execute(() -> mSortKeyDao.update(sortKey));
    }

    public void insert(UserProfile userProfile) {
        SelfNoteRoomDatabase.databaseWriteExecutor.execute(() -> mUserDao.insert(userProfile));
    }

    public void update(UserProfile userProfile) {
        SelfNoteRoomDatabase.databaseWriteExecutor.execute(() -> mUserDao.update(userProfile));
    }
}
*/
