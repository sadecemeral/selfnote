package de.mh.selfnote.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import de.mh.selfnote.objects.UserProfile;
import de.mh.selfnote.objects.UserRelationship;

@Dao
public interface UserDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(UserProfile userProfile);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void update(UserProfile userProfile);

    @Query("DELETE FROM user_profile_table")
    void deleteAll();

    @Query("SELECT * FROM user_profile_table WHERE USER_NICKNAME = :userNickname AND USER_PASSWORD = :userPassword")
    UserProfile getUserProfile(String userNickname, String userPassword);

    @Query("SELECT * FROM user_relationship_table WHERE USER_ID_1 = :userID")
    List<UserRelationship> getUserRelationship(int userID);
}
