/*
package de.mh.selfnote.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.mh.selfnote.R;
import de.mh.selfnote.objects.Category;
import de.mh.selfnote.objects.Note;
import de.mh.selfnote.objects.SortKey;
import de.mh.selfnote.objects.UserProfile;
import de.mh.selfnote.objects.UserRelationship;
import de.mh.selfnote.toolbox.Utilities;

@Database(entities = {Note.class, Category.class, SortKey.class, UserProfile.class, UserRelationship.class}, version = 1, exportSchema = false)
public abstract class SelfNoteRoomDatabase extends RoomDatabase {

    public abstract NoteDAO noteDao();
    public abstract CategoryDAO categoryDao();
    public abstract SortKeyDAO sortKeyDao();
    public abstract UserDAO userDao();

    private static volatile SelfNoteRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    private static Context Parent;

    static SelfNoteRoomDatabase getDatabase(final Context context) {
        Parent = context;

        if (INSTANCE == null) {
            synchronized (SelfNoteRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            SelfNoteRoomDatabase.class, "selfnote_database")
                            .allowMainThreadQueries()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }

        return INSTANCE;
    }

    final private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            // If you want to keep data through app restarts,
            // comment out the following block
            databaseWriteExecutor.execute(() -> {
                // Populate the database in the background.
                // If you want to start with more words, just add them.
                //NoteDAO dao = INSTANCE.noteDao();
                //dao.deleteAll();

                if (Parent == null)
                    return;

                //Init Lists
                CategoryDAO daoCat = INSTANCE.categoryDao();
                daoCat.deleteAll();
                String[] strInitCatArray = Parent.getResources().getStringArray(R.array.noteCategoryInit_array);
                if (strInitCatArray != null && strInitCatArray.length > 0) {

                    for (String s : strInitCatArray) {

                        int iColor = Utilities.getColorByName(Parent, s);
                        Category dataset = new Category(s, iColor, true);
                        daoCat.insert(dataset);
                    }
                }

                SortKeyDAO daoSortKey = INSTANCE.sortKeyDao();
                daoSortKey.deleteAll();
                String[] strSortKeyName = Parent.getResources().getStringArray(R.array.noteSortKeyNameInit_array);
                String[] strSortKeyCol = Parent.getResources().getStringArray(R.array.noteSortKeyColInit_array);

                if (strSortKeyName == null || strSortKeyCol == null || strSortKeyName.length != strSortKeyCol.length)
                    return;

                for (int i = 0; i < strSortKeyName.length; i++) {
                    SortKey dataset = new SortKey(strSortKeyName[i], strSortKeyCol[i], i+1, false);
                    daoSortKey.insert(dataset);
                }

            });
        }
    };
}*/
