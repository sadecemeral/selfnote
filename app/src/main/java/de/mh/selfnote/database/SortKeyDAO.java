package de.mh.selfnote.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import de.mh.selfnote.objects.SortKey;

@Dao
public interface SortKeyDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(SortKey dataset);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void update(SortKey dataset);

    @Query("DELETE FROM sort_key_table")
    void deleteAll();

    @Query("SELECT * FROM sort_key_table ORDER BY SORTKEY_ORDER ASC")
    List<SortKey> getListSortKey();

    @Query("SELECT DISTINCT SORTKEY_NOTE_COLUMN FROM sort_key_table WHERE SORTKEY_ENABLED = 1 ORDER BY SORTKEY_ORDER ASC")
    String[] getListSortKeyEnabled();
}
