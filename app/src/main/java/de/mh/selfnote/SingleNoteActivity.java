package de.mh.selfnote;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.transition.Transition;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.view.ViewCompat;

import com.bumptech.glide.Glide;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipDrawable;
import com.google.android.material.chip.ChipGroup;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.ArrayList;

import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.objects.Note;
import de.mh.selfnote.toolbox.CircularImageLoadingDrawable;
import de.mh.selfnote.toolbox.Utilities;
import de.mh.selfnote.viewModel.CategoryViewModel;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class SingleNoteActivity extends BasicActivity {
    public static final String NOTE_CATEGORY_COLOR = "NOTE_CATEGORY_COLOR";
    public static final String NOTE_PERSONAL = "NOTE_PERSONAL";
    public static final String NOTE_TIMESTAMP = "NOTE_TIMESTAMP";
    public static final String NOTE_TITLE = "NOTE_TITLE";
    public static final String NOTE_TEXT = "NOTE_TEXT";
    public static final String NOTE_KEYWORD = "NOTE_KEYWORD";
    public static final String NOTE_RATING = "NOTE_RATING";
    public static final String NOTE_IMAGE_LIST = "NOTE_IMAGE_LIST";
    public static final String NOTE_IMAGE = "NOTE_IMAGE";

    // Extra name for the ID parameter
    public static final String EXTRA_PARAM_ID = "detail:_id";

    // View name of the header image. Used for activity scene transitions
    public static final String VIEW_NAME_HEADER_IMAGE = "detail:header:image";

    // View name of the header title. Used for activity scene transitions
    public static final String VIEW_NAME_HEADER_TITLE = "detail:header:title";

    private TextView mPersonalView;
    private ImageSwitcher mHeaderImageView;
    private TextView mHeaderTitle;
    private ExpandableTextView mText;
    private TextView mTimestamp;
    private ChipGroup mKeywords;
    private RatingBar mRatingBar;
    private TextView mNoteImageCounterTextView;

    private ArrayList<String> noteImageListURL;

    private int currentIndex = 0;
    private int downX = 0;
    private int upX = 0;
    private int defaultNoteImageDrawableIdent;

    private Note mNote;
    private Intent mIntent;

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            case R.id.MenuItemSingleNoteEdit:
                Intent replyIntent;

                if (CategoryViewModel.categoryName_Diary.equals(mNote.getNoteCategoryName()))
                    replyIntent = new Intent(SingleNoteActivity.this, DiaryActivity.class);
                else
                    replyIntent = new Intent(SingleNoteActivity.this, AddNewNoteActivity.class);

                replyIntent.putExtra("NOTE_KEY", mNote.getNoteKey());

                activityLauncher.launch(replyIntent, result -> {
                    if (result.getResultCode() != Activity.RESULT_OK) {
                        Toast.makeText(SingleNoteActivity.this, R.string.toast_save_canceled, Toast.LENGTH_SHORT).show();
                    } else
                        this.onBackPressed();
                });
                return true;
            case R.id.MenuItemSingleNoteUnpin:
                UserProfileViewModel.getInstance().setNotePinned(mNote);
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //ActionBar
        this.getMenuInflater().inflate(R.menu.menu_single_note_activity, menu);

        if (!UserProfileViewModel.isLoggedUser(mNote.getCreatorUID()))
            menu.findItem(R.id.MenuItemSingleNoteEdit).setEnabled(false);
        else
            menu.findItem(R.id.MenuItemSingleNoteEdit).setEnabled(true);

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_note);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mProgressBar = findViewById(R.id.progressBar_singlenote);

        mPersonalView = findViewById(R.id.single_note_personal);
        mHeaderImageView = findViewById(R.id.single_note_image_switcher);
        mHeaderTitle = findViewById(R.id.single_note_title);
        mText = findViewById(R.id.single_note_text);
        mTimestamp = findViewById(R.id.single_note_timestamp);
        mRatingBar = findViewById(R.id.single_note_diary_ratingbar);
        mKeywords = findViewById(R.id.single_note_keyword_tag_group);
        mNoteImageCounterTextView = findViewById(R.id.single_note_image_counter);

        mHeaderImageView.setFactory(() -> {
            ImageView myView = new ImageView(getApplicationContext());
            myView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            myView.setLayoutParams(new ImageSwitcher.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.MATCH_PARENT));
            return myView;
        });

        mHeaderImageView.setInAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
        mHeaderImageView.setOutAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out));
        mHeaderImageView.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                downX = (int) event.getX();
                return true;
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                upX = (int) event.getX();

                if (noteImageListURL == null || noteImageListURL.isEmpty()) {
                    mNoteImageCounterTextView.setVisibility(View.GONE);
                    return true;
                }

                if (upX - downX > 100) {
                    //curIndex  current image index in array viewed by user
                    currentIndex--;
                    // condition
                    if (currentIndex < 0)
                        currentIndex = noteImageListURL.size() - 1;

                    mHeaderImageView.setInAnimation(AnimationUtils.loadAnimation(SingleNoteActivity.this, android.R.anim.slide_in_left));
                    mHeaderImageView.setOutAnimation(AnimationUtils.loadAnimation(SingleNoteActivity.this, android.R.anim.slide_out_right));

                } else if (downX - upX > -100) {
                    //curIndex  current image index in array viewed by user
                    currentIndex++;
                    // condition
                    if (currentIndex >= noteImageListURL.size())
                        currentIndex = 0;

                    mHeaderImageView.setInAnimation(AnimationUtils.loadAnimation(SingleNoteActivity.this, R.anim.slide_in_right));
                    mHeaderImageView.setOutAnimation(AnimationUtils.loadAnimation(SingleNoteActivity.this, R.anim.slide_out_left));
                }

                Glide.with(this)
                        .load(noteImageListURL.get(currentIndex))
                        .placeholder(CircularImageLoadingDrawable.getWait(SingleNoteActivity.this))
                        .into((ImageView) mHeaderImageView.getNextView());

                mHeaderImageView.showNext();

                mNoteImageCounterTextView.setVisibility(View.VISIBLE);
                mNoteImageCounterTextView.setText((currentIndex + 1) + "/" + noteImageListURL.size());

                return true;
            }
            return false;
        });

        mIntent = this.getIntent();

        // BEGIN_INCLUDE(detail_set_view_name)
        /*
         * Set the name of the view's which will be transition to, using the static values above.
         * This could be done in the layout XML, but exposing it via static variables allows easy
         * querying from other Activities
         */
        ViewCompat.setTransitionName(mHeaderImageView, VIEW_NAME_HEADER_IMAGE);
        ViewCompat.setTransitionName(mHeaderTitle, VIEW_NAME_HEADER_TITLE);
        // END_INCLUDE(detail_set_view_name)

        loadItem();
    }

    private void loadItem() {

        mNote = UserProfileViewModel.getNoteViewModel().findPinnedNoteByKey(mIntent.getStringExtra(EXTRA_PARAM_ID));

        if (mNote == null)
            return;

        mHeaderTitle.setBackgroundColor(mNote.getNoteCategory().getCategoryColor());
        // Set the title TextView to the item's name and author
        mHeaderTitle.setText(mNote.getNoteTitle());
        mText.setText(mNote.getNoteText());
        mTimestamp.setText(Utilities.formatDate("dd.MM.yyyy HH:mm", mNote.getNoteTimestamp()));

        if (mNote.isNotePersonal())
            mPersonalView.setVisibility(View.VISIBLE);
        else
            mPersonalView.setVisibility(View.INVISIBLE);

        mKeywords.removeAllViews();
        if (mNote.getNoteKeyword() != null && mNote.getNoteKeyword().length() > 0) {
            String strValue = mNote.getNoteKeyword();
            String[] strTag = strValue.split("[, ]");

            if (strTag.length < 1)
                strTag = new String[]{strValue};

            int iCounter = -1;
            while (++iCounter < strTag.length) {
                final Chip chip = new Chip(this);
                chip.setChipDrawable(ChipDrawable.createFromResource(this, R.xml.chip));
                chip.setText(strTag[iCounter]);
                chip.setTextAppearance(R.style.ChipTextAppearance);
                chip.setClickable(false);
                mKeywords.addView(chip);
            }
        }

        mRatingBar.setRating(mNote.getNoteRating());
        if (mNote.getNoteRating() != 0) {
            mRatingBar.setVisibility(View.VISIBLE);
        } else
            mRatingBar.setVisibility(View.GONE);

        noteImageListURL = mNote.getNoteImageListURL();

        mHeaderImageView.setBackgroundColor(mNote.getNoteCategory().getCategoryColor());

        //default Kategoriebild/Farbe
        defaultNoteImageDrawableIdent = Utilities.getDrawableIdentByName(this, mNote.getNoteCategoryName());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && addTransitionListener()) {
            // If we're running on Lollipop and we have added a listener to the shared element
            // transition, load the thumbnail. The listener will load the full-size image when
            // the transition is complete.
            loadThumbnail();
        } else {
            // If all other cases we should just load the full-size image now
            loadFullSizeImage();
        }
    }

    private void loadThumbnail() {
        mHeaderImageView.setImageResource(defaultNoteImageDrawableIdent);
    }

    private void loadFullSizeImage() {
        if (noteImageListURL == null || noteImageListURL.isEmpty()) {
            mNoteImageCounterTextView.setVisibility(View.GONE);
            return;
        }

        Glide.with(this)
                .load(noteImageListURL.get(currentIndex))
                .placeholder(CircularImageLoadingDrawable.getWait(SingleNoteActivity.this))
                .error(defaultNoteImageDrawableIdent)
                .into((ImageView) mHeaderImageView.getCurrentView());

        mNoteImageCounterTextView.setVisibility(View.VISIBLE);
        mNoteImageCounterTextView.setText((currentIndex + 1) + "/" + noteImageListURL.size());
    }

    /**
     * Try and add a {@link Transition.TransitionListener} to the entering shared element
     * {@link Transition}. We do this so that we can load the full-size image after the transition
     * has completed.
     *
     * @return true if we were successful in adding a listener to the enter transition
     */
    @RequiresApi(21)
    private boolean addTransitionListener() {
        final Transition transition = getWindow().getSharedElementEnterTransition();

        if (transition != null) {
            // There is an entering shared element transition so add a listener to it
            transition.addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionEnd(Transition transition) {
                    // As the transition has ended, we can now load the full-size image
                    loadFullSizeImage();

                    // Make sure we remove ourselves as a listener
                    transition.removeListener(this);
                }

                @Override
                public void onTransitionStart(Transition transition) {
                    // No-op
                }

                @Override
                public void onTransitionCancel(Transition transition) {
                    // Make sure we remove ourselves as a listener
                    transition.removeListener(this);
                }

                @Override
                public void onTransitionPause(Transition transition) {
                    // No-op
                }

                @Override
                public void onTransitionResume(Transition transition) {
                    // No-op
                }
            });
            return true;
        }

        // If we reach here then we have not added a listener
        return false;
    }
}