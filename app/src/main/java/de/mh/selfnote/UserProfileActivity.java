package de.mh.selfnote;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.imageview.ShapeableImageView;

import java.util.Objects;

import de.mh.selfnote.adapter.UserRelationshipListAdapter;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.extended.NoticeDialogFragment;
import de.mh.selfnote.extended.WrapContentLinearLayoutManager;
import de.mh.selfnote.objects.UserProfile;
import de.mh.selfnote.toolbox.AppManager;
import de.mh.selfnote.toolbox.Utilities;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class UserProfileActivity extends BasicActivity {

    private RecyclerView _userRelationshipList;
    private UserProfile userProfile;
    private UserRelationshipListAdapter _adapter;

    TextView viewUserID;
    TextView viewUserName;
    ShapeableImageView viewImage;
    TextView viewUserEMail;
    TextView viewUserBirthday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.activity_title_userprofile);

        mProgressBar = findViewById(R.id.progressBar_userprofile);

        viewUserID = findViewById(R.id.view_userprofile_userid);
        viewUserName = findViewById(R.id.view_userprofile_username);
        viewImage = findViewById(R.id.view_userprofile_userimage);
        viewUserEMail = findViewById(R.id.view_userprofile_useremail);
        viewUserBirthday = findViewById(R.id.view_userprofile_userbirthday);

        _userRelationshipList = findViewById(R.id.user_relationship_list);
        _userRelationshipList.setLayoutManager(new WrapContentLinearLayoutManager(this));

        _adapter = new UserRelationshipListAdapter(this,
                UserProfileViewModel.getInstance().getRelationshipOptions(this));

        _userRelationshipList.setAdapter(_adapter);

        fillInputFields();
    }

    private void fillInputFields() {
        userProfile = UserProfileViewModel.getUserProfile(null);

        if (userProfile == null)
            return;

        viewUserID.setText(userProfile.getUserID());
        viewUserName.setText("Name: " + userProfile.getUserName());
        viewUserEMail.setText("Email: " + userProfile.getUserEmail());
        viewUserBirthday.setText("Geburtstag: " + Utilities.formatDate("dd.MM.yyyy", userProfile.getUserBirthday()));

        Utilities.loadUserprofilePhotoIntoView(this, userProfile, viewImage);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //ActionBar
        this.getMenuInflater().inflate(R.menu.menu_userprofile_activity, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.MenuItemEditUserProfile:
                Intent intent = new Intent(UserProfileActivity.this, EditUserProfileActivity.class);
                activityLauncher.launch(intent, result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        this.fillInputFields();
                    }
                });

                return true;
            case R.id.MenuItemCancelUserProfile:
                AppManager.getAppManager().finishActivity(this, RESULT_CANCELED);

                return true;
            case R.id.SubMenuItemUserAccountDelete:
                showNoticeDialog(
                        getString(R.string.dialog_title_attention),
                        getString(R.string.dialog_text_question_delete_user_account),
                        new int[]{R.string.btn_delete_user_account, R.string.btn_Cancel});

                return true;
            case R.id.SubMenuItemUserPasswordChange:
                startActivity(new Intent(UserProfileActivity.this, UpdateUserPasswordActivity.class));

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (_adapter != null)
            _adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (_adapter != null) {
            _adapter.stopListening();
        }
    }

    @Override
    public void onDialogPositiveClick(NoticeDialogFragment dialog, int iTextId) {
        super.onDialogPositiveClick(dialog, iTextId);

        switch (iTextId) {
            case R.string.btn_cancel_relationship:

                if (dialog.userRelationship != null)
                    UserProfileViewModel.getInstance().cancelRelationship(this, dialog.userRelationship);

                break;
            case R.string.btn_delete_user_account:

                UserProfileViewModel.getInstance().deleteUserAccount(this);

                break;
            default:
                break;
        }

    }
}