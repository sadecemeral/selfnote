package de.mh.selfnote;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.target.Target;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.material.textfield.TextInputEditText;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.extended.NoticeDialogFragment;
import de.mh.selfnote.objects.Category;
import de.mh.selfnote.objects.Note;
import de.mh.selfnote.objects.NoteImage;
import de.mh.selfnote.objects.StatusKnz;
import de.mh.selfnote.toolbox.AppManager;
import de.mh.selfnote.toolbox.ImagePicker;
import de.mh.selfnote.toolbox.Utilities;
import de.mh.selfnote.viewModel.CategoryViewModel;
import de.mh.selfnote.viewModel.NoteViewModel;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class DiaryActivity extends BasicActivity {

    private class DownloadFilesTask extends AsyncTask<String, Integer, NoteImage> {
        final WeakReference<Context> parentCtx;

        WeakReference<ImageView> imageView;
        WeakReference<FutureTarget<Bitmap>> futureTarget;

        private DownloadFilesTask(Context parent) {
            parentCtx = new WeakReference<>(parent);
        }

        private DownloadFilesTask(Context parent, ImageView imgView) {
            parentCtx = new WeakReference<>(parent);
            imageView = new WeakReference<>(imgView);
        }

        protected NoteImage doInBackground(String... url) {

            NoteImage oObject = new NoteImage();
            String strURL = url[0];

            try {

                futureTarget = new WeakReference<>(
                        Glide.with(parentCtx.get())
                                .asBitmap()
                                .load(strURL)
                                .submit(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL));

                Bitmap bm = futureTarget.get().get();

                oObject.setNoteImageURL(strURL);
                oObject.setNoteImageBitmap(Bitmap.createBitmap(bm));

            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return oObject;
        }

        protected void onPostExecute(NoteImage oObject) {
            if (isCancelled() || oObject == null) {
                return;
            }

            diaryImageList.add(oObject);

            if (imageView != null) {
                imageView.get().setImageBitmap(oObject.getNoteImageBitmap());
            }

            // Do something with the Bitmap and then when you're done with it:
            Glide.with(parentCtx.get()).clear(futureTarget.get());
        }
    }

    private String strItemKey = null;
    private ImageSwitcher imageView;
    private TextInputEditText inputEditTextDiary;
    private RatingBar ratingBar;
    private TextView imageCounterTextView;
    private Note mNote;
    private Category diaryCategory;
    private NoteViewModel _viewNoteModel;
    private ArrayList<NoteImage> diaryImageList = new ArrayList<>();
    private ArrayList<NoteImage> deleteDiaryImageList = new ArrayList<>();
    private int currentIndex = 0;
    private int downX = 0;
    private int upX = 0;
    private int placeholderIdImage = R.drawable.ic_insert_photo;
    private int backgroundColorImage = Color.BLACK;

    private void fillInputFields(Intent intent) {
        if (intent == null || !intent.hasExtra("NOTE_KEY"))
            return;

        strItemKey = intent.getStringExtra("NOTE_KEY");
        mNote = _viewNoteModel.findNoteByKey(strItemKey);

        if (mNote == null)
            return;

        inputEditTextDiary.setText(mNote.getNoteText());

        //Bild
        if (mNote.getNoteImageListURL() != null && !mNote.getNoteImageListURL().isEmpty()) {

            currentIndex = 0;
            int iSum = mNote.getNoteImageListURL().size();
            for (int iCounter = 0; iCounter < iSum; iCounter++) {
                String noteImageURL = mNote.getNoteImageListURL().get(iCounter);
                if (iCounter == 0)
                    new DownloadFilesTask(DiaryActivity.this, (ImageView) imageView.getCurrentView())
                            .execute(noteImageURL);
                else
                    new DownloadFilesTask(DiaryActivity.this).execute(noteImageURL);
            }

            imageCounterTextView.setVisibility(View.VISIBLE);
            imageCounterTextView.setText("1/" + iSum);
        }

        ratingBar.setRating(mNote.getNoteRating());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diary);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.activity_title_diary);

        mScrollView = findViewById(R.id.scrollView_diary);
        mProgressBar = findViewById(R.id.progressBar_diary);

        //Kategorie fix
        diaryCategory = UserProfileViewModel.getCategoryViewModel().getCategoryByName(CategoryViewModel.categoryName_Diary);

        if (diaryCategory != null) {
            //Hintergrund
            backgroundColorImage = diaryCategory.getCategoryColor();
            placeholderIdImage = Utilities.getDrawableIdentByName(this, diaryCategory.getCategoryName());
        }

        //NoteViewModel
        _viewNoteModel = UserProfileViewModel.getNoteViewModel();

        //ImageCounter
        imageCounterTextView = findViewById(R.id.diary_image_counter);

        //FAB Menu
        FloatingActionsMenu fab_menu = findViewById(R.id.fab_diary_images_menu);

        //ImageSwitcher
        imageView = findViewById(R.id.diary_image_upload);
        imageView.setFactory(() -> {
            ImageView myView = new ImageView(getApplicationContext());
            myView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            myView.setLayoutParams(
                    new ImageSwitcher.LayoutParams(
                            ActionBar.LayoutParams.MATCH_PARENT,
                            ActionBar.LayoutParams.MATCH_PARENT));
            return myView;
        });

        //Bilder
        imageView.setInAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
        imageView.setOutAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out));
        imageView.setBackgroundColor(backgroundColorImage);
        imageView.setImageResource(placeholderIdImage);
        imageView.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                downX = (int) event.getX();
                return true;
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                upX = (int) event.getX();

                if (diaryImageList.size() == 0)
                    return true;

                if (upX - downX > 100) {
                    //curIndex  current image index in array viewed by user
                    currentIndex--;
                    // condition
                    if (currentIndex < 0)
                        currentIndex = diaryImageList.size() - 1;

                    imageView.setInAnimation(AnimationUtils.loadAnimation(DiaryActivity.this, android.R.anim.slide_in_left));
                    imageView.setOutAnimation(AnimationUtils.loadAnimation(DiaryActivity.this, android.R.anim.slide_out_right));

                } else if (downX - upX > -100) {
                    //curIndex  current image index in array viewed by user
                    currentIndex++;
                    // condition
                    if (currentIndex >= diaryImageList.size())
                        currentIndex = 0;

                    imageView.setInAnimation(AnimationUtils.loadAnimation(DiaryActivity.this, R.anim.slide_in_right));
                    imageView.setOutAnimation(AnimationUtils.loadAnimation(DiaryActivity.this, R.anim.slide_out_left));
                }

                NoteImage oImage = diaryImageList.get(currentIndex);

                if (oImage != null && oImage.getNoteImageUri() != null) {

                    imageView.setImageURI(oImage.getNoteImageUri());

                } else if (oImage != null && oImage.getNoteImageBitmap() != null) {

                    ((ImageView) imageView.getNextView()).setImageBitmap(oImage.getNoteImageBitmap());
                    imageView.showNext();
                } else
                    return false;

                imageCounterTextView.setVisibility(View.VISIBLE);
                imageCounterTextView.setText((currentIndex + 1) + "/" + diaryImageList.size());

                return true;
            }
            return false;
        });

        //check permission: Kamera/Speicher
        checkPermissions(ImagePicker.permissionStringList);

        //Upload für Image
        findViewById(R.id.fab_diary_image_add).setOnClickListener(v -> {

            ImagePicker imgPicker = ImagePicker.getInstance();
            Intent chooseImageIntent = imgPicker.getPickImageIntent(this, getString(R.string.intent_pickimg_addnewdiary), true);
            if (chooseImageIntent == null)
                return;

            activityLauncher.launch(chooseImageIntent, result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {

                    fab_menu.collapseImmediately();

                    ClipData clipData = null;
                    if (result.getData() != null)
                        clipData = result.getData().getClipData();

                    if (clipData != null) {
                        Uri imageUri = null;
                        for (int i = 0; i < clipData.getItemCount(); i++) {
                            imageUri = clipData.getItemAt(i).getUri();
                            // your code for multiple image selection
                            NoteImage img = new NoteImage();
                            img.setNoteImageUri(imageUri);
                            img.setNoteImageStatusKnz(StatusKnz.INSERT);
                            diaryImageList.add(img);
                        }

                        if (imageUri != null)
                            imageView.setImageURI(imageUri);

                    } else {
                        Uri imageUri = result.getData().getData();
                        // your codefor single image selection
                        NoteImage img = new NoteImage();
                        img.setNoteImageUri(imageUri);
                        img.setNoteImageStatusKnz(StatusKnz.INSERT);
                        diaryImageList.add(img);

                        imageView.setImageURI(imageUri);
                    }

                    currentIndex = diaryImageList.size() - 1;

                    imageCounterTextView.setVisibility(View.VISIBLE);
                    imageCounterTextView.setText(diaryImageList.size() + "/" + diaryImageList.size());
                }
            });
        });

        findViewById(R.id.fab_diary_image_delete).setOnClickListener(v -> {
            if (diaryImageList.size() > 0 && diaryImageList.get(currentIndex) != null) {
                NoteImage oImage = diaryImageList.get(currentIndex);

                if (!TextUtils.isEmpty(oImage.getNoteImageURL())) {
                    oImage.setNoteImageStatusKnz(StatusKnz.DELETE);
                    deleteDiaryImageList.add(oImage);
                }

                diaryImageList.remove(currentIndex);
            }

            if (diaryImageList.size() == 0) {
                imageView.setImageResource(placeholderIdImage);
                imageCounterTextView.setVisibility(View.GONE);
                currentIndex = -1;
                return;
            }

            --currentIndex;

            if (currentIndex < 0)
                currentIndex = diaryImageList.size() - 1;

            NoteImage oImage = diaryImageList.get(currentIndex);

            if (oImage.getNoteImageUri() != null) {

                imageView.setImageURI(oImage.getNoteImageUri());

            } else if (oImage.getNoteImageBitmap() != null) {

                ((ImageView) imageView.getNextView()).setImageBitmap(oImage.getNoteImageBitmap());
                imageView.showNext();
            } else
                return;

            imageCounterTextView.setVisibility(View.VISIBLE);
            imageCounterTextView.setText((currentIndex + 1) + "/" + diaryImageList.size());
        });

        inputEditTextDiary = findViewById(R.id.diary_text);
        ratingBar = findViewById(R.id.diary_ratingbar);

        this.fillInputFields(this.getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //ActionBar
        this.getMenuInflater().inflate(R.menu.menu_diary_activity, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {

            case R.id.MenuItemInfoDiary:
                this.showNoticeDialog(this.getString(R.string.dialog_title_information),
                        this.getString(R.string.dialog_diary_information));
                return true;
            case R.id.MenuItemDeleteDiary:
                if (strItemKey != null)
                    this.showNoticeDialog(
                            this.getString(R.string.dialog_title_warning),
                            this.getString(R.string.dialog_text_question_delete),
                            new int[]{R.string.btn_Delete, R.string.btn_Cancel});
                return true;
            case R.id.MenuItemSaveCancelDiary:
                AppManager.getAppManager().finishActivity(this, RESULT_CANCELED);

                return true;
            case R.id.MenuItemSaveDiary:
                saveDiary();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveDiary() {
        if (TextUtils.isEmpty(inputEditTextDiary.getText())) {
            Toast.makeText(getApplicationContext(), R.string.toast_empty_diary_not_saved, Toast.LENGTH_LONG).show();
        } else {
            String strTitle = inputEditTextDiary.getText().toString();
            if (strTitle != null && strTitle.length() >= 20)
                strTitle = strTitle.substring(0, 20);

            if (mNote == null) {
                mNote = new Note();
                mNote.setNoteKey(_viewNoteModel.getNewNoteKey());
                mNote.setNoteTimestamp(System.currentTimeMillis());
                mNote.setCreatorUID(UserProfileViewModel.getLoggedUserID());
            }

            mNote.setNoteCategory(diaryCategory);
            mNote.setNoteTitle(strTitle);
            mNote.setNoteText(inputEditTextDiary.getText().toString());
            mNote.setNotePersonal();
            mNote.setNoteRating(ratingBar.getRating());

            UserProfileViewModel.getInstance().addReaderUIDToNote(mNote);

            diaryImageList.addAll(deleteDiaryImageList);

            _viewNoteModel.save(this, diaryImageList, mNote);
        }
    }

    @Override
    public void onDialogPositiveClick(NoticeDialogFragment dialog, int iTextId) {
        super.onDialogPositiveClick(dialog, iTextId);

        switch (iTextId) {
            case R.string.btn_Delete:
                UserProfileViewModel.getNoteViewModel().delete(this, mNote);
                break;
            default:
                break;
        }
    }
}
