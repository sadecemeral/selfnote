package de.mh.selfnote;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textfield.TextInputEditText;

import org.w3c.dom.Text;

import java.util.Objects;

import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.objects.UserProfile;
import de.mh.selfnote.toolbox.AppManager;
import de.mh.selfnote.toolbox.Utilities;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class UpdateUserPasswordActivity extends BasicActivity {

    private TextInputEditText editUserEmail;
    private TextInputEditText editUserPasswordOld;
    private TextInputEditText editUserPasswordNew;
    private TextInputEditText editUserPasswordNewConfirm;
    private ShapeableImageView imgUserPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_password_edit);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.activity_title_update_userpassword);

        mProgressBar = findViewById(R.id.progressBar_userpassword_edit);
        editUserEmail = findViewById(R.id.edit_userpassword_email);
        editUserEmail.setEnabled(false);
        editUserPasswordOld = findViewById(R.id.edit_userpassword_password_old);
        editUserPasswordNew = findViewById(R.id.edit_userpassword_password_new);
        editUserPasswordNewConfirm = findViewById(R.id.edit_userpassword_password_new_confirm);
        imgUserPassword = findViewById(R.id.edit_userpassword_userimage);

        fillInputFields();
    }

    private void fillInputFields() {
        UserProfile userProfile = UserProfileViewModel.getUserProfile(null);
        if (userProfile == null)
            return;

        editUserEmail.setText(userProfile.getUserEmail());

        Utilities.loadUserprofilePhotoIntoView(this, userProfile, imgUserPassword);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //ActionBar
        this.getMenuInflater().inflate(R.menu.menu_edit_userpassword_activity, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.MenuItemSaveCancelUserPassword:
                AppManager.getAppManager().finishActivity(this, RESULT_CANCELED);

                return true;
            case R.id.MenuItemSaveUserPassword:
                saveData();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean checkInputFields() {

        boolean isCheckOK = true;

        if (TextUtils.isEmpty(editUserPasswordOld.getText()) || editUserPasswordOld.getText().length() < 6) {
            editUserPasswordOld.setError("Bitte mindestens 6 Zeichen eingeben!");
            isCheckOK = false;
        }

        if (TextUtils.isEmpty(editUserPasswordNew.getText()) || editUserPasswordNew.getText().length() < 6) {
            editUserPasswordNew.setError("Bitte mindestens 6 Zeichen eingeben!");
            isCheckOK = false;
        }

        if (!Objects.requireNonNull(editUserPasswordNew.getText()).toString().equals(Objects.requireNonNull(editUserPasswordNewConfirm.getText()).toString())) {
            editUserPasswordNewConfirm.setError("Passwort stimmt nicht überein!");
            isCheckOK = false;
        }

        return isCheckOK;
    }

    private void saveData() {
        if (!this.checkInputFields())
            return;

        UserProfileViewModel.getInstance().updateUserPassword(this, editUserEmail.getText().toString(),
                editUserPasswordOld.getText().toString(), editUserPasswordNew.getText().toString());
    }
}
