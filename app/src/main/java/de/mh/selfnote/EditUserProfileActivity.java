package de.mh.selfnote;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.IntentSenderRequest;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.identity.BeginSignInRequest;
import com.google.android.gms.auth.api.identity.Identity;
import com.google.android.gms.auth.api.identity.SignInClient;
import com.google.android.gms.auth.api.identity.SignInCredential;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.objects.UserProfile;
import de.mh.selfnote.objects.UserProfileImage;
import de.mh.selfnote.toolbox.AppManager;
import de.mh.selfnote.toolbox.DatePicker;
import de.mh.selfnote.toolbox.ImagePicker;
import de.mh.selfnote.toolbox.Utilities;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class EditUserProfileActivity extends BasicActivity {

    private TextInputEditText editUserName;
    private TextInputEditText editUserNickname;
    private TextInputEditText editUserEmail;
    private TextInputEditText editUserBirthday;
    private TextInputEditText editUserPassword;
    private TextInputEditText editUserPasswordConfirm;
    private ShapeableImageView imgUserProfile;

    private UserProfile userProfile;
    private UserProfileImage profileImage = new UserProfileImage();

    private SignInClient oneTapClient;
    private BeginSignInRequest signUpRequest;
    private final ActivityResultLauncher<IntentSenderRequest> intentSender = registerForActivityResult(new ActivityResultContracts.StartIntentSenderForResult(), activityResult -> {
        Intent data = activityResult.getData();
        try {
            SignInCredential credential = oneTapClient.getSignInCredentialFromIntent(data);
            String idToken = credential.getGoogleIdToken();
            String email = credential.getId();
            String username = credential.getId();
            String password = credential.getPassword();
            String name = credential.getDisplayName();
            Uri userPhoto = credential.getProfilePictureUri();

            if (idToken != null) {
                // Got an ID token from Google. Use it to authenticate
                // with Firebase.

                Utilities.setSignInToken(EditUserProfileActivity.this, idToken);

                editUserEmail.setText(email);
                editUserEmail.setEnabled(false);
                editUserNickname.setText(username.substring(0, username.indexOf("@")));
                editUserName.setText(name);
                editUserPassword.setEnabled(false);
                editUserPasswordConfirm.setEnabled(false);

                if (userPhoto != null) {
                    profileImage.setProfileImageURL(userPhoto.toString());
                    Glide.with(this).load(userPhoto).into(imgUserProfile);
                }
            } else if (password != null) {
                // Got a saved username and password. Use them to authenticate
                // with your backend.
                System.out.println("Got password.");
            }
        } catch (ApiException e) {
            switch (e.getStatusCode()) {
                case CommonStatusCodes.CANCELED:
                    System.out.println("One-tap dialog was closed.");
                    break;
                case CommonStatusCodes.NETWORK_ERROR:
                    System.out.println("One-tap encountered a network error.");
                    break;
                default:
                    Toast.makeText(EditUserProfileActivity.this, "Couldn't get credential from result. "
                            + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    });

    private void beginSignUp() {
        oneTapClient.beginSignIn(signUpRequest).addOnSuccessListener(this, result -> {
            IntentSenderRequest senderRequest = new IntentSenderRequest.Builder(result.getPendingIntent().getIntentSender()).build();
            intentSender.launch(senderRequest);
        }).addOnFailureListener(this, e -> {
            // No Google Accounts found. Just continue presenting the signed-out UI.
            Toast.makeText(EditUserProfileActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    public void onCompleteSave() {
        AppManager.getAppManager().finishActivity(this, RESULT_OK);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile_edit);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.activity_title_edit_userprofile);

        //Google SignUp
        oneTapClient = Identity.getSignInClient(EditUserProfileActivity.this);
        signUpRequest = BeginSignInRequest.builder().setGoogleIdTokenRequestOptions(BeginSignInRequest.GoogleIdTokenRequestOptions.builder().setSupported(true)
                // Your server's client ID, not your Android client ID.
                .setServerClientId(getString(R.string.default_web_client_id))
                // Show all accounts on the device.
                .setFilterByAuthorizedAccounts(false).build()).build();
        //Google SignUp

        mProgressBar = findViewById(R.id.progressBar_userprofile_edit);

        editUserName = findViewById(R.id.edit_userprofile_name);
        editUserNickname = findViewById(R.id.edit_userprofile_nickname);
        editUserEmail = findViewById(R.id.edit_userprofile_email);
        editUserPassword = findViewById(R.id.edit_userprofile_password);
        editUserPasswordConfirm = findViewById(R.id.edit_userprofile_password_confirm); //Passwort bestätigen

        editUserPasswordConfirm.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                saveData();
                handled = true;
            }
            return handled;
        });

        //check permission
        checkPermissions(ImagePicker.permissionStringList);

        imgUserProfile = findViewById(R.id.edit_userprofile_image);
        imgUserProfile.setOnClickListener(view -> {

            ImagePicker imgPicker = ImagePicker.getInstance();
            Intent chooseImageIntent = imgPicker.getPickImageIntent(this, getString(R.string.intent_pickimg_userprofile), false);
            if (chooseImageIntent == null) return;

            activityLauncher.launch(chooseImageIntent, result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    // your code for single image selection
                    ClipData clipData = null;
                    if (result.getData() != null) clipData = result.getData().getClipData();

                    Uri profilePhotoUri = null;
                    if (clipData != null) {
                        for (int i = 0; i < clipData.getItemCount(); i++) {
                            profilePhotoUri = clipData.getItemAt(i).getUri();
                            // your code for multiple image selection
                            break;
                        }

                    } else {
                        profilePhotoUri = imgPicker.getImageFromResult(this, result.getResultCode(), result.getData());
                    }

                    Glide.with(EditUserProfileActivity.this).load(profilePhotoUri).circleCrop().placeholder(R.drawable.ic_user_profile).into(imgUserProfile);

                    profileImage.setProfileImageUri(profilePhotoUri);
                }
            });
        });

        editUserBirthday = findViewById(R.id.edit_userprofile_birthday);

        this.fillInputFields();
    }

    private void fillInputFields() {
        userProfile = UserProfileViewModel.getUserProfile(null);

        if (userProfile == null) {
            if (getIntent().hasExtra(LoginActivity.SIGN_UP_GOOGLE))
                //Regristierung
                beginSignUp();

            return;
        }

        editUserName.setText(userProfile.getUserName());
        editUserNickname.setText(userProfile.getUserID());
        editUserEmail.setText(userProfile.getUserEmail());
        editUserEmail.setEnabled(false);
        editUserBirthday.setText(Utilities.formatDate("dd.MM.yyyy", userProfile.getUserBirthday()));
        editUserPassword.setEnabled(false);
        editUserPasswordConfirm.setEnabled(false);

        Utilities.loadUserprofilePhotoIntoView(this, userProfile, imgUserProfile);
    }

    public void showDatePickerDialog(View v) {
        DatePicker newFragment = new DatePicker(editUserBirthday);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //ActionBar
        this.getMenuInflater().inflate(R.menu.menu_edit_userprofile_activity, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.MenuItemSaveCancelUserProfile:
                AppManager.getAppManager().finishActivity(this, RESULT_CANCELED);

                return true;
            case R.id.MenuItemSaveUserProfile:
                saveData();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean checkInputFields() {

        boolean isCheckOK = true;

        if (TextUtils.isEmpty(editUserName.getText())) {
            editUserName.setError("Bitte Name pflegen!");
            isCheckOK = false;
        }

        if (TextUtils.isEmpty(editUserNickname.getText())) {
            editUserNickname.setError("Bitte Benutzernamen pflegen!");
            isCheckOK = false;
        }

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z0-9-]+\\.+[a-z0-9]+";

        if (TextUtils.isEmpty(editUserEmail.getText()) ||
                !Objects.requireNonNull(editUserEmail.getText()).toString().trim().matches(emailPattern)) {

            editUserEmail.setError("Bitte eine gültige Email-Adresse pflegen!");
            isCheckOK = false;
        }

        if (TextUtils.isEmpty(editUserBirthday.getText())) {
            editUserBirthday.setError("Bitte Geburtstag eingeben!");
            isCheckOK = false;
        }

        if (editUserPassword.isEnabled()) {
            if (TextUtils.isEmpty(editUserPassword.getText()) || editUserPassword.getText().length() < 6) {

                editUserPassword.setError("Bitte mindestens 6 Zeichen eingeben!");

                isCheckOK = false;
            }
        }

        if (editUserPasswordConfirm.isEnabled()) {
            if (!Objects.requireNonNull(editUserPassword.getText()).toString().equals(Objects.requireNonNull(editUserPasswordConfirm.getText()).toString())) {
                editUserPasswordConfirm.setError("Passwort stimmt nicht überein!");
                isCheckOK = false;
            }
        }

        return isCheckOK;
    }

    private void saveData() {
        if (!this.checkInputFields()) return;

        if (userProfile != null) {
            userProfile.setUserBirthday(Utilities.getDateLong(editUserBirthday.getText().toString(), "dd.MM.yyyy"));
            userProfile.setUserName(Objects.requireNonNull(editUserName.getText()).toString());
            userProfile.setUserID(Objects.requireNonNull(editUserNickname.getText()).toString().toLowerCase());

            UserProfileViewModel.getInstance().save(EditUserProfileActivity.this, profileImage.getProfileImageUri(), userProfile);
        } else {
            Intent replyIntent = new Intent();
            replyIntent.putExtra("UserProfile_UserEmail", Objects.requireNonNull(editUserEmail.getText()).toString());
            replyIntent.putExtra("UserProfile_UserID", Objects.requireNonNull(editUserNickname.getText()).toString().toLowerCase());
            replyIntent.putExtra("UserProfile_UserPassword", Objects.requireNonNull(editUserPassword.getText()).toString());
            replyIntent.putExtra("UserProfile_UserBirthday", Utilities.getDateLong(editUserBirthday.getText().toString(), "dd.MM.yyyy"));
            replyIntent.putExtra("UserProfile_UserName", Objects.requireNonNull(editUserName.getText()).toString());

            UserProfileViewModel.getInstance().createUser(EditUserProfileActivity.this, replyIntent, profileImage, Utilities.getSignInToken(this));
        }
    }
}