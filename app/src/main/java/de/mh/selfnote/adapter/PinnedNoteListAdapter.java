package de.mh.selfnote.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import de.mh.selfnote.R;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.objects.Note;
import de.mh.selfnote.toolbox.CircularImageLoadingDrawable;
import de.mh.selfnote.toolbox.Utilities;

public class PinnedNoteListAdapter extends FirebaseListAdapter<Note> {

    final private Context parent;
    final private Hashtable<String, Note> hashTable = new Hashtable<>();
    private ArrayList<Note> dataList;
    private ArrayList<Note> filteredDataList;

    public Note findNoteByKey(String strNoteKey) {
        if (hashTable.containsKey(strNoteKey))
            return hashTable.get(strNoteKey);

        return null;
    }

    public ArrayList<Note> getDataList() {
        return dataList;
    }

    public void sortData(String[] sortKeyOrder) {
        Utilities.sortDataList(filteredDataList, sortKeyOrder);
        notifyDataSetChanged();
    }

    public void filterData(List<String> filterCriteria) {
        filteredDataList = Utilities.filterDataList(dataList, filterCriteria);
        Utilities.sortDataList(filteredDataList, null);
        notifyDataSetChanged();
    }

    public PinnedNoteListAdapter(@NonNull Context ctx, @NonNull FirebaseListOptions options) {
        super(options);
        parent = ctx;
    }

    @Override
    protected void populateView(@NonNull View v, @NonNull Note Note, int position) {
        TextView NoteTitle = v.findViewById(R.id.pinned_note_list_textview_name);
        ImageView NoteImage = v.findViewById(R.id.pinned_note_list_imageview_item);

        NoteTitle.setText(Note.getNoteTitle());
        //Hintergrundfarbe nach Kategorie
        if (Note.getNoteCategory() != null) {
            NoteTitle.setBackgroundColor(Note.getNoteCategory().getCategoryColor());
        }

        //default Kategoriebild
        int defaultNoteImageDrawableIdent = Utilities.getDrawableIdentByName(parent, Note.getNoteCategoryName());
        //Notizenbilder
        ArrayList<String> imgListURL = Note.getNoteImageListURL();
        if (imgListURL == null || imgListURL.isEmpty()) {

           /* UserProfileViewModel.getInstance().getUserImageReference(Note.getCreatorUID())
                    .child(Note.getNoteKey()).getDownloadUrl()
                    .addOnSuccessListener(uri -> {
                        Note.changeNoteImageListURL(uri.toString(), StatusKnz.INSERT);

                        Glide.with(parent)
                                .load(uri.toString())
                                .centerCrop()
                                .placeholder(CircularImageLoadingDrawable.getWait(parent))
                                .error(defaultNoteImageDrawableIdent)
                                .into(NoteImage);
                    })
                    .addOnFailureListener(e -> {
                        Glide.with(parent)
                                .load(defaultNoteImageDrawableIdent)
                                .centerInside()
                                .into(NoteImage);
                    });*/

            Glide.with(parent)
                    .load(defaultNoteImageDrawableIdent)
                    .centerInside()
                    .into(NoteImage);

        } else {

            for (String imageURL : imgListURL) {

                Glide.with(parent)
                        .load(imageURL)
                        .centerCrop()
                        .placeholder(CircularImageLoadingDrawable.getWait(parent))
                        .error(defaultNoteImageDrawableIdent)
                        .into(NoteImage);

                break;
            }
        }
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();

        dataList = null;
        hashTable.clear();
        for (Note snapshot : getSnapshots()) {
            hashTable.put(snapshot.getNoteKey(), snapshot);
        }

        if (hashTable != null && hashTable.size() > 0)
            dataList = new ArrayList<>(hashTable.values());

        filterData(null);

        if (parent instanceof BasicActivity)
            ((BasicActivity) parent).finishProgressBar();
    }

    @Override
    public void onChildChanged(@NonNull ChangeEventType type,
                               @NonNull DataSnapshot snapshot,
                               int newIndex,
                               int oldIndex) {

        if (parent instanceof BasicActivity)
            ((BasicActivity) parent).startProgressBar();
        super.onChildChanged(type, snapshot, newIndex, oldIndex);
    }

    @NonNull
    @Override
    public Note getItem(int position) {
        if (filteredDataList != null && filteredDataList.size() > 0) {
            return filteredDataList.get(position);
        }

        return null;
    }


    @Override
    public int getCount() {
        if (filteredDataList != null)
            return filteredDataList.size();

        return 0;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
}