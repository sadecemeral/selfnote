package de.mh.selfnote.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.google.android.flexbox.FlexboxLayoutManager;

import java.util.ArrayList;

import de.mh.selfnote.R;
import de.mh.selfnote.toolbox.CircularImageLoadingDrawable;

public class NoteImageListAdapter extends RecyclerView.Adapter<NoteImageListAdapter.NoteImageListAdapterViewHolder> {

    final private ArrayList<String> imageURLList;
    final private Context parentCtx;

    public NoteImageListAdapter(Context parent, ArrayList<String> itemList) {
        super();
        imageURLList = itemList;
        parentCtx = parent;
    }

    @NonNull
    @Override
    public NoteImageListAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_image_list_gallery_item, parent, false);
        return new NoteImageListAdapter.NoteImageListAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteImageListAdapterViewHolder holder, int position) {
        holder.loadImage(imageURLList.get(position));
    }

    @Override
    public int getItemCount() {

        if (imageURLList == null || imageURLList.isEmpty())
            return 0;

        return imageURLList.size();
    }

    class NoteImageListAdapterViewHolder extends RecyclerView.ViewHolder {

        private String noteImageURL;
        final private ImageView imageView;
        final private ViewGroup.LayoutParams layoutParams;

        public NoteImageListAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.note_image_list_gallery_view);
            layoutParams = imageView.getLayoutParams();

            final ImagePopup imagePopup = new ImagePopup(parentCtx);
            imagePopup.setBackgroundColor(Color.BLACK);  // Optional
            imagePopup.setFullScreen(true); // Optional
            imagePopup.setHideCloseIcon(true);  // Optional
            imagePopup.setImageOnClickClose(true);  // Optional

            imageView.setOnClickListener(view -> {
                imagePopup.initiatePopupWithGlide(noteImageURL);
                imagePopup.viewPopup();
            });
        }

        public void loadImage(String noteImageURL) {
            this.noteImageURL = noteImageURL;

            Glide.with(parentCtx)
                    .load(noteImageURL)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(CircularImageLoadingDrawable.getWait(parentCtx))
                    .into(imageView);

            if (layoutParams instanceof FlexboxLayoutManager.LayoutParams) {
                FlexboxLayoutManager.LayoutParams flexboxLp = (FlexboxLayoutManager.LayoutParams) layoutParams;
                flexboxLp.setFlexGrow(1.0f);
            }
        }
    }
}
