package de.mh.selfnote.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipDrawable;
import com.google.android.material.chip.ChipGroup;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import de.mh.selfnote.AddNewNoteActivity;
import de.mh.selfnote.DiaryActivity;
import de.mh.selfnote.NoteImageListGalleryActivity;
import de.mh.selfnote.R;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.extended.ExpandableNoteTextView;
import de.mh.selfnote.objects.Note;
import de.mh.selfnote.objects.SortKey;
import de.mh.selfnote.objects.UserProfile;
import de.mh.selfnote.toolbox.BetterActivityResult;
import de.mh.selfnote.toolbox.Utilities;
import de.mh.selfnote.viewModel.CategoryViewModel;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class NoteListAdapter extends FirebaseRecyclerAdapter<Note, NoteListAdapter.NoteViewHolder> {

    final private Context parentCtx;
    final private Hashtable<String, Note> hashTable = new Hashtable<>();
    private ArrayList<Note> dataList;
    private ArrayList<Note> filteredDataList;
    private String filteredByUID = null;

    public Note findNoteByKey(String strNoteKey) {
        return (hashTable.containsKey(strNoteKey)) ? hashTable.get(strNoteKey) : null;
    }

    public void sortData(String[] sortKeyOrder) {
        Utilities.sortDataList(filteredDataList, sortKeyOrder);
        notifyDataSetChanged();
    }

    public void filterData(List<String> filterCriteria) {
        filteredDataList = Utilities.filterDataList(dataList, filterCriteria);
        Utilities.sortDataList(filteredDataList, null);
        notifyDataSetChanged();
    }

    public NoteListAdapter(@NonNull Context ctx, @NonNull FirebaseRecyclerOptions options) {
        super(options);
        parentCtx = ctx;
    }

    public NoteListAdapter(@NonNull Context ctx, @NonNull FirebaseRecyclerOptions options, String filterByUID) {
        super(options);
        parentCtx = ctx;
        filteredByUID = filterByUID;
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.main_note_list_item, viewGroup, false);
        return new NoteViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull NoteViewHolder holder, int position, @NonNull Note model) {
        Note current = getItem(position);
        holder.bind(current, position);
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();

        dataList = null;
        filteredDataList = null;
        hashTable.clear();
        for (Note snapshot : getSnapshots()) {
            if (filteredByUID != null && !TextUtils.equals(snapshot.getCreatorUID(), filteredByUID))
                continue;

            hashTable.put(snapshot.getNoteKey(), snapshot);
        }

        if (!hashTable.isEmpty())
            filteredDataList = dataList = new ArrayList<>(hashTable.values());

        if (filteredByUID == null)
            filterData(null);
        else {
            sortData(new String[]{SortKey.SORTKEY_TIMESTAMP});
        }

        if (parentCtx != null && parentCtx instanceof BasicActivity)
            ((BasicActivity) parentCtx).finishProgressBar();
    }

    @Override
    public void onChildChanged(@NonNull ChangeEventType type,
                               @NonNull DataSnapshot snapshot,
                               int newIndex,
                               int oldIndex) {

        if (parentCtx != null && parentCtx instanceof BasicActivity)
            ((BasicActivity) parentCtx).startProgressBar();

        super.onChildChanged(type, snapshot, newIndex, oldIndex);
    }

    @NonNull
    @Override
    public Note getItem(int position) {
        if (filteredDataList != null && filteredDataList.size() > 0) {
            return filteredDataList.get(position);
        }

        return null;
    }

    @Override
    public int getItemCount() {
        if (filteredDataList != null)
            return filteredDataList.size();

        return 0;
    }

    class NoteViewHolder extends RecyclerView.ViewHolder {
        final ImageView NoteUserImage;
        final TextView NoteUserID;
        final ImageView NoteLockImg;
        final ImageView NotePinnedImg;
        final Button NoteActionsBtn;
        final ImageView NoteImage;
        final TextView NoteImageCounter;
        final TextView NoteTitle;
        final ExpandableNoteTextView NoteTextPreview;
        final TextView NoteText;
        final TextView NoteTimestamp;
        final ChipGroup NoteKeywordChipGroup;
        final RatingBar NoteRatingBar;

        int itemPosition;
        Note Note;
        UserProfile UserProfile;
        final BetterActivityResult<Intent, ActivityResult> activityLauncher;
        final SparseBooleanArray mCollapsedStatus = new SparseBooleanArray();

        @SuppressLint({"NonConstantResourceId", "WrongViewCast"})
        public NoteViewHolder(View itemView) {
            super(itemView);

            activityLauncher = ((BasicActivity) parentCtx).getActivityLauncher();

            NoteUserImage = itemView.findViewById(R.id.view_note_list_item_user_image);
            NoteUserID = itemView.findViewById(R.id.view_note_list_item_user_userid);
            NoteKeywordChipGroup = itemView.findViewById(R.id.tag_group_note_list_item_keyword);
            NoteTitle = itemView.findViewById(R.id.view_note_list_item_title);
            NoteImage = itemView.findViewById(R.id.imageview_note_list_item_image);
            NoteImageCounter = itemView.findViewById(R.id.view_note_list_item_image_counter);
            NoteTextPreview = itemView.findViewById(R.id.etv_note_list_item_text_preview);
            NoteText = itemView.findViewById(R.id.view_note_list_item_text);
            NoteTimestamp = itemView.findViewById(R.id.view_note_list_item_timestamp);
            NoteActionsBtn = itemView.findViewById(R.id.btn_note_list_item_more_actions);
            NoteRatingBar = itemView.findViewById(R.id.ratingbar_note_list_item_diary);
            NoteLockImg = itemView.findViewById(R.id.imageview_note_list_item_locked);
            NotePinnedImg = itemView.findViewById(R.id.imageview_note_list_item_pinned);

            //NoteImageListGallery Collage
            NoteImage.setOnClickListener(view ->
            {
                if (Note.getNoteImageListURL() == null || Note.getNoteImageListURL().isEmpty())
                    return;

                //onClick
                Intent replyIntent = new Intent(parentCtx, NoteImageListGalleryActivity.class);
                replyIntent.putExtra(NoteImageListGalleryActivity.INTENT_IMG_URL_LIST, Note.getNoteImageListURL());
                parentCtx.startActivity(replyIntent);
            });

            //bearbeiten. anpinnen
            NoteActionsBtn.setOnClickListener(view ->
            {
                PopupMenu popup = new PopupMenu(parentCtx, view);
                popup.inflate(R.menu.menu_note_list_item_actions);

                if (!UserProfileViewModel.isLoggedUser(Note.getCreatorUID()))
                    popup.getMenu().findItem(R.id.MenuItemNoteItemActionEdit).setEnabled(false);
                else
                    popup.getMenu().findItem(R.id.MenuItemNoteItemActionEdit).setEnabled(true);

                if (NotePinnedImg.getVisibility() == View.VISIBLE) {
                    popup.getMenu().findItem(R.id.MenuItemNoteItemActionPin).setVisible(false);
                    popup.getMenu().findItem(R.id.MenuItemNoteItemActionUnpin).setVisible(true);
                } else {
                    popup.getMenu().findItem(R.id.MenuItemNoteItemActionPin).setVisible(true);
                    popup.getMenu().findItem(R.id.MenuItemNoteItemActionUnpin).setVisible(false);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    popup.setForceShowIcon(true);
                }
                popup.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.MenuItemNoteItemActionEdit:
                            Intent replyIntent;

                            if (CategoryViewModel.categoryName_Diary.equals(Note.getNoteCategoryName()))
                                replyIntent = new Intent(parentCtx, DiaryActivity.class);
                            else
                                replyIntent = new Intent(parentCtx, AddNewNoteActivity.class);

                            replyIntent.putExtra("NOTE_KEY", Note.getNoteKey());

                            activityLauncher.launch(replyIntent, result -> {
                                if (result.getResultCode() != Activity.RESULT_OK) {
                                    Toast.makeText(parentCtx.getApplicationContext(), R.string.toast_save_canceled, Toast.LENGTH_SHORT).show();
                                }
                            });
                            return true;
                        case R.id.MenuItemNoteItemActionPin:
                        case R.id.MenuItemNoteItemActionUnpin:
                            UserProfileViewModel.getInstance().setNotePinned(Note);
                            return true;
                        default:
                            return false;
                    }
                });
                popup.show();
            });
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        public void bind(Note current, int position) {
            if (current == null)
                return;

            this.Note = current;
            this.itemPosition = position;

            //Benutzerprofil
            UserProfile = UserProfileViewModel.getUserProfile(Note.getCreatorUID());
            if (UserProfile != null) {

                NoteUserID.setText(UserProfile.getUserID());

                Utilities.loadUserprofilePhotoIntoView(parentCtx, UserProfile, NoteUserImage);
            }

            //default Kategoriebild
            int defaultNoteImageDrawableIdent = Utilities.getDrawableIdentByName(parentCtx, Note.getNoteCategoryName());
            //Bildzähler
            NoteImageCounter.setVisibility(View.GONE);
            //Notizenbilder
            ArrayList<String> imgListURL = Note.getNoteImageListURL();
            if (imgListURL == null || imgListURL.isEmpty()) {

                /*UserProfileViewModel.getInstance().getUserImageReference(Note.getCreatorUID())
                        .child(Note.getNoteKey()).getDownloadUrl()
                        .addOnSuccessListener(uri -> {
                            Note.changeNoteImageListURL(uri.toString(), StatusKnz.INSERT);

                            Glide.with(parentCtx)
                                    .load(uri.toString())
                                    .centerCrop()
                                    .placeholder(defaultNoteImageDrawableIdent)
                                    .into(NoteImage);
                        })
                        .addOnFailureListener(e -> {
                            Glide.with(parentCtx)
                                    .load(defaultNoteImageDrawableIdent)
                                    .into(NoteImage);

                        });*/

                Glide.with(parentCtx)
                        .load(defaultNoteImageDrawableIdent)
                        .into(NoteImage);

            } else {
                if (imgListURL.size() > 1) {
                    NoteImageCounter.setVisibility(View.VISIBLE);
                    NoteImageCounter.setText(imgListURL.size() + "+");
                }

                for (String imageURL : imgListURL) {

                    Glide.with(parentCtx)
                            .load(imageURL)
                            .centerCrop()
                            .placeholder(defaultNoteImageDrawableIdent)
                            .into(NoteImage);

                    break;
                }
            }

            //check, if Pinned Note
            if (UserProfileViewModel.getInstance().isNotePinnedByLoggedUser(Note))
                NotePinnedImg.setVisibility(View.VISIBLE);
            else
                NotePinnedImg.setVisibility(View.GONE);

            //check, if Locked/Personal Note
            if (Note.isNotePersonal())
                NoteLockImg.setVisibility(View.VISIBLE);
            else
                NoteLockImg.setVisibility(View.GONE);

            //Titel, Text und Anlagezeitpunkt
            NoteTitle.setText(Note.getNoteTitle());
            NoteTextPreview.setText(Note.getNoteText(), mCollapsedStatus, itemPosition,
                    NoteText);
            NoteTimestamp.setText(Utilities.formatDate("dd.MM.yyyy HH:mm", Note.getNoteTimestamp()));

            //Stichwörter
            NoteKeywordChipGroup.removeAllViews();
            if (Note.getNoteKeyword() != null && Note.getNoteKeyword().length() > 0) {
                String strValue = Note.getNoteKeyword();
                String[] strTag = strValue.split("[, ]");

                if (strTag.length < 1)
                    strTag = new String[]{strValue};

                int iCounter = -1;
                while (++iCounter < strTag.length) {
                    final Chip chip = new Chip(parentCtx);
                    chip.setChipDrawable(ChipDrawable.createFromResource(parentCtx, R.xml.chip));
                    chip.setText(strTag[iCounter]);
                    chip.setTextAppearance(R.style.ChipTextAppearance);
                    chip.setClickable(false);
                    NoteKeywordChipGroup.addView(chip);
                }
            }

            //Bewertung/Rating
            if (CategoryViewModel.categoryName_Diary.equals(Note.getNoteCategoryName())) {
                NoteRatingBar.setRating(Note.getNoteRating());
                NoteRatingBar.setVisibility(View.VISIBLE);
            } else
                NoteRatingBar.setVisibility(View.GONE);

            //Hintergrundfarbe nach Kategorie
            if (Note.getNoteCategory() != null) {
                GradientDrawable shape = (GradientDrawable) parentCtx.getDrawable(R.drawable.shape_rounded_border);
                shape.setColor(Note.getNoteCategory().getCategoryColor());
                itemView.setBackground(shape);
            }
        }
    }
}