package de.mh.selfnote.adapter;

import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;

import java.util.ArrayList;

import de.mh.selfnote.R;
import de.mh.selfnote.UserNotesActivity;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.objects.UserProfile;
import de.mh.selfnote.toolbox.Utilities;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class UserListAdapter extends FirebaseRecyclerAdapter<UserProfile,
        UserListAdapter.UserProfileViewHolder> {

    BasicActivity parentCtx;
    private ArrayList<UserProfile> dataList = new ArrayList<>();

    public UserListAdapter(BasicActivity ctx, @NonNull FirebaseRecyclerOptions options) {
        super(options);
        parentCtx = ctx;
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();

        dataList.clear();

        for (UserProfile snapshot : getSnapshots()) {
            if (snapshot == null || snapshot.getFirebaseUID() == null)
                continue;

            if (!UserProfileViewModel.isLoggedUser(snapshot.getFirebaseUID()))
                dataList.add(snapshot);
        }

        if (parentCtx != null)
            parentCtx.finishProgressBar();
    }

    @Override
    public void onChildChanged(@NonNull ChangeEventType type,
                               @NonNull DataSnapshot snapshot,
                               int newIndex,
                               int oldIndex) {

        if (parentCtx != null)
            parentCtx.startProgressBar();

        super.onChildChanged(type, snapshot, newIndex, oldIndex);
    }

    @NonNull
    @Override
    public UserProfile getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @NonNull
    @Override
    public UserProfileViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.user_list_item, viewGroup, false);
        return new UserProfileViewHolder(view, this);
    }

    @Override
    protected void onBindViewHolder(@NonNull UserProfileViewHolder holder, int position,
                                    @NonNull UserProfile model) {
        holder.bind(model);
    }

    class UserProfileViewHolder extends RecyclerView.ViewHolder {
        TextView viewUserId;
        TextView viewUserName;
        ImageView viewImgView;
        Button actionButton;
        Button viewRelationState;
        UserProfile userProfile;

        public UserProfileViewHolder(View viewItem, FirebaseRecyclerAdapter adapter) {
            super(viewItem);

            viewUserId = viewItem.findViewById(R.id.view_user_list_item_userid);
            viewUserName = viewItem.findViewById(R.id.view_user_list_item_username);
            viewImgView = viewItem.findViewById(R.id.view_user_list_item_image);
            viewRelationState = viewItem.findViewById(R.id.view_user_list_item_relationship_state);
            actionButton = viewItem.findViewById(R.id.btn_user_list_item_more_actions);
            actionButton.setOnClickListener(view -> {

                PopupMenu popup = new PopupMenu(parentCtx, view);
                popup.inflate(R.menu.menu_user_list_item_actions);
                popup.getMenu().findItem(R.id.MenuItemUserItemActionRequest).setVisible(true);
                popup.getMenu().findItem(R.id.MenuItemUserItemActionCancel).setVisible(false);
                popup.getMenu().findItem(R.id.MenuItemUserItemActionMute).setVisible(false);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    popup.setForceShowIcon(true);
                }

                popup.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.MenuItemUserItemActionRequest:
                            UserProfileViewModel.getInstance().requestFriendship(
                                    parentCtx, userProfile.getFirebaseUID(), adapter);
                            return true;
                        default:
                            return false;
                    }
                });
                popup.show();

            });
        }

        private void setViewRelationState(String strRelationshipState) {
            if (strRelationshipState == null) {
                actionButton.setVisibility(View.VISIBLE);
                viewRelationState.setVisibility(View.GONE);
            } else {
                actionButton.setVisibility(View.GONE);
                viewRelationState.setVisibility(View.VISIBLE);
                viewRelationState.setText(strRelationshipState);

                if (TextUtils.equals(strRelationshipState, parentCtx.getString(R.string.textview_user_relationship_state_friend))) {

                    if (userProfile == null || TextUtils.isEmpty(userProfile.getFirebaseUID()))
                        return;

                    itemView.setClickable(true);
                    itemView.setOnClickListener(v -> {
                        Intent intent = new Intent(parentCtx, UserNotesActivity.class);
                        intent.putExtra(UserNotesActivity.EXTRA_PARAM_ID, userProfile.getFirebaseUID());
                        parentCtx.startActivity(intent);
                    });
                }
            }
        }

        public void bind(UserProfile current) {
            userProfile = current;

            itemView.setOnClickListener(null);
            itemView.setClickable(false);

            if (userProfile == null)
                return;

            //set Relationship State
            this.setViewRelationState(UserProfileViewModel.getInstance().getRelationshipState(parentCtx, userProfile.getFirebaseUID()));

            viewUserId.setText(userProfile.getUserID());
            viewUserName.setText(userProfile.getUserName());

            //load Profilephoto
            Utilities.loadUserprofilePhotoIntoView(parentCtx, userProfile, viewImgView);
        }
    }
}