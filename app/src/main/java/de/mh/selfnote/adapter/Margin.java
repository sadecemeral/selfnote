package de.mh.selfnote.adapter;

import static android.text.Layout.DIR_RIGHT_TO_LEFT;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.Layout;
import android.text.style.LeadingMarginSpan;

public class Margin implements LeadingMarginSpan.LeadingMarginSpan2 {
    private int margin;
    private int lines;
    private boolean useFirstLineMargin = true;

    Margin(int lines, int margin) {
        this.margin = margin;
        this.lines = lines;
    }

    @Override
    public void drawLeadingMargin(Canvas canvas, Paint p, int x, int dir,
                                  int top, int baseline, int bottom,
                                  CharSequence text,
                                  int start, int end, boolean first, Layout layout) {

        int lineForOffset = layout.getLineForOffset(start);
        System.out.println("Zeile "+lineForOffset+": "+layout.getLineWidth(lineForOffset)+" - "+text.subSequence(start, end));


        if (lineForOffset > lines) {
            useFirstLineMargin = false;
        } else {
            useFirstLineMargin = true;
        }
    }

    @Override
    public int getLeadingMargin(boolean first) {

        if (useFirstLineMargin) {
            return margin;
        } else {
            return 0;
        }
    }

    @Override
    public int getLeadingMarginLineCount() {

        if (useFirstLineMargin) {
            return lines;
        } else {
            return 0;
        }
    }
}