package de.mh.selfnote.adapter;

import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.common.ChangeEventType;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;

import de.mh.selfnote.R;
import de.mh.selfnote.UserNotesActivity;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.objects.UserProfile;
import de.mh.selfnote.objects.UserRelationship;
import de.mh.selfnote.toolbox.Utilities;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class UserRelationshipListAdapter extends FirebaseRecyclerAdapter<UserRelationship, UserRelationshipListAdapter.UserRelationshipViewHolder> {

    BasicActivity parent;

    public UserRelationshipListAdapter(BasicActivity ctx, @NonNull FirebaseRecyclerOptions options) {
        super(options);
        parent = ctx;
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();

        if (parent != null)
            parent.finishProgressBar();
    }

    @Override
    public void onChildChanged(@NonNull ChangeEventType type,
                               @NonNull DataSnapshot snapshot,
                               int newIndex,
                               int oldIndex) {

        if (parent != null)
            parent.startProgressBar();

        super.onChildChanged(type, snapshot, newIndex, oldIndex);
    }

    @NonNull
    @Override
    public UserRelationshipViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.user_list_item, viewGroup, false);
        return new UserRelationshipViewHolder(view);
    }

    @Override
    protected void onBindViewHolder(@NonNull UserRelationshipViewHolder holder, int position,
                                    @NonNull UserRelationship model) {
        holder.bind(model);
    }

    class UserRelationshipViewHolder extends RecyclerView.ViewHolder {
        TextView viewUserId;
        TextView viewUserName;
        ImageView viewImgView;
        Button viewRelationState;
        Button actionButton;
        UserRelationship userRelationship;
        UserProfile userProfile;

        public UserRelationshipViewHolder(View viewItem) {
            super(viewItem);

            viewUserId = viewItem.findViewById(R.id.view_user_list_item_userid);
            viewUserName = viewItem.findViewById(R.id.view_user_list_item_username);
            viewImgView = viewItem.findViewById(R.id.view_user_list_item_image);
            viewRelationState = viewItem.findViewById(R.id.view_user_list_item_relationship_state);
            actionButton = viewItem.findViewById(R.id.btn_user_list_item_more_actions);
            actionButton.setOnClickListener(view -> {

                PopupMenu popup = new PopupMenu(parent, view);
                popup.inflate(R.menu.menu_user_list_item_actions);
                popup.getMenu().findItem(R.id.MenuItemUserItemActionRequest).setVisible(false);
                popup.getMenu().findItem(R.id.MenuItemUserItemActionCancel).setVisible(true);
                popup.getMenu().findItem(R.id.MenuItemUserItemActionMute).setVisible(false);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    popup.setForceShowIcon(true);
                }

                popup.setOnMenuItemClickListener(item -> {
                    switch (item.getItemId()) {
                        case R.id.MenuItemUserItemActionCancel:
                            parent.showNoticeDialog(
                                    parent.getString(R.string.dialog_title_warning),
                                    parent.getString(R.string.dialog_text_question_cancel_friendship,
                                            userProfile.getUserID()),
                                    new int[]{R.string.btn_cancel_relationship, R.string.btn_Cancel},
                                    userRelationship);
                            return true;
                        case R.id.MenuItemUserItemActionMute:

                            return true;
                        default:
                            return false;
                    }
                });
                popup.show();
            });
        }

        private void setViewRelationState(String strRelationshipState) {
            if (TextUtils.equals(parent.getString(R.string.textview_user_relationship_state_friend), strRelationshipState)) {
                actionButton.setVisibility(View.VISIBLE);
                viewRelationState.setVisibility(View.GONE);

                if (userProfile == null || TextUtils.isEmpty(userProfile.getFirebaseUID()))
                    return;

                itemView.setClickable(true);
                itemView.setOnClickListener(v -> {
                    Intent intent = new Intent(parent, UserNotesActivity.class);
                    intent.putExtra(UserNotesActivity.EXTRA_PARAM_ID, userProfile.getFirebaseUID());
                    parent.startActivity(intent);
                });
            } else {
                actionButton.setVisibility(View.GONE);
                viewRelationState.setVisibility(View.VISIBLE);
                viewRelationState.setText(strRelationshipState);
            }
        }

        public void bind(UserRelationship current) {
            userRelationship = current;

            itemView.setOnClickListener(null);
            itemView.setClickable(false);

            if (userRelationship == null)
                return;

            userProfile = UserProfileViewModel.getUserProfile(userRelationship.getContactUID());
            if (userProfile == null)
                return;

            //set Relationship State
            this.setViewRelationState(UserProfileViewModel.getInstance().getRelationshipState(parent, userProfile.getFirebaseUID()));

            viewUserId.setText(userProfile.getUserID());
            viewUserName.setText(userProfile.getUserName());

            //load Profilephoto
            Utilities.loadUserprofilePhotoIntoView(parent, userProfile, viewImgView);
        }
    }
}