package de.mh.selfnote.adapter;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;

import java.util.Hashtable;

import de.mh.selfnote.objects.Category;
import de.mh.selfnote.viewModel.CategoryViewModel;

public class NoteCategoryListAdapter extends FirebaseListAdapter<Category> implements SpinnerAdapter {

    private Hashtable<String, Integer> htItemPosition = new Hashtable<>();

    public NoteCategoryListAdapter(FirebaseListOptions options) {
        super(options);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        Category oItem = getItem(position);

        if (TextUtils.equals(oItem.getCategoryName(), CategoryViewModel.categoryName_Diary)) {
            view.setVisibility(View.GONE);
        }

        return view;
    }

    @Override
    protected void populateView(@NonNull View view, @NonNull Category model, int position) {
        ((TextView) view).setText(model.getCategoryName());
        view.setBackgroundColor(model.getCategoryColor());
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();

        int iCounter = -1;
        htItemPosition.clear();
        for (Category snapshot : getSnapshots()) {
            ++iCounter;
            htItemPosition.put(snapshot.getCategoryKey(), iCounter);
        }
    }

    public int getItemPosition(String strKey) {
        if (htItemPosition == null || htItemPosition.isEmpty())
            return -1;

        if (htItemPosition.containsKey(strKey))
            return htItemPosition.get(strKey);

        return -1;
    }
}