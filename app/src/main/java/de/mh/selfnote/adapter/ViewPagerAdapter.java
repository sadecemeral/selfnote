package de.mh.selfnote.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.ArrayList;

public class ViewPagerAdapter extends FragmentStateAdapter {

    private ArrayList<Fragment> fragmentList = new ArrayList<>();
    private FragmentManager fragmentManager;

    public ViewPagerAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
        this.fragmentManager = fragmentManager;
    }

    public void addFragment(Fragment newFragment, int position) {
        Fragment existFragment = fragmentManager.findFragmentByTag("f"+position);
        if (existFragment == null)
            fragmentList.add(newFragment);
        else
            fragmentList.add(existFragment);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) { return fragmentList.get(position); }

    @Override
    public int getItemCount() {
        return fragmentList.size();
    }

    public Fragment getItem(int position) { return fragmentList.get(position); }
}