package de.mh.selfnote.service;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.google.firebase.FirebaseException;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;

import de.mh.selfnote.R;
import de.mh.selfnote.toolbox.Utilities;

public class UserNotificationService extends FirebaseMessagingService {

    private static final String TAG = "UserNotificationService";
    public static String registrationToken;
    public static Long registrationTime;

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages
        // are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data
        // messages are the type
        // traditionally used with GCM. Notification messages are only received here in
        // onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated
        // notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages
        // containing both notification
        // and data payloads are treated as notification messages. The Firebase console always
        // sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());

            Utilities.showNotification(this,
                    remoteMessage.getNotification().getTitle(),
                    remoteMessage.getNotification().getBody());
        }
    }

    /**
     * Schedule async work using WorkManager.
     */
    private void scheduleJob() {
        // [START dispatch_job]
        OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(SelfNoteWorker.class)
                .build();
        WorkManager.getInstance(this).beginWith(work).enqueue();
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    @Override
    public void onNewToken(@NonNull String token) {
        Log.d(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // FCM registration token to your app server.
        registrationToken = token;
        registrationTime = System.currentTimeMillis();
    }

    public void sendToToken(String registrationToken) throws FirebaseException {
        // [START send_to_token]
        // This registration token comes from the client FCM SDKs.

        // See documentation on defining a message payload.
        RemoteMessage message = new RemoteMessage.Builder(registrationToken)
                .addData("score", "850")
                .addData("time", "2:45")
                .build();

        // Send a message to the device corresponding to the provided
        // registration token.
        FirebaseMessaging.getInstance().send(message);
        // Response is a message ID string.
        System.out.println("Successfully sent message: ");
        // [END send_to_token]
    }

    public void sendAll(List<String> registrationTokens) throws FirebaseException {
        // [START send_multicast]
        // Create a list containing up to 500 registration tokens.
        // These registration tokens come from the client FCM SDKs.
        // List<String> registrationTokens = Arrays.asList(
        // "YOUR_REGISTRATION_TOKEN_1",
        // // ...
        // "YOUR_REGISTRATION_TOKEN_n"
        //  );

        for (String token : registrationTokens) {
            RemoteMessage message = new RemoteMessage.Builder(token)
                    .addData("score", "850")
                    .addData("time", "2:45")
                    .build();

            FirebaseMessaging.getInstance().send(message);
        }
    }

    private void subscribeToTopic(Context parent, String strTopic) {
        // [START subscribe_topics]
        FirebaseMessaging.getInstance().subscribeToTopic(strTopic)
                .addOnCompleteListener(task -> {
                    String msg = parent.getString(R.string.msg_subscribed);
                    if (!task.isSuccessful()) {
                        msg = parent.getString(R.string.msg_subscribe_failed);
                    }
                    Log.d(TAG, msg);
                    Toast.makeText(parent, msg, Toast.LENGTH_SHORT).show();
                });
        // [END subscribe_topics]
    }
}