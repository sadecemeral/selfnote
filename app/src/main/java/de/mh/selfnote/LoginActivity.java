package de.mh.selfnote;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.IntentSenderRequest;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.splashscreen.SplashScreen;

import com.google.android.gms.auth.api.identity.BeginSignInRequest;
import com.google.android.gms.auth.api.identity.SignInClient;
import com.google.android.gms.auth.api.identity.SignInCredential;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.GoogleAuthProvider;

import java.util.Objects;

import de.mh.selfnote.extended.BaseClickableSpan;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.extended.NoticeDialogFragment;
import de.mh.selfnote.toolbox.AppManager;
import de.mh.selfnote.toolbox.Utilities;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class LoginActivity extends BasicActivity {

    private UserProfileViewModel _viewModel = UserProfileViewModel.getInstance();
    private TextInputEditText editTextUserEmail;
    private TextInputEditText editTextUserPassw;
    private CheckBox checkBoxRemember;
    private SplashScreen splashScreen;

    public final static String SIGN_UP_GOOGLE = "signUpGoogle";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        splashScreen = SplashScreen.installSplashScreen(LoginActivity.this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.activity_title_login);

        //Google
        //Google SignIn
        SignInButton signInButton = findViewById(R.id.sign_in_button);
        Utilities.setGooglePlusButtonText(signInButton, getString(R.string.btn_google_sign_in));
        signInButton.setOnClickListener(v -> {
            beginSignIn();
        });

        //Google SignUp
        SignInButton signUpButton = findViewById(R.id.sign_up_button);
        Utilities.setGooglePlusButtonText(signUpButton, getString(R.string.btn_google_sign_up));
        signUpButton.setOnClickListener(v -> {
            _viewModel.clearUserProfile(LoginActivity.this, true);
            Intent intent = new Intent(LoginActivity.this, EditUserProfileActivity.class);
            intent.putExtra(SIGN_UP_GOOGLE, true);
            startActivity(intent);
        });

        //Google One Tap SignIn
        oneTapClient = _viewModel.createOneTapClient(LoginActivity.this);
        signInRequest = BeginSignInRequest.builder().setGoogleIdTokenRequestOptions(BeginSignInRequest.GoogleIdTokenRequestOptions.builder().setSupported(true)
                        // Your server's client ID, not your Android client ID.
                        .setServerClientId(getString(R.string.default_web_client_id))
                        // Only show accounts previously used to sign in.
                        .setFilterByAuthorizedAccounts(true).build())
                // Automatically sign in when exactly one credential is retrieved.
                .setAutoSelectEnabled(true).build();
        //Google

        mProgressBar = findViewById(R.id.progressBar_login);

        editTextUserEmail = findViewById(R.id.input_userprofile_email);
        editTextUserPassw = findViewById(R.id.input_userprofile_password);
        checkBoxRemember = findViewById(R.id.checkBox_userprofile_remember);
        checkBoxRemember.setOnCheckedChangeListener((compoundButton, b) -> {
            if (!compoundButton.isChecked()) {
                Utilities.resetLoginRemember(LoginActivity.this);
            }
        });

        TextView hyperlinkView = findViewById(R.id.hyperlink_new_account);
        BaseClickableSpan.clickify(hyperlinkView, getString(R.string.hyperlink_new_account), () -> {

            _viewModel.clearUserProfile(LoginActivity.this, true);

            startActivity(new Intent(LoginActivity.this, EditUserProfileActivity.class));
        });

        TextView linkResetPassword = findViewById(R.id.hyperlink_reset_password);
        BaseClickableSpan.clickify(linkResetPassword, getString(R.string.hyperlink_reset_password), () -> {
            editTextUserPassw.getText().clear();

            if (!this.checkEMail()) {
                Toast.makeText(this,
                        "Bitte eine gültige Email-Adresse pflegen!", Toast.LENGTH_LONG).show();
                return;
            }

            _viewModel.sendPasswordResetEmail(this, editTextUserEmail.getEditableText().toString());
        });

        Button btn_login = findViewById(R.id.btn_login_user);
        btn_login.setOnClickListener(view ->
                accessToMain(editTextUserEmail.getEditableText().toString(), editTextUserPassw.getEditableText().toString(), null, checkBoxRemember.isChecked()));
    }

    @Override
    protected void onStart() {
        super.onStart();

        //automatische Anmeldung
        boolean remember = Utilities.isLoginRemember(LoginActivity.this);
        if (remember) {
            // Keep the splash screen visible for this Activity
            splashScreen.setKeepOnScreenCondition(() -> true);

            editTextUserEmail.setText(Utilities.getLoginUserEmail(this));
            editTextUserPassw.setText(Utilities.getLoginUserPassword(this));
            checkBoxRemember.setChecked(true);

            accessToMain(Utilities.getLoginUserEmail(this), Utilities.getLoginUserPassword(this), null, false);
        } else {
            clearInputFileds();

            if (Boolean.TRUE.equals(Utilities.getShowOneTapUI(this))) {
                String idToken = Utilities.getSignInToken(LoginActivity.this);
                if (idToken != null && !idToken.isEmpty())
                    accessToMain(null, null, idToken, false);
                else
                    beginSignIn();
            }
        }
    }

    public void clearInputFileds() {
        editTextUserEmail.setText(null);
        editTextUserPassw.setText(null);
        editTextUserPassw.clearFocus();
        checkBoxRemember.setChecked(false);
    }

    private void accessToMain(String strUserEmail, String strUserPassword, String idToken, boolean isChecked) {

        if (idToken == null || idToken.isEmpty()) {
            if (!this.checkInputFields()) {
                finishProgressBar();
                return;
            }
        }

        if (isChecked) {
            Utilities.setLoginRemember(this, strUserEmail, strUserPassword);
        }

        _viewModel.loginUser(this, strUserEmail, strUserPassword, idToken);
    }

    public void setProgressBarProgress(int iDiff) {
        if (mProgressBar == null)
            return;

        mProgressBar.incrementProgressBy(iDiff);
        if (mProgressBar.getProgress() >= mProgressBar.getMax()) {

            Activity preAct = AppManager.getAppManager().previousActivity();
            String className = AddNewNoteActivity.class.getName();

            if (preAct == null || !className.equals(preAct.getClass().getName())) {
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }

            AppManager.getAppManager().finishActivity(LoginActivity.this, RESULT_OK);
        }
    }

    /**
     * If sign in fails, display a message to the user.
     *
     * @param authCredentialSignInMethod
     * @param exception
     */
    public void signInFailed(String authCredentialSignInMethod, Exception exception) {
        exception.printStackTrace();

        if (TextUtils.equals(authCredentialSignInMethod, GoogleAuthProvider.GOOGLE_SIGN_IN_METHOD)) {
            beginSignIn();
        } else {
            Toast.makeText(LoginActivity.this, getString(R.string.toast_login_incorrect), Toast.LENGTH_SHORT).show();
        }
    }

    public void finishProgressBar() {
        super.finishProgressBar();

        if (splashScreen != null)
            splashScreen.setKeepOnScreenCondition(() -> false);
    }

    @Override
    public void onDialogPositiveClick(NoticeDialogFragment dialog, int iTextId) {
        super.onDialogPositiveClick(dialog, iTextId);

        _viewModel.sendEmailVerification(this);
    }

    @Override
    public void onDialogNegativeClick(NoticeDialogFragment dialog, int iTextId) {
        super.onDialogNegativeClick(dialog, iTextId);

        _viewModel.clearUserProfile(LoginActivity.this, true);
    }

    private boolean checkInputFields() {

        boolean isCheckOK = true;

        if (!checkEMail()) {
            editTextUserEmail.setError("Bitte eine gültige Email-Adresse pflegen!");
            isCheckOK = false;
        }

        if (TextUtils.isEmpty(editTextUserPassw.getText())) {
            editTextUserPassw.setError("Bitte Passwort eingeben!");
            isCheckOK = false;
        }

        return isCheckOK;
    }

    private boolean checkEMail() {

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z0-9-]+\\.+[a-z0-9]+";

        if (TextUtils.isEmpty(editTextUserEmail.getText()) ||
                !Objects.requireNonNull(editTextUserEmail.getText()).toString().trim().matches(emailPattern)) {
            return false;
        }

        return true;
    }

    private SignInClient oneTapClient;
    private BeginSignInRequest signInRequest;
    private final ActivityResultLauncher<IntentSenderRequest> intentSender = registerForActivityResult(new ActivityResultContracts.StartIntentSenderForResult(), activityResult -> {
        Intent data = activityResult.getData();
        try {
            SignInCredential credential = oneTapClient.getSignInCredentialFromIntent(data);
            String idToken = credential.getGoogleIdToken();
            String password = credential.getPassword();
            String email = credential.getId();
            if (idToken != null) {
                Utilities.setSignInToken(LoginActivity.this, idToken);
                // Got an ID token from Google. Use it to authenticate
                // with Firebase.
                accessToMain(email, password, idToken, false);
            } else if (password != null) {
                // Got a saved username and password. Use them to authenticate
                // with your backend.
                System.out.println("Got password.");
            }
        } catch (ApiException e) {
            switch (e.getStatusCode()) {
                case CommonStatusCodes.CANCELED:
                    System.out.println("One-tap dialog was closed.");
                    Utilities.setShowOneTapUI(this, false);
                    break;
                case CommonStatusCodes.NETWORK_ERROR:
                    System.out.println("One-tap encountered a network error.");
                    break;
                default:
                    Toast.makeText(LoginActivity.this, "Couldn't get credential from result. " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    });

    public void beginSignIn() {
        oneTapClient.beginSignIn(signInRequest).addOnSuccessListener(this, result -> {

            IntentSenderRequest senderRequest = new IntentSenderRequest.Builder(result.getPendingIntent().getIntentSender()).build();
            intentSender.launch(senderRequest);

        }).addOnFailureListener(this, e -> {
            // No saved credentials found. Launch the One Tap sign-up flow, or
            // do nothing and continue presenting the signed-out UI.
            // No Google Accounts found. Just continue presenting the signed-out UI.
            Utilities.setShowOneTapUI(this, false);
            Toast.makeText(LoginActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        });
    }
}