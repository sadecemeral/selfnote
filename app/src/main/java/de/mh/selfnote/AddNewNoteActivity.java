package de.mh.selfnote;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ImageSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.target.Target;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.android.material.chip.ChipDrawable;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import de.mh.selfnote.adapter.NoteCategoryListAdapter;
import de.mh.selfnote.extended.BasicActivity;
import de.mh.selfnote.extended.NoticeDialogFragment;
import de.mh.selfnote.objects.Category;
import de.mh.selfnote.objects.Note;
import de.mh.selfnote.objects.NoteImage;
import de.mh.selfnote.objects.StatusKnz;
import de.mh.selfnote.toolbox.AppManager;
import de.mh.selfnote.toolbox.ImagePicker;
import de.mh.selfnote.toolbox.Utilities;
import de.mh.selfnote.viewModel.UserProfileViewModel;

public class AddNewNoteActivity extends BasicActivity {

    private class DownloadFilesTask extends AsyncTask<String, Integer, NoteImage> {
        final WeakReference<Context> parentCtx;

        WeakReference<ImageView> imageView;
        WeakReference<FutureTarget<Bitmap>> futureTarget;

        private DownloadFilesTask(Context parent) {
            parentCtx = new WeakReference<>(parent);
        }

        private DownloadFilesTask(Context parent, ImageView imgView) {
            parentCtx = new WeakReference<>(parent);
            imageView = new WeakReference<>(imgView);
        }

        protected NoteImage doInBackground(String... url) {

            NoteImage oObject = new NoteImage();
            String strURL = url[0];

            try {

                futureTarget = new WeakReference<>(
                        Glide.with(parentCtx.get())
                                .asBitmap()
                                .load(strURL)
                                .submit(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL));

                Bitmap bm = futureTarget.get().get();

                oObject.setNoteImageURL(strURL);
                oObject.setNoteImageBitmap(Bitmap.createBitmap(bm));

            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return oObject;
        }

        protected void onPostExecute(NoteImage oObject) {
            if (isCancelled() || oObject == null) {
                return;
            }

            noteImageList.add(oObject);

            if (imageView != null) {
                imageView.get().setImageBitmap(oObject.getNoteImageBitmap());
            }

            // Do something with the Bitmap and then when you're done with it:
            Glide.with(parentCtx.get()).clear(futureTarget.get());
        }
    }

    private NoteCategoryListAdapter spinnerArrayAdapter;

    private CheckBox mNotePersonalCheckbox;
    private EditText mEditNoteTitleView;
    private EditText mEditNoteTextView;
    private Spinner mSpinnerNoteCategory;
    private AppCompatEditText mEditNoteKeywordView;
    private ImageSwitcher mNoteImageSwitcher;
    private TextView mNoteImageCounterTextView;
    private int currentIndex = -1;
    private int downX = 0;
    private int upX = 0;
    private int iSpannedLength = 0;
    private int iTagLength = 0;
    private final Context ctx = this;
    private String strNoteKey = null;
    private Note mNote;
    private ArrayList<NoteImage> noteImageList = new ArrayList<>();
    private ArrayList<NoteImage> deleteNoteImageList = new ArrayList<>();
    private int placeholderIdImage = R.drawable.ic_insert_photo;
    private int backgroundColorImage = Color.BLACK;

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        String sharedTitle = intent.getStringExtra(Intent.EXTRA_TITLE);
        String sharedSubject = intent.getStringExtra(Intent.EXTRA_SUBJECT);
        if (sharedText != null) {
            mEditNoteTextView.setText(sharedText);
        }
        if (sharedTitle != null) {
            mEditNoteTitleView.setText(sharedTitle);
        } else if (sharedSubject != null) {
            mEditNoteTitleView.setText(sharedSubject);
        }
    }

    void handleSendImage(Intent intent) {
        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            // Update UI to reflect image being shared
            NoteImage img = new NoteImage();
            img.setNoteImageUri(imageUri);
            img.setNoteImageStatusKnz(StatusKnz.INSERT);
            noteImageList.add(img);

            currentIndex = 0;
            mNoteImageSwitcher.setImageURI(imageUri);

            mNoteImageCounterTextView.setVisibility(View.VISIBLE);
            mNoteImageCounterTextView.setText("1/1");
        }

        handleSendText(intent);
    }

    void handleSendMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {
            // Update UI to reflect multiple images being shared

            for (Uri imageUri : imageUris) {
                NoteImage img = new NoteImage();
                img.setNoteImageUri(imageUri);
                img.setNoteImageStatusKnz(StatusKnz.INSERT);
                noteImageList.add(img);
            }

            currentIndex = 0;
            mNoteImageSwitcher.setImageURI(imageUris.get(currentIndex));

            mNoteImageCounterTextView.setVisibility(View.VISIBLE);
            mNoteImageCounterTextView.setText("1/" + noteImageList.size());
        }

        handleSendText(intent);
    }

    private void handleIntent() {
        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        // Check if user is signed in (non-null) and update UI accordingly.
        if (UserProfileViewModel.getInstance().getLoggedUserID() == null) {

            activityLauncher.launch(new Intent(AddNewNoteActivity.this, LoginActivity.class), result -> {
                int iResult = result.getResultCode();

                if (iResult != Activity.RESULT_OK) {
                    AppManager.getAppManager().finishActivity(AddNewNoteActivity.this, RESULT_CANCELED);
                }
            });
        }

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            } else if (type.startsWith("image/")) {
                handleSendImage(intent); // Handle single image being sent
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            if (type.startsWith("image/")) {
                handleSendMultipleImages(intent); // Handle multiple images being sent
            }
        } else {
            // Handle other intents, such as being started from the home screen
            fillInputFields(intent);
        }
    }

    private void fillInputFields(Intent intent) {
        if (intent == null || !intent.hasExtra("NOTE_KEY"))
            return;

        strNoteKey = intent.getStringExtra("NOTE_KEY");
        mNote = UserProfileViewModel.getNoteViewModel() != null ? UserProfileViewModel.getNoteViewModel().findNoteByKey(strNoteKey) : null;

        if (mNote == null)
            return;

        mEditNoteTextView.setText(mNote.getNoteText());
        mEditNoteTitleView.setText(mNote.getNoteTitle());
        mNotePersonalCheckbox.setChecked(mNote.isNotePersonal());

        //Bild
        if (mNote.getNoteImageListURL() != null && !mNote.getNoteImageListURL().isEmpty()) {

            currentIndex = 0;
            int iSum = mNote.getNoteImageListURL().size();
            for (int iCounter = 0; iCounter < iSum; iCounter++) {
                String noteImageURL = mNote.getNoteImageListURL().get(iCounter);
                if (iCounter == 0)
                    new DownloadFilesTask(AddNewNoteActivity.this, (ImageView) mNoteImageSwitcher.getCurrentView())
                            .execute(noteImageURL);
                else
                    new DownloadFilesTask(AddNewNoteActivity.this).execute(noteImageURL);
            }

            mNoteImageCounterTextView.setVisibility(View.VISIBLE);
            mNoteImageCounterTextView.setText("1/" + iSum);
        }

        //Stichwörter
        String strKeyWord = mNote.getNoteKeyword();
        if (strKeyWord == null || strKeyWord.length() < 1)
            return;
        String[] strTagArray = strKeyWord.split("[, ]");
        if (strTagArray.length < 1)
            strTagArray = new String[]{strKeyWord};

        int iIndex = -1;
        while (++iIndex < strTagArray.length)
            mEditNoteKeywordView.getEditableText().append(strTagArray[iIndex]).append(" ");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_note);
        Objects.requireNonNull(getSupportActionBar()).setTitle(R.string.activity_title_noteItem);

        mScrollView = findViewById(R.id.scrollView_addnewnote);
        mProgressBar = findViewById(R.id.progressBar_addnewnote);

        //Stichwörter
        mEditNoteKeywordView = findViewById(R.id.add_new_note_keyword);
        mEditNoteKeywordView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence == null || charSequence.length() < 1) return;

                String strValue = charSequence.toString();
                if (strValue.endsWith(" ")) if (strValue.length() > iSpannedLength)
                    iTagLength = strValue.length() - iSpannedLength;
                else iSpannedLength = strValue.length();
                else iTagLength = 0;
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() - iSpannedLength == iTagLength) {
                    ChipDrawable chip = ChipDrawable.createFromResource(ctx, R.xml.chip);
                    chip.setText(editable.subSequence(iSpannedLength, editable.length()));
                    chip.setBounds(0, 0, chip.getIntrinsicWidth(), chip.getIntrinsicHeight());
                    ImageSpan span = new ImageSpan(chip);
                    editable.setSpan(span, iSpannedLength, editable.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    iSpannedLength = editable.length();
                    iTagLength = 0;
                }
            }
        });

        mEditNoteKeywordView.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            boolean handled = false;
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                if (textView.getText() != null && !(textView.getText().toString().endsWith(" ")))
                    textView.getEditableText().append(" ");
            }
            return handled;
        });

        mNotePersonalCheckbox = findViewById(R.id.add_new_note_personal_checkbox);
        mEditNoteTitleView = findViewById(R.id.add_new_note_title);
        mEditNoteTextView = findViewById(R.id.add_new_note_text);

        //ComboBox für Kategorie
        mSpinnerNoteCategory = findViewById(R.id.add_new_note_category_spinner);
        mSpinnerNoteCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            private boolean selectionControl = true;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mNote != null && selectionControl) {
                    selectionControl = false;
                    mSpinnerNoteCategory.setSelection(spinnerArrayAdapter.getItemPosition(mNote.getNoteCategoryKey()));
                    return;
                }

                Category selectedCategory = spinnerArrayAdapter.getItem(position);

                if (selectedCategory != null) {
                    String strSpinnerText = selectedCategory.getCategoryName();
                    backgroundColorImage = selectedCategory.getCategoryColor();
                    placeholderIdImage = Utilities.getDrawableIdentByName(ctx, strSpinnerText);
                    ((TextView) view).setText(strSpinnerText);
                }

                mNoteImageSwitcher.setBackgroundColor(backgroundColorImage);

                if (currentIndex < 0)
                    mNoteImageSwitcher.setImageResource(placeholderIdImage);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        //ImageCounter
        mNoteImageCounterTextView = findViewById(R.id.add_new_note_image_counter);

        //FAB Menu
        FloatingActionsMenu fab_menu = findViewById(R.id.fab_edit_note_images_menu);

        //check permission: Kamera/Speicher
        checkPermissions(ImagePicker.permissionStringList);

        //Upload für Image
        findViewById(R.id.fab_add_new_note_images).setOnClickListener(v -> {

            ImagePicker imgPicker = ImagePicker.getInstance();
            Intent chooseImageIntent = imgPicker.getPickImageIntent(this, getString(R.string.intent_pickimg_addnewnote), true);
            if (chooseImageIntent == null)
                return;

            activityLauncher.launch(chooseImageIntent, result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {

                    fab_menu.collapseImmediately();

                    ClipData clipData = null;
                    if (result.getData() != null)
                        clipData = result.getData().getClipData();

                    if (clipData != null) {
                        Uri imageUri = null;
                        for (int i = 0; i < clipData.getItemCount(); i++) {
                            imageUri = clipData.getItemAt(i).getUri();
                            // your code for multiple image selection
                            NoteImage img = new NoteImage();
                            img.setNoteImageUri(imageUri);
                            img.setNoteImageStatusKnz(StatusKnz.INSERT);
                            noteImageList.add(img);
                        }

                        if (imageUri != null)
                            mNoteImageSwitcher.setImageURI(imageUri);

                    } else {
                        Uri imageUri = imgPicker.getImageFromResult(this, result.getResultCode(), result.getData());

                        // your codefor single image selection
                        NoteImage img = new NoteImage();
                        img.setNoteImageUri(imageUri);
                        img.setNoteImageStatusKnz(StatusKnz.INSERT);
                        noteImageList.add(img);

                        mNoteImageSwitcher.setImageURI(imageUri);
                    }

                    currentIndex = noteImageList.size() - 1;

                    mNoteImageCounterTextView.setVisibility(View.VISIBLE);
                    mNoteImageCounterTextView.setText(noteImageList.size() + "/" + noteImageList.size());
                }
            });
        });

        findViewById(R.id.fab_delete_note_image).setOnClickListener(v -> {
            if (noteImageList.size() > 0 && noteImageList.get(currentIndex) != null) {
                NoteImage oImage = noteImageList.get(currentIndex);

                if (!TextUtils.isEmpty(oImage.getNoteImageURL())) {
                    oImage.setNoteImageStatusKnz(StatusKnz.DELETE);
                    deleteNoteImageList.add(oImage);
                }

                noteImageList.remove(currentIndex);
            }

            if (noteImageList.size() == 0) {
                mNoteImageSwitcher.setImageResource(placeholderIdImage);
                mNoteImageCounterTextView.setVisibility(View.GONE);
                currentIndex = -1;
                return;
            }

            --currentIndex;

            if (currentIndex < 0)
                currentIndex = noteImageList.size() - 1;

            NoteImage oImage = noteImageList.get(currentIndex);

            if (oImage.getNoteImageUri() != null) {
                mNoteImageSwitcher.setImageURI(oImage.getNoteImageUri());
            } else if (oImage.getNoteImageBitmap() != null) {
                ((ImageView) mNoteImageSwitcher.getNextView()).setImageBitmap(oImage.getNoteImageBitmap());
                mNoteImageSwitcher.showNext();
            } else return;

            mNoteImageCounterTextView.setVisibility(View.VISIBLE);
            mNoteImageCounterTextView.setText((currentIndex + 1) + "/" + noteImageList.size());
        });

        mNoteImageSwitcher = findViewById(R.id.add_new_note_image_switcher);
        mNoteImageSwitcher.setFactory(() -> {
            ImageView myView = new ImageView(getApplicationContext());
            myView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            myView.setLayoutParams(new ImageSwitcher.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT,
                    ActionBar.LayoutParams.MATCH_PARENT));
            return myView;
        });

        mNoteImageSwitcher.setInAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
        mNoteImageSwitcher.setOutAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_out));
        mNoteImageSwitcher.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                downX = (int) event.getX();
                return true;
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                upX = (int) event.getX();

                if (noteImageList.size() == 0)
                    return true;

                if (upX - downX > 100) {
                    //curIndex  current image index in array viewed by user
                    currentIndex--;
                    // condition
                    if (currentIndex < 0)
                        currentIndex = noteImageList.size() - 1;

                    mNoteImageSwitcher.setInAnimation(AnimationUtils.loadAnimation(AddNewNoteActivity.this, android.R.anim.slide_in_left));
                    mNoteImageSwitcher.setOutAnimation(AnimationUtils.loadAnimation(AddNewNoteActivity.this, android.R.anim.slide_out_right));

                } else if (downX - upX > -100) {
                    //curIndex  current image index in array viewed by user
                    currentIndex++;
                    // condition
                    if (currentIndex >= noteImageList.size())
                        currentIndex = 0;

                    mNoteImageSwitcher.setInAnimation(AnimationUtils.loadAnimation(AddNewNoteActivity.this, R.anim.slide_in_right));
                    mNoteImageSwitcher.setOutAnimation(AnimationUtils.loadAnimation(AddNewNoteActivity.this, R.anim.slide_out_left));
                }

                NoteImage oImage = noteImageList.get(currentIndex);

                if (oImage != null && oImage.getNoteImageUri() != null) {
                    mNoteImageSwitcher.setImageURI(oImage.getNoteImageUri());
                } else if (oImage != null && oImage.getNoteImageBitmap() != null) {
                    ((ImageView) mNoteImageSwitcher.getNextView()).setImageBitmap(oImage.getNoteImageBitmap());
                    mNoteImageSwitcher.showNext();
                } else
                    return false;

                mNoteImageCounterTextView.setVisibility(View.VISIBLE);
                mNoteImageCounterTextView.setText((currentIndex + 1) + "/" + noteImageList.size());

                return true;
            }
            return false;
        });

        handleIntent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //ActionBar
        this.getMenuInflater().inflate(R.menu.menu_add_new_note_activity, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.MenuItemSaveCancelNote:
                AppManager.getAppManager().finishActivity(this, RESULT_CANCELED);

                return true;
            case R.id.MenuItemSaveNote:
                saveNote();

                return true;
            case R.id.MenuItemDeleteNote:
                if (strNoteKey != null)
                    this.showNoticeDialog(this.getString(R.string.dialog_title_warning), this.getString(R.string.dialog_text_question_delete), new int[]{R.string.btn_Delete, R.string.btn_Cancel});

                return true;
            case R.id.MenuItemShareNote:

                String strIntentTitle = this.getString(R.string.title_share_note);
                String strTitle = mEditNoteTitleView.getText().toString();
                String strText = mEditNoteTextView.getText().toString();
                Bitmap bm = null;

                //add by app
                strText += "\n----------------\n#SelfNote";

                if (noteImageList.isEmpty()) {
                    Bitmap oldBitmap = BitmapFactory.decodeResource(getResources(), placeholderIdImage);

                    // Create new bitmap based on the size and config of the old
                    bm = oldBitmap.copy(Bitmap.Config.ARGB_8888, true);
                    Canvas canvas = new Canvas(bm);
                    canvas.drawColor(backgroundColorImage);
                    canvas.drawBitmap(oldBitmap, 0, 0, null);

                    //water mark
                    Bitmap waterMark = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_round_selfnote);
                    //  canvas.drawBitmap(waterMark, 0, 0, null);
                    int startX = (canvas.getWidth() - waterMark.getWidth());
                    int startY = (canvas.getHeight() - waterMark.getHeight());
                    canvas.drawBitmap(waterMark, startX, startY, null);

                } else {
                    bm = Utilities.getBitmap((ImageView) mNoteImageSwitcher.getCurrentView());

                    if (noteImageList.size() > 1) {

                        ArrayList<Uri> imageUris = new ArrayList<>();

                        ClipData clipData = null;

                        for (NoteImage noteImage : noteImageList) {
                            Uri imageUri = null;
                            if (noteImage.getNoteImageUri() != null)
                                imageUri = noteImage.getNoteImageUri();
                            else if (noteImage.getNoteImageBitmap() != null)
                                imageUri = Utilities.getUriForBitmap(noteImage.getNoteImageBitmap(), this);
                            else
                                continue;

                            imageUris.add(imageUri); // Add your image URIs here

                            if (clipData == null)
                                clipData = ClipData.newUri(getContentResolver(), strTitle, imageUri);
                            else
                                clipData.addItem(new ClipData.Item(imageUri));
                        }

                        Intent shareIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);

                        shareIntent.setClipData(clipData);
                        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
                        shareIntent.putExtra(Intent.EXTRA_TITLE, strTitle);
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, strTitle);
                        shareIntent.putExtra(Intent.EXTRA_TEXT, strTitle.toUpperCase() + "\n\n" + strText);
                        shareIntent.setType("image/*");
                        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(Intent.createChooser(shareIntent, strIntentTitle));

                        return true;
                    }
                }

                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TITLE, strTitle);
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, strTitle);
                shareIntent.putExtra(Intent.EXTRA_TEXT, strTitle.toUpperCase() + "\n\n" + strText);
                shareIntent.setType("text/plain");

                if (bm != null) {
                    Uri shareUri = Utilities.getUriForBitmap(bm, this);
                    shareIntent.setDataAndType(shareUri, "image/png");
                    shareIntent.setClipData(ClipData.newUri(getContentResolver(), strTitle, shareUri));
                    shareIntent.putExtra(Intent.EXTRA_STREAM, shareUri);
                }

                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, strIntentTitle));

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveNote() {
        if (TextUtils.isEmpty(mEditNoteTitleView.getText()) || mSpinnerNoteCategory.getSelectedItem() == null || TextUtils.isEmpty(mSpinnerNoteCategory.getSelectedItem().toString())) {
            Toast.makeText(getApplicationContext(), R.string.toast_empty_note_not_saved, Toast.LENGTH_LONG).show();
        } else {

            if (mNote == null) {
                mNote = new Note();
                mNote.setNoteKey(UserProfileViewModel.getNoteViewModel().getNewNoteKey());
                mNote.setNoteTimestamp(System.currentTimeMillis());
                mNote.setCreatorUID(UserProfileViewModel.getLoggedUserID());
            }

            mNote.setNoteCategory((Category) mSpinnerNoteCategory.getSelectedItem());
            mNote.setNoteTitle(mEditNoteTitleView.getText().toString());
            mNote.setNoteText(mEditNoteTextView.getText().toString());
            mNote.setNoteKeyword(Objects.requireNonNull(mEditNoteKeywordView.getText()).toString());

            if (mNotePersonalCheckbox.isChecked())
                mNote.setNotePersonal();
            else
                mNote.setNotePersonal(false);

            UserProfileViewModel.getInstance().addReaderUIDToNote(mNote);

            noteImageList.addAll(deleteNoteImageList);

            UserProfileViewModel.getNoteViewModel().save(this, noteImageList, mNote);
        }
    }

    @Override
    public void onDialogPositiveClick(NoticeDialogFragment dialog, int iTextId) {
        super.onDialogPositiveClick(dialog, iTextId);

        switch (iTextId) {
            case R.string.btn_Delete:
                UserProfileViewModel.getNoteViewModel().delete(this, mNote);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (spinnerArrayAdapter != null) {
            spinnerArrayAdapter.startListening();
        } else if (UserProfileViewModel.getCategoryViewModel() != null) {
            spinnerArrayAdapter = new NoteCategoryListAdapter(
                    UserProfileViewModel.getCategoryViewModel().getCategoryOptions(this));
            mSpinnerNoteCategory.setAdapter(spinnerArrayAdapter);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (spinnerArrayAdapter != null) {
            spinnerArrayAdapter.stopListening();
        }
    }
}