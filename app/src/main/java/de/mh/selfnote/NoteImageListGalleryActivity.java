package de.mh.selfnote;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;

import java.util.ArrayList;

import de.mh.selfnote.adapter.NoteImageListAdapter;
import de.mh.selfnote.extended.BasicActivity;

public class NoteImageListGalleryActivity extends BasicActivity {

    private RecyclerView viewResult;
    private NoteImageListAdapter adapter;
    private ArrayList<String> imgURLList;
    public static final String INTENT_IMG_URL_LIST = "NOTE_IMAGE_LIST_URL";

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_image_list_gallery);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        viewResult = findViewById(R.id.note_image_list_gallery);

        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(NoteImageListGalleryActivity.this);
        flexboxLayoutManager.setFlexWrap(FlexWrap.WRAP);
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        flexboxLayoutManager.setAlignItems(AlignItems.STRETCH);

        viewResult.setLayoutManager(flexboxLayoutManager);

        imgURLList = getIntent().getStringArrayListExtra(INTENT_IMG_URL_LIST);
        //Adapter initialisierung
        adapter = new NoteImageListAdapter(this, imgURLList);
        viewResult.setAdapter(adapter);
    }
}